<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Manage Accounts</title>

        <!--        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">
                <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
                <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
                <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
                <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
                <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
                <link href="assets/css/manager.css" rel="stylesheet" type="text/css"/>-->
        <!------------------------------------------------------------->
        <!--bs-->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">-->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link href="css/manageSongs.css" rel="stylesheet" type="text/css"/>

        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>

        <script src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
        <link rel="stylesheet" href="https://cdn.datatables.net/1.11.5/css/jquery.dataTables.min.css">

        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/bootstrap-select.min.js"></script>

        <style>
            table {
                width: 50%;
                counter-reset: 0;
            }
            table tr {
                counter-increment: row-num;
            }
            table tr td:first-child::before {
                content: counter(row-num) "";
            }
            .table-title {
                background: gray;
            }
        </style>
        <script>
            $(document).ready(function () {
                $('#dtTableProduct').DataTable();
                $('.dataTables_length').addClass('bs-select');
            });
        </script>
    </head>
    <body>


        <div class="container">
            <div class="table-wrapper">
                <div class="table-title">
                    <div class="row">
                        <div class="col-sm-6">
                            <h2>Manage <b>Accounts</b></h2>
                        </div>
                    </div>
                </div>
                <h3 style="color: red">${mess}</h3>
                <table id="dtTableProduct"  class="table table-striped table-hover ">
                    <thead>
                        <tr>
                            <th class="text-center">No</th>
                            <th class="text-center">Username</th>
                            <th class="text-center">Password</th>
                            <th class="text-center">Email</th>
                            <th class="text-center">Date Created</th>
                            <th class="text-center">Role</th>
                            <th class="text-center">Status</th>
                            <th class="text-center">Action</th>
                            <th class="text-center">Details</th>
                            <th class="text-center">Report</th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach items="${requestScope.listAccount}" var="u">
                            <tr class="text-center">
                                <td style="vertical-align: middle"></td>
                                <td style="vertical-align: middle">${u.username}</td>
                                <td style="vertical-align: middle">******</td>
                                <td style="vertical-align: middle">${u.email}</td>
                                <td style="vertical-align: middle">${u.t_create}</td>
                                <td style="vertical-align: middle"><c:forEach items="${requestScope.listRole}" var="r">
                                        <c:if test="${r.id == u.role_ID}">${r.name}</c:if>
                                    </c:forEach></td> 
                                <td style="vertical-align: middle"><c:if test="${u.status == true}"><a>Active</a></c:if>
                                    <c:if test="${u.status == false}"><a>Inactive</a></c:if>
                                    </td>
                                    <td style="vertical-align: middle">
                                    <c:if test="${u.status == true}"><a href="block?id=${u.user_ID}" class="btn btn-danger">Deactivate</a></c:if>
                                    <c:if test="${u.status == false}"><a href="unblock?id=${u.user_ID}" class="btn btn-success">Activate</a></c:if>
                                    </td>                        
                                    <td style="vertical-align: middle"><a href="profiledetails?userid=${u.user_ID}" class="fa fa-eye"></a></td>
                                <c:if test="${u.report == true}">
                                 <td style="vertical-align: middle"><a href="">Report block</a></td>
                                </c:if>
                                   <c:if test="${u.report == false}">
                                 <td style="vertical-align: middle"></td>
                                </c:if>
                            
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </div>
            <a href="home"><button type="button" class="btn btn-danger">Back to home</button></a>
        </div>
        <script src="js/manager.js" type="text/javascript"></script>
    </body>
</html>
