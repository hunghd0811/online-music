<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<!DOCTYPE html>
<html>
    <head>
        <title>SignUp Page</title>
        <!--Made with love by Mutiullah Samim -->

        <!--Bootsrap 4 CDN-->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

        <!--Fontawesome CDN-->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

        <!--Custom styles-->
        <link rel="stylesheet" href="./css/login.css">
    </head>

    <body style="background-image: url('./img/bg-gif/music-bgr.jpg');">
        <style>
            .btn-primary {
                background-color: #f55656;
                border-color: #f55656;
            }
            .btn-primary:hover {
                color: black;
                background-color: white;
            }
            .card{
                height: 700px;
            }
            .input-group {
                color: white;
            }
            .login_btn {
                background-color: #f55656;
            }
            .input-group-prepend span {
                background-color: #f55656;
            }
            .social_icon span {
                color: #f55656;
            }
        </style>
        <div class="container">
            <div class="d-flex justify-content-center h-100">
                <div style="height: 35rem;" class="card">
                    <div class="card-header">
                        <h3>Sign Up</h3>
                        <div class="d-flex justify-content-end social_icon">
                            <span><i class="fab fa-facebook-square"></i></span>
                            <span><i class="fab fa-google-plus-square"></i></span>
                            <span><i class="fab fa-twitter-square"></i></span>
                        </div>
                    </div>
                    <div class="card-body" style="height: 600px">
                        <form action="signup" method="post">
                            <p class="text-success">${requestScope.success}</p>
                            <p class="text-danger">${requestScope.error}</p>
                            <p class="text-danger">${requestScope.error1}</p>
                            <div class="input-group form-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fas fa-user"></i></span>
                                </div>
                                <input type="text" name="username" class="form-control" placeholder="Username" required>

                            </div>
                            <div class="input-group form-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fas fa-solid fa-envelope"></i></span>
                                </div>
                                <input type="email" name="email" class="form-control" placeholder="Email" required>
                            </div>   
                            <div class="input-group form-group">
                                <div class="input-group-prepend">

                                    <input required  type="radio" name="gender" value="Male" ${user.gender =="Male"?"checked":""}/>Male
                                    <input required type="radio" name="gender" value="Female" ${user.gender == "Female"?"checked":""}/>Female
                                </div>                                                              
                            </div>  
                            <div class="input-group form-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fas fa-solid fa-user"></i></span>
                                </div>
                                <input type="date" name="date" class="form-control" required>
                            </div>    
                            <div class="input-group form-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fas fa-key"></i></span>
                                </div>
                                <input type="password" name="password" class="form-control" placeholder="Password" required>
                            </div>
                            <div class="input-group form-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fas fa-key"></i></span>
                                </div>
                                <input type="password" name="repassword" class="form-control" placeholder="Confirm Password" required>
                            </div>                                                 
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-lg btn-block">SignUp</button>
                            </div>
                        </form>
                    </div>
                    <div class="card-footer">
                        <div class="d-flex justify-content-center links">
                            Already have an account?<a href="login.jsp">Sign In</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
