<%-- 
    Document   : updateSongs
    Created on : May 29, 2022, 12:39:23 AM
    Author     : Thanh Thao
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Edit Product</title>
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link href="css/manageSongs.css" rel="stylesheet" type="text/css"/>
        <!-------------------------------------------------------->


        <style>
            img{
                width: 200px;
                height: 120px;
            }
        </style>
    </head>
    <body>

        <div class="container">
            <div class="table-wrapper">
                <div class="table-title">
                    <div class="row">
                        <div class="col-sm-6">
                            <h2>Edit <b>Songs</b></h2>
                        </div>
                        <div class="col-sm-6">
                        </div>
                    </div>
                </div>
            </div>
            <div id="editEmployeeModal">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <form action="update" method="post">
                            <div class="modal-header">						
                                <h4 class="modal-title">Update Songs</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            </div>
                            <div class="modal-body">					
                                <div class="form-group">
                                    <label>ID</label>
                                    <input value="${detail.song_ID}" name="id" type="text" class="form-control" readonly required>
                                </div>
                                <div class="form-group">
                                    <label>Name</label>
                                    <input value="${detail.name}" name="name" type="text" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label>Author</label>
                                    <input value="${detail.author}"name="author" class="form-control" required>
                                </div>

                                <div class="form-group">
                                    <label>Image</label>
                                    <input value="${detail.image}" name="image" type="text" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label>Path Song</label>
                                    <input value="${detail.path}" name="path" type="text" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label>Duration</label>
                                    <input value="${detail.duration}" name="duration" type="text" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label>Lyric</label>
                                    <textarea value="${detail.lyric}" name="lyric" type="text" class="form-control" >${detail.lyric}</textarea>
                                </div> 
                                <div class="form-group">
                                    <label>Category</label>
                                    <select name="category" class="form-select" aria-label="Default select example">
                                        <option value="0">Non-Category</option>
                                        <c:forEach items="${requestScope.listC}" var="o">
                                            <option <c:if test="${detail.category_ID == o.categoryID}">
                                                    selected
                                                </c:if>
                                                value="${o.categoryID}">${o.categoryDetail_name}</option>
                                        </c:forEach>
                                    </select>
                                </div>

                            </div>
                            <div class="modal-footer">
                                <a href="manager"><input type="button"  class="btn btn-default" value="Cancel"></a>
                                <input type="submit" class="btn btn-success" value="Edit">
                            </div>

                        </form>
                    </div>
                </div>
            </div>

        </div>




        <script src="js/manager.js" type="text/javascript"></script>
    </body>
</html>

