<%-- 
    Document   : managealbum
    Created on : May 31, 2022, 9:36:24 PM
    Author     : anhqu
--%>




<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>View PlayList</title>
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link href="css/manage.css" rel="stylesheet" type="text/css"/>

        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>

        <script src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
        <link rel="stylesheet" href="https://cdn.datatables.net/1.11.5/css/jquery.dataTables.min.css">

        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/bootstrap-select.min.js"></script>

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
        <script src="https://cdn.jsdelivr.net/sharer.js/latest/sharer.min.js"></script>
        <style>
            img{
                width: 200px;
                height: 120px;
            }

            .slider__item .slider__content {
                margin-bottom: 500px;
            }
            @media all and (min-width: 992px) {
                .header__nav .nav-item .dropdown-menu{ display: none; }
                .header__nav .nav-item:hover .nav-link{   }
                .header__nav .nav-item:hover .dropdown-menu{ display: block; background-color: #e2264d }
                .header__nav .nav-item .dropdown-menu{ margin-top:0; }
            }
            select{
                background-color: #616161;
                color: white;
                border-radius: 10px;
            }
            .btn {
                position: relative;
                z-index: 1;
                height: 46px;
                line-height: 43px;
                font-size: 14px;
                font-weight: 600;
                display: inline-block;
                padding: 0 20px;
                text-align: center;
                text-transform: uppercase;
                border-radius: 30px;
                -webkit-transition: all 500ms;
                -o-transition: all 500ms;
                transition: all 500ms;
                border: 2px solid #f55656;
                letter-spacing: 1px;
                box-shadow: none;
                background-color: #f55656;
                color: #ffffff;
                cursor: pointer;
            }
            a, a:active, a:focus, a:hover {
                color: #232323;
                text-decoration: none;
                -webkit-transition-duration: 500ms;
                -o-transition-duration: 500ms;
                transition-duration: 500ms;
                outline: none;
            }
            .table-title {
                background: gray;
            }
        </style>
        <script>
            $(document).ready(function () {
                $('#dtTableProduct').DataTable();
                $('.dataTables_length').addClass('bs-select');
            });
        </script>
    </head>
    <body>
        <jsp:include page="Header.jsp"/>

        <br>
        <br>
        <br>
        <div class="container">
            <div class="table-wrapper">
                <div class="table-title">
                    <div class="row">
                        <div class="col-sm-6">
                            <h2>View <b>PlayList</b></h2>
                        </div>                    
                    </div>
                </div>
                <table id="dtTableProduct" class="table table-striped table-hover ">
                    <thead>
                        <tr>
                            <th>PlayListID</th>                                                 
                            <th>PlayListName</th> 

                            <th>Time Create</th>
                            <th>Share</th>
                            <th></th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>

                        <c:forEach items="${requestScope.viewplay}" var="o">

                            <tr>
                                <td>${o.playlist_ID}</td>                                                            
<!--                                <td>${o.name} </td> -->
                                <td>
                                    <a href= "detailplaylist?id=${o.playlist_ID}">${o.name}</a>
                                </td>
                                <td>${o.t_create}</td>

                                <td style="vertical-align: middle"><c:if test="${o.sharestatus == false}"><a>Not Share</a></c:if>
                                    <c:if test="${o.sharestatus == true}"><a>Share</a></c:if>
                                    </td>
                                    <td style="vertical-align: middle">
                                    <c:if test="${o.sharestatus == false}"><a href="share?id=${o.playlist_ID}" class="btn btn-danger">Share</a></c:if>
                                    <c:if test="${o.sharestatus == true}"><a href="unshare?id=${o.playlist_ID}" class="btn btn-success">Not Share</a></c:if>
                                    </td>

                                    <td>
                                        <a href="deleteplaylist?id=${o.playlist_ID}" onclick="return confirm('You want to delete this product?');" class="delete" data-toggle="modal"><i class="material-icons" data-toggle="tooltip" title="Delete">&#xE872;</i></a>                                                                  
                                    <c:if test="${o.sharestatus == true}" >
                                        <a href="#addEmployeeModal" data-toggle="modal"    ><i class="fas fa-share-alt"></i></a>
                                        </c:if>
                                </td>
                            </tr>
                        <div id="addEmployeeModal" class="modal fade">
                            <div class="modal-dialog">
                                <div class="modal-content">

                                    <div class="modal-header">						
                                        <h4 class="modal-title">Link to share</h4>
                                        <button type="button" class="close" data-dismiss="modal" >&times;</button>
                                    </div>
                                    <div class="modal-body">   
                                        <div class="form-group">
                                            <input type="text" id="copyText" value="http://localhost:8080/SE1607_Group4_OnlineMusic/shareplaylist?pid=${o.playlist_ID}" readonly class="form-control" readonly="">

                                        </div>
                                    </div>
                                    <div class="modal-footer">

                                        <input type="button" class="btn btn-success" data-dismiss="modal" value="Oke">
                                    </div>

                                </div>
                            </div>
                        </div>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
            <div class="table-wrapper">
                <div class="table-title">
                    <div class="row">
                        <div class="col-sm-6">
                            <h2>Share <b>PlayList</b></h2>
                        </div>                    
                    </div>
                </div>
                <table id="dtTableProduct" class="table table-striped table-hover ">
                    <thead>
                        <tr>
                            <th>PlayListID</th>                                                 
                            <th>PlayListName</th> 
                            <th>UserName</th>                        
                            <th>Time create</th>
                            <th>Status</th>

                        </tr>
                    </thead>
                    <tbody>

                        <c:forEach items="${requestScope.viewshare}" var="o">

                            <tr>
                                <td>${o.playlist_ID}</td>                                                            

                                <td>
                                    <a href= "detailplaylist?id=${o.playlist_ID}">${o.name}</a>
                                </td>

                                <td> 
                                    <c:if test="${requestScope.user.user_ID == o.user_ID}">
                                        ${requestScope.user.first_name}  ${requestScope.user.last_name} 
                                    </c:if>
                                </td>
                                <td>${o.t_create}</td>

                                <td style="vertical-align: middle"><c:if test="${o.sharestatus == false}"><a>Not Share</a></c:if>
                                    <c:if test="${o.sharestatus == true}"><a>Share</a></c:if>
                                    </td>



                                </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </div>
            <a href="home"><button type="button" class="btn btn-danger">Back to home</button></a>

        </div>

        <!-- Edit Modal HTML -->



        <script src="js/manager.js" type="text/javascript"></script>
        <script>
                                            const copyText = document.querySelector("#copyText");
                                            const pasteText = document.querySelector("#pasteText");
                                            const button = document.querySelector("button");
                                            const tooltip = document.querySelector(".tooltip");
                                            button.addEventListener('click', function () {
                                                copyText.select();
                                                pasteText.value = "";
                                                tooltip.classList.add("show");
                                                setTimeout(function () {
                                                    tooltip.classList.remove("show");
                                                }, 700);
                                                if (document.execCommand("copy")) {
                                                    pasteText.focus();
                                                } else {
                                                    alert("Xảy ra lỗi");
                                                }
                                            });
        </script>
    </body>
</html>