<%-- 
    Document   : adminProfileDetails
    Created on : May 21, 2022, 7:00:37 PM
    Author     : Black
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

        <script src="js/profile.js"></script>
        <link href="css/profile.css" rel="stylesheet" type="text/css"/>

    </head>
    <body>
        <div class="container">
            <div class="row">
                <section class="vh-100" style="background-color: #F7F4E9;">
                    <div class="container py-5 h-100">
                        <div class="row d-flex justify-content-center  h-100">
                            <div class="col col-lg-6 mb-4 mb-lg-0">
                                <div class="card mb-3" style="border-radius: .5rem;">
                                    <div class="row g-0">
                                        <div class="col-md-4 gradient-custom text-center text-white"
                                             style="border-top-left-radius: .5rem; border-bottom-left-radius: .5rem;">
                                            <img src="https://mdbcdn.b-cdn.net/img/Photos/new-templates/bootstrap-chat/ava1-bg.webp" class="img-fluid my-5" style="width: 80px;" />
                                            <h5>Online Music</h5>
                                            <p>Facebook</p>
                                            <p>Youtube</p>
                                            <i class="far fa-edit mb-5"></i>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="card-body p-4">
                                                <h6>Information</h6>
                                                <hr class="mt-0 mb-4">
                                                <div class="row pt-1">
                                                    <div class="col-6 mb-3">
                                                        <h6>User Name</h6>
                                                        <p class="text-muted">${profiledetails.username}</p>
                                                    </div>
                                                    <div class="col-6 mb-3">
                                                        <h6>Password</h6>
                                                        <p class="text-muted">${profiledetails.password}</p>
                                                    </div>
                                                </div>
                                                <div class="row pt-1">
                                                    <div class="col-6 mb-3">
                                                        <h6>First Name</h6>
                                                        <p class="text-muted">${profiledetails.first_name}</p>
                                                    </div>
                                                    <div class="col-6 mb-3">
                                                        <h6>Last Name</h6>
                                                        <p class="text-muted">${profiledetails.last_name}</p>
                                                    </div>
                                                </div>
                                                <div class="row pt-1">
                                                    <div class="col-6 mb-3">
                                                        <h6>Time Create</h6>
                                                        <p class="text-muted">${profiledetails.t_create}</p>
                                                    </div>
                                                    <div class="col-6 mb-3">
                                                        <h6>Last Online</h6>
                                                        <p class="text-muted">${profiledetails.t_lastOnline}</p>
                                                    </div>
                                                </div>
                                                <div class="row pt-1">
                                                    <div class="col-6 mb-3">
                                                        <h6>Email</h6>
                                                        <p class="text-muted">${profiledetails.email}</p>
                                                    </div>
                                                    <div class="col-6 mb-3">
                                                        <h6>Address</h6>
                                                        <p class="text-muted">${profiledetails.address}</p>
                                                    </div>
                                                </div>
                                                <div class="row pt-1">
                                                    <div class="col-6 mb-3">
                                                        <h6>Gender</h6>
                                                        <p class="text-muted">${profiledetails.gender}</p>
                                                    </div>
                                                    <div class="col-6 mb-3">
                                                        <h6>Date Of Birth</h6>
                                                        <p class="text-muted">${profiledetails.dob}</p>
                                                    </div>
                                                </div>
                                                <div class="row pt-1 d-flex justify-content-center">
                                                    <div class="col-6 mb-3">    
                                                        <a href="/SE1607_Group4_OnlineMusic/home"><button class="btn btn-primary" >Back To Home</button></a>
                                                        
                                                    </div>
                                                    <div class="col-6 mb-3">
                                                        <a href="/SE1607_Group4_OnlineMusic/listacc"><button class="btn btn-primary" >Manage Account</button></a>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </section>


            </div>
        </div>
    </body>
</html>
