<%-- 
    Document   : namageLike
    Created on : Jun 19, 2022, 10:10:20 AM
    Author     : Auriat
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Manager Like</title>

        
<!--        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link href="assets/css/manager.css" rel="stylesheet" type="text/css"/>-->
        <!------------------------------------------------------------->
        <!--bs-->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">-->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link href="css/manageSongs.css" rel="stylesheet" type="text/css"/>
        
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>

        <script src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
        <link rel="stylesheet" href="https://cdn.datatables.net/1.11.5/css/jquery.dataTables.min.css">

        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/bootstrap-select.min.js"></script>
        <!--font awesome-->
        <script src='https://kit.fontawesome.com/a076d05399.js' crossorigin='anonymous'></script>
        
        <style>
            img{
                width: 200px;
                height: 120px;
            }
        </style>
        <script>
            $(document).ready(function () {
                $('#dtTableProduct').DataTable();
                $('.dataTables_length').addClass('bs-select');
            });
        </script>
    </head>
    <body>
        <div class="container">
            <div class="table-wrapper">
                <div class="table-title">
                    <div class="row">
                        <div class="col-sm-6">
                            <h2>Manage <b>Like</b></h2>
                            <%
                    if (session.getAttribute("mess") != null) {
                %>
                <h2 id="h2" style="color:greenyellow">${sessionScope.mess}</h2>
                <%
                        session.removeAttribute("mess");
                    }
                %>  
                
                <%
                    if (session.getAttribute("mess1") != null) {
                %>
                <h2 id="h2" style="color:red">${sessionScope.mess1}</h2>
                <%
                        session.removeAttribute("mess1");
                    }
                %>  
                
                <%
                    if (session.getAttribute("mess2") != null) {
                %>
                <h2 id="h2" style="color:gold">${sessionScope.mess2}</h2>
                <%
                        session.removeAttribute("mess2");
                    }
                %>  
                        </div>
                        <!--                     <form class="search-form" action="manager" method="get">
                                            <input type="text" name="txt" placeholder="Search" required>
                                            <button type="submit"  class="btn btn-black" >Search</button>
                                        </form>-->
                        <div class="col-sm-6">
<!--                            <a href="#deleteEmployeeModal" class="btn btn-danger" data-toggle="modal"><i class="material-icons">&#xE15C;</i> <span>Delete</span></a>						-->
                        </div>
                    </div>
                </div>
                <table id="dtTableProduct"  class="table table-striped table-hover ">
                    <thead>
                        <tr>

                            <th>Like</th>
                            <th>Name</th>
                            <th>Author</th>
                            <th>Image</th>
                            <th>Path</th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach items="${requestScope.listS}" var="o">
                            <tr>

                                <td>${o.totalLike}</td>
                                
                                <td>
                                    <a href= "songDetail?id=${o.song_ID}">${o.name}</a>
                                </td>
                                <td>${o.author}</td>
                                <td>
                                    <img src="${o.image}">
                                </td>
                                <td>${o.path}</td>
                                
                            </tr>
                      </c:forEach>
                    </tbody>
                </table>
            </div>
            <a href="home"><button type="button" class="btn btn-primary">Back to home</button></a>
        </div>
        <script src="js/manager.js" type="text/javascript"></script>
    </body>
</html>