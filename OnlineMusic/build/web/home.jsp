
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%-- 
    Document   : home
    Created on : 28-05-2022
    Author     : duy
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<c:set var="user" scope="page" value="${sessionScope.user}" />
<c:set var="newsong" scope="page" value="${requestScope.newsong}" />
<c:set var="catename" scope="page" value="${requestScope.catename}" />
<c:set var="hotsong" scope="page" value="${requestScope.hotsong}" />
<c:set var="albums_Vpop" scope="page" value="${requestScope.albums_Vpop}" />
<c:set var="albums_USUK" scope="page" value="${requestScope.albums_USUK}" />
<c:set var="albums_Lofi" scope="page" value="${requestScope.albums_Lofi}" />
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Loustic</title>
        <link rel="stylesheet" href="./css/app.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
        <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.15.1/css/all.css" type="text/css">
        <link href="https://netdna.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">

    </head>
    <style>

        #likebn{
            display: none;
        }

        
        .slider__item .slider__content {
            margin-bottom: 500px;
        }
        @media all and (min-width: 992px) {
            .header__nav .nav-item .dropdown-menu{ display: none; }
            .header__nav .nav-item:hover .nav-link{   }
            .header__nav .nav-item:hover .dropdown-menu{ display: block; background-color: #e2264d }
            .header__nav .nav-item .dropdown-menu{ margin-top:0; }
        }

        .row {
            display: block;
            margin-left: 60px;
        }
        .col-sm-4 {
            flex: 0 0 33.333333%;
        }
        img.card-img-top{
            width: 300px; 
            height: 300px;
            margin-right: 30px;
        }

        .album-detail{
            display: inline-grid;
        }
        .album-detail .btn{
            margin-left: 100px;
            margin-right: 100px;
            margin-bottom: 100px;
        }
        album-btn{
            width: 400px;
            height: 400px;
        }
        select{
            background-color: #616161;
            color: white;
            border-radius: 10px;
        }
        .btn {
            position: relative;
            z-index: 1;
            height: 46px;
            line-height: 43px;
            font-size: 14px;
            font-weight: 600;
            display: inline-block;
            padding: 0 20px;
            text-align: center;
            text-transform: uppercase;
            border-radius: 30px;
            -webkit-transition: all 500ms;
            -o-transition: all 500ms;
            transition: all 500ms;
            border: 2px solid #f55656;
            letter-spacing: 1px;
            box-shadow: none;
            background-color: #f55656;
            color: #ffffff;
            cursor: pointer;
        }
        a, a:active, a:focus, a:hover {
            color: #232323;
            text-decoration: none;
            -webkit-transition-duration: 500ms;
            -o-transition-duration: 500ms;
            transition-duration: 500ms;
            outline: none;
        }
        .btn-option.active {
            background-color: #ffffff;
            border: none;
            color: #000000;
        }
        
    </style>
    <body>
        <%--Hearder--%>
        <section id="header" style="background: #000">
            <div class="header-mobile">
                <div class="mobile-toggle"><i class="far fa-times close"></i></div>
                <div class="header__logo"><a class="header-brand" href="./home"><img src="./img/core-img/logo.png" alt=""></a></div>
                <div class="menu-nav"> 
                    <ul class="header__nav-moblie">
                        <li>
                            <%
                                if (session.getAttribute("user") == null) {
                            %> 
                            <br>
                            <a href="login.jsp">Đăng nhập</a>
                            <%
                                }
                            %>
                        </li>
                    </ul>

                    <%--Search--%>
                    <div class="header__search"> 
                        <form value="txtSearch" action="search" method="post" style="display : flex;">
                            <input type="text" name="search" placeholder="Search and hit enter..." style="margin-top: 10px">
                            <select name="searchType" style="height: 30px; margin-top: 12px;">
                                <option value="1">Search By Name</option>
                                <option value="2">Search By Lyrics</option>
                                <option value="3">Search By Author</option>

                            </select>
                            <button class="btn" type="submit"> <i class="fa fa-search"> </i></button>
                        </form>
                    </div>
                </div>
            </div>
            <%--Hearder container--%>                
            <div class="header__container container-fluid">
                <div class="header__content" style="background-color: #000"> 
                    <div class="header__logo"><a class="header-brand" href="./home"><img src="./img/core-img/logo.png" alt=""></a></div>
                    <div class="header__menu"> 
                        <ul class="header__nav">
                            <c:if test="${user.role_ID == 1}">
                                <li class="nav-item dropdown">
                                    <a class="nav-link" href="./listacc">Manage Account</a>
                                    <ul class="dropdown-menu">
                                        <li><a class="dropdown-item" href="./manageAlbum">Manage Albums</a></li>
                                        <li><a class="dropdown-item" href="./manager">Manage Songs</a></li>
                                        <li><a class="dropdown-item" href="./managecomment">Manage Comment</a></li>
                                        <li><a class="dropdown-item" href="managehistory">Manage View History</a></li>
                                    </ul>
                                </li>
                            </c:if>
                            <c:if test="${user.role_ID == 3}">
                                <li><a href="./manager">Manage Songs</a></li>
                                </c:if>    
                            <li><a class="current" href="./home">Home </a></li>
                            <li class="nav-item dropdown">
                                <a class="nav-link" href="vpop?id=${1}">Nhạc Việt Nam </a>
                                <ul class="dropdown-menu">
                                    <li><a class="dropdown-item" href="cate?id=${1}"> Nhạc Trẻ</a></li>
                                    <li><a class="dropdown-item" href="cate?id=${2}"> Nhạc Trữ Tình </a></li>
                                    <li><a class="dropdown-item" href="cate?id=${3}"> Nhạc Cách Mạng </a></li>
                                </ul>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link" href="vpop?id=${2}">Nhạc Quốc Tế </a>
                                <ul class="dropdown-menu">
                                    <li><a class="dropdown-item" href="cate?id=${4}"> Nhạc US-UK</a></li>
                                    <li><a class="dropdown-item" href="cate?id=${5}"> Nhạc KPOP </a></li>
                                    <li><a class="dropdown-item" href="cate?id=${6}"> Nhạc Anime </a></li>
                                </ul>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link" href="vpop?id=${3}">Lofi </a>
                                <ul class="dropdown-menu">
                                    <li><a class="dropdown-item" href="cate?id=${8}"> Nhạc Piano</a></li>
                                    <li><a class="dropdown-item" href="cate?id=${9}"> Nhạc Guitar </a></li>
                                    <li><a class="dropdown-item" href="cate?id=${7}"> Nhạc Chill </a></li>
                                </ul>
                            </li>

                            <li class="nav-item dropdown">
                                <a class="nav-link" href="./albums">Albums </a>

                                <c:if test="${user != null}">
                                <li><a href="./allplaylist">View PlayList </a></li>                                
                                </c:if>
                            </li>
                            <c:if test="${user != null}">
                                <li><a href="history">Recently Visited</a></li>
                                </c:if>

                        </ul>
                        <div class="header__search"> 
                            <form value ="txtSearch" action="search" method="post" style="display: flex">
                                <input type="text" name="txtsearch" placeholder="Search and hit enter..." style="margin-top: 10px">
                                <select name="searchType" style="height: 30px; margin-top: 12px">
                                    <option value="1">Search By Name</option>
                                    <option value="2">Search By Lyrics</option>
                                    <option value="3">Search By Author</option>

                                </select>
                                <button class="btn" type="submit"> <i class="fa fa-search"> </i></button>
                            </form>
                        </div>
                        <ul class="header__social">      
                            <li class="login">
                                <%
                                    if (session.getAttribute("user") == null) {
                                %>                           
                                <a href="login.jsp">Đăng nhập</a>
                                <%
                                    }
                                %>
                            </li>

                            <c:if test="${user != null}">
                                <li>
                                    <div class="user"> 
                                        <a href="UpdateProfile"><div class="user__avatar bg-img" 
                                                                     <c:if test="${(user.avatar == null) or (user.avatar.isEmpty())}">
                                                                         style="background-image: url('./img/avatar.jpg');"
                                                                     </c:if>
                                                                     <c:if test="${user.avatar != null}">
                                                                         style="background-image: url('./img/uploads/${user.avatar}');"
                                                                     </c:if>
                                                                     ></div></a>
                                        <a class="fas fa-sign-out-alt icon" href="signout"></a>
                                        <div class="user__option"> 
                                            <div class="user__option-content"> 
                                                <div class="option-item view-info">
                                                    <div class="user__avatar bg-img" style="background-image: url('./img/core-img/avatar-default-1.jpg');"></div>
                                                    <div class="user__info"> 
                                                        <div class="user__name heading">${user.username} </div>
                                                        <div class="subtitle">See your profile</div>
                                                    </div>
                                                </div>
                                                <div class="option-item setting"><i class="fas fa-cog icon"></i>
                                                    <div class="heading">Settings </div>
                                                </div>
                                                <form action="sign">
                                                    <input type="text" name="url" value="home" hidden>
                                                    <label for="user__sign-out--pc">
                                                        <div class="option-item logout"><i class="fas fa-sign-out-alt icon"></i>
                                                            <div class="heading">Log Out</div>
                                                        </div>
                                                    </label>
                                                    <input type="submit" hidden id="user__sign-out--pc">
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </c:if>

                        </ul>
                    </div>
                    <div class="mobile-toggle"><i class="far fa-bars open"></i><i class="far fa-times close">  </i></div>
                </div>
                <div class="header__background"></div>
            </div>
        </section>     

        <section id="slider">
            <% int index = -1; %>
            <c:forEach var="slider" items="${newsong}">
                <% index++;%>
                <div class="slider__item bg-overlay bg-img slider__item-<%=index%> 
                     <% if (index == 1) { %> active <% } %>" style="background-image: url('${slider.image}');">
                    <div class="container">
                        <div class="slider__content">
                            <div class="slider__wellcome"> 
                                <h1 class="slider__heading">❤</h1>
                                <p class="slider__text"></p>
                                <div class="slider__group--btn">
                                    <div class="btn filter">Welcome Loustic Music</div>
                                    <div class="btn btn--second filter">Have A Good Day!</div>
                                </div>
                            </div>
                            <div class="slider__player player song-item" data-path="${slider.path}">
                                <div class="player__img bg-img" style="background-image: url('${slider.image}') ;">
                                    <div class="song-img--hover"></div>
                                </div>
                                <div class="player__content">
                                    <div class="player__info">
                                        <p class="player__date">${slider.t_create}</p>
                                        <h1 class="player__name song-name">${slider.name}</h1>
                                        <p class="player__text"><span class="player__author">${slider.author} | </span><span class="player_duration">00:${slider.duration}</span></p>
                                    </div>
                                    <div class="player__control">
                                        <div class="play"><i class="fas fa-play-circle"></i></div>
                                        <div class="pause active"><i class="fas fa-pause-circle"></i></div><span class="currenTime">00:00</span>
                                        <div class="sidebarTime--bg">
                                            <div class="sidebarTime--current"> </div>
                                        </div><span class="duration">${slider.duration}</span>
                                        <div class="volume">
                                            <div class="volume__icon"> <i class="mute fas fa-volume-slash"></i><i class="volume--low fas fa-volume active"></i><i class="volume--hight fas fa-volume-up"></i></div>
                                        </div>
                                        <div class="volume__silebar--bg">
                                            <div class="volume__silebar--current"></div>
                                        </div>
                                    </div>
                                    <div class="like-share-download">
                                        <div class="option like"></div>
                                        <div class="div">
                                            <div class="option share"> <i class="fas fa-share-alt"> <span>Share</span></i></div>
                                            <div class="option download"><c:if test="${user != null}"><a href="${slider.path}" download></c:if><i class="fas fa-download"> <span>Download</span></i></a></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </c:forEach>
        </section>

        <section id="recommend">  
            <%--New Song--%>
            <c:if test="${newsong != null}">
                <div class="row">
                    <h2 class="slider__heading"><a style="font-family: Georgia; color: #f55656" href="./newsong">New Song Month></a></h2><br/>
                    <c:forEach var="a" items="${newsong}">
                        <div class="album-detail" style="margin-right: 34px;">
                            <img class="card-img-top" style="background: url('${a.image}') no-repeat center center / cover">
                            <h3><a style="font-family: Arial; font-size: large;" href="songDetail?id=${a.song_ID}">${a.name}</a>
                                <c:forEach items="${requestScope.catename}" var="cname">
                                    <c:if test="${a.category_ID == cname.categoryID}">
                                        <b style="font-family: Courier; font-weight: 10; font-size: medium">(${cname.categoryName}) </b><br/>
                                    </c:if>
                                </c:forEach> 
                                <b style="font-family: Courier; font-weight: 10; font-size: medium">${a.author}</b><br/>
                                <b style="font-family: Courier; font-weight: 10; font-size: medium">${a.t_create}</b>
                            </h3>
                        </div>
                    </c:forEach> 
                </c:if>
            </div><br/><br/>

            <c:if test="${hotsong != null}"> 
                <div class="row">
                    <h2 class="slider__heading" ><a style="font-family: Georgia; color: #f55656" href="./hotsong">Hot Song></a></h2><br/>
                    <c:forEach var="a" items="${hotsong}">
                        <div class="album-detail " style="margin-right: 34px;">
                            <img class="card-img-top" style="background: url('${a.image}') no-repeat center center / cover">
                            <h3><a style="font-family: Arial; font-size: large;" href="songDetail?id=${a.song_ID}">${a.name}</a>
                                <c:forEach items="${requestScope.catename}" var="cname">
                                    <c:if test="${a.category_ID == cname.categoryID}">
                                        <b style="font-family: Courier; font-weight: 10; font-size: medium">(${cname.categoryName}) </b><br/>
                                    </c:if>
                                </c:forEach> 
                                <b style="font-family: Courier; font-weight: 10; font-size: medium">${a.author}</b><br/>
                                <b style="font-family: Courier; font-weight: 10; font-size: medium">${a.t_create}</b>
                            </h3>
                        </div>
                    </c:forEach>  
                </c:if>
            </div><br/><br/>

            <c:if test="${listS != null}"> 
                <div class="row">
                    <h2 class="slider__heading"><a style="font-family: Georgia; color: #f55656" href="./likesong">Most Like Song></a></h2><br/>
                    <c:forEach var="a" items="${listS}">
                        <div class="album-detail " style="margin-right: 34px;">
                            <img class="card-img-top" style="background: url('${a.image}') no-repeat center center / cover">
                            <h3><a style="font-family: Arial; font-size: large" href="songDetail?id=${a.song_ID}">${a.name}</a>
                                <c:forEach items="${requestScope.catename}" var="cname">
                                    <c:if test="${a.category_ID == cname.categoryID}">
                                        <b style="font-family: Courier; font-weight: 10; font-size: medium">(${cname.categoryName}) </b>
                                        <label for="heart" style="color: red">❤ </label><b>${a.totalLike}</b><br/>
                                    </c:if>
                                </c:forEach>
                                
                                <b style="font-family: Courier; font-weight: 10; font-size: medium">${a.author}</b><br/>
                                <b style="font-family: Courier; font-weight: 10; font-size: medium">${a.t_create}</b>
                            </h3>
                            
                        </div>  
                    </c:forEach> 
                </c:if>
            </div>
        </section>

        <section id="recommend">           
            <div class="container">
                <h1 class="text-center">Lựa chọn của Loustic</h1>
                <div class="line center-block"></div>
                <div class="group-tab__name text-center">
                    <h3>Vpop </h3>
                    <h3>Wpop </h3>
                    <h3>Lofi </h3>
                </div>
                <div class="group-tab__content">
                    <div class="tab__container">
                        <div class="tab__heading">
                            <h2>Bài Hát</h2>
                            <div class="btn-group">
                                <div  class="btn btn-option btn-playAll"> <i  class="fas fa-play"> <span>Phát tất cả</span></i></div>
                                <div class="btn btn-option btn-playRandom"> <i class="fas fa-play"><span>Phát ngẫu nhiên</span></i></div>
                            </div>
                        </div>
                        <div class="tab__content vpop-tab active">
                            <div class="container__slide hide-on-mobile">
                                <div class="container__slide-show">
                                    <% index = 0;%>
                                    <c:forEach var="album" items="${albums_Vpop}">
                                        <% index++;%>
                                        <div class="container__slide-item <% switch (index) {
                                                case 1: %>first<% break;
                                                    case 2: %>second<% break;
                                                        case 3: %>third<% break;
                                                            default: %>fourth<% break;
                                                                } %>">
                                            <div class="container__slide-img" style="background: url('${album.image}') no-repeat center center / cover">
                                            </div>
                                        </div>
                                    </c:forEach>
                                </div>
                            </div>

                            <div class="tab__container-list">
                                <div class="play-list scroll-overflow">
                                    <c:forEach var="album" items="${albums_Vpop}">
                                        <div class="song-item" data-path="${album.path}">
                                            <div class="song-content">
                                                <div class="song-img bg-img" style="background-image: url('${album.image}');">
                                                    <div class="song-img--hover"><i class="fas fa-play"></i></div>
                                                </div>
                                                <div class="song-info"> 
                                                    <h2 class="song-name"><a href="songDetail?id=${album.song_ID}">${album.name}</a></h2>
                                                    <div class="song-author">${album.author}</div>
                                                </div>
                                            </div>
                                            <div class="song-duration">${album.duration}</div>
                                            <div class="player__content">
                                                <div class="player__control"> 
                                                    <div class="play"><i class="fas fa-play-circle"></i></div>
                                                    <div class="pause active"><i class="fas fa-pause-circle"></i></div><span class="currenTime">00:00</span>
                                                    <div class="sidebarTime--bg">
                                                        <div class="sidebarTime--current"></div>
                                                    </div><span class="duration">${album.duration}</span>
                                                </div>
                                            </div>
                                            <div class="song-option"> 
                                                <div class="option download"><c:if test="${user != null}"><a href="${album.path}" download></c:if><i class="fal fa-arrow-alt-to-bottom"></i></a></div>
                                                <div <c:if test="${user != null}"> data-user_id="${user.user_ID}" data-song="${album.song_ID}"</c:if> class="option like active"> 
                                                    <a id="likebn" class="fas fa-heart" href="manageLike?songID=${album.song_ID}&operation=like&flag=1" 
                                                       <c:forEach items="${requestScope.checkLikeSong}" var="l">
                                                           <c:if test="${l.song_ID == album.song_ID}">style="color: red; display: block"</c:if>
                                                       </c:forEach>
                                                       > </a>
                                                    <a class="fas fa-heart" 
                                                       <c:forEach items="${requestScope.checkLikeSong}" var="l">
                                                           <c:if test="${l.song_ID == album.song_ID}">hidden</c:if>
                                                       </c:forEach>
                                                           href="manageLike?songID=${album.song_ID}&operation=like&flag=0"> </a>

                                                </div>
                                                <div>
                                                    <a href="addtoplaylists?songID=${album.song_ID}">Add playlist</a>
                                                </div>
                                                <div>
                                                    <a href="addonesongtoplaylist?son=${album.song_ID}"><i class="fas fa-solid fa-ellipsis-h"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </c:forEach>
                                </div>
                            </div>      
                        </div>

                        <div class="tab__content usuk-tab">
                            <div class="container__slide hide-on-mobile">
                                <div class="container__slide-show">
                                    <% index = 0;%>
                                    <c:forEach var="album" items="${albums_USUK}">
                                        <% index++;%>
                                        <div class="container__slide-item <% switch (index) {
                                                case 1: %>first<% break;
                                                    case 2: %>second<% break;
                                                        case 3: %>third<% break;
                                                            default: %>fourth<% break;
                                                                } %>">
                                            <div class="container__slide-img" style="background: url('${album.image}') no-repeat center center / cover">
                                            </div>
                                        </div>
                                    </c:forEach>
                                </div>
                            </div>

                            <div class="tab__container-list">
                                <div class="play-list scroll-overflow"> 
                                    <c:forEach var="album" items="${albums_USUK}">
                                        <div class="song-item" data-path="${album.path}">
                                            <div class="song-content">
                                                <div class="song-img bg-img" style="background-image: url('${album.image}');">
                                                    <div class="song-img--hover"><i class="fas fa-play"></i></div>
                                                </div>
                                                <div class="song-info"> 
                                                    <h2 class="song-name"><a href="songDetail?id=${album.song_ID}">${album.name}</a></h2>
                                                    <div class="song-author">${album.author}</div>
                                                </div>
                                            </div>
                                            <div class="song-duration">${album.duration}</div>
                                            <div class="player__content">
                                                <div class="player__control"> 
                                                    <div class="play"><i class="fas fa-play-circle"></i></div>
                                                    <div class="pause active"><i class="fas fa-pause-circle"></i></div><span class="currenTime">00:00</span>
                                                    <div class="sidebarTime--bg">
                                                        <div class="sidebarTime--current"></div>
                                                    </div><span class="duration">${album.duration}</span>
                                                </div>
                                            </div>
                                            <div class="song-option"> 
                                                <div class="option download"><c:if test="${user != null}"><a href="${album.path}" download></c:if><i class="fal fa-arrow-alt-to-bottom"></i></a></div>
                                                <div <c:if test="${user != null}"> data-user_id="${user.user_ID}" data-song="${album.song_ID}"</c:if> class="option like active"> 
                                                    <a id="likebn" class="fas fa-heart" href="manageLike?songID=${album.song_ID}&operation=like&flag=1" 
                                                       <c:forEach items="${requestScope.checkLikeSong}" var="l">
                                                           <c:if test="${l.song_ID == album.song_ID}">style="color: red; display: block"</c:if>
                                                       </c:forEach>
                                                       > </a>
                                                    <a class="fas fa-heart" 
                                                       <c:forEach items="${requestScope.checkLikeSong}" var="l">
                                                           <c:if test="${l.song_ID == album.song_ID}">hidden</c:if>
                                                       </c:forEach>
                                                           href="manageLike?songID=${album.song_ID}&operation=like&flag=0"> </a>


                                                </div>
                                                <div>
                                                    <a href="addtoplaylists?songID=${album.song_ID}">Add playlist</a>
                                                </div>
                                                <div>
                                                    <a href="addonesongtoplaylist?son=${album.song_ID}"><i class="fas fa-solid fa-ellipsis-h"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </c:forEach>
                                </div>
                            </div>
                        </div>

                        <div class="tab__content lofi-tab">
                            <div class="container__slide hide-on-mobile">
                                <div class="container__slide-show">
                                    <% index = 0;%>
                                    <c:forEach var="album" items="${albums_Lofi}">
                                        <% index++;%>
                                        <div class="container__slide-item <% switch (index) {
                                                case 1: %>first<% break;
                                                    case 2: %>second<% break;
                                                        case 3: %>third<% break;
                                                            default: %>fourth<% break;
                                                                }%>">
                                            <div class="container__slide-img" style="background: url('${album.image}') no-repeat center center / cover">
                                            </div>
                                        </div>
                                    </c:forEach>
                                </div>
                            </div>

                            <div class="tab__container-list">
                                <div class="play-list scroll-overflow">
                                    <c:forEach var="album" items="${albums_Lofi}">
                                        <div class="song-item" data-path="${album.path}">
                                            <div class="song-content">
                                                <div class="song-img bg-img" style="background-image: url('${album.image}');">
                                                    <div class="song-img--hover"><i class="fas fa-play"></i></div>
                                                </div>
                                                <div class="song-info"> 
                                                    <h2 class="song-name"><a href="songDetail?id=${album.song_ID}">${album.name}</a></h2>
                                                    <div class="song-author">${album.author}</div>
                                                </div>
                                            </div>
                                            <div class="song-duration">${album.duration}</div>
                                            <div class="player__content">
                                                <div class="player__control"> 
                                                    <div class="play"><i class="fas fa-play-circle"></i></div>
                                                    <div class="pause active"><i class="fas fa-pause-circle"></i></div><span class="currenTime">00:00</span>
                                                    <div class="sidebarTime--bg">
                                                        <div class="sidebarTime--current"></div>
                                                    </div><span class="duration">${album.duration}</span>
                                                </div>
                                            </div>
                                            <div class="song-option"> 
                                                <div class="option download"><c:if test="${user != null}"><a href="${album.path}" download></c:if><i class="fal fa-arrow-alt-to-bottom"></i></a></div>
                                                <div <c:if test="${user != null}"> data-user_id="${user.user_ID}" data-song="${album.song_ID}"</c:if> class="option like active"> 
                                                    <a id="likebn" class="fas fa-heart" href="manageLike?songID=${album.song_ID}&operation=like&flag=1" 
                                                       <c:forEach items="${requestScope.checkLikeSong}" var="l">
                                                           <c:if test="${l.song_ID == album.song_ID}">style="color: red; display: block"</c:if>
                                                       </c:forEach>
                                                       > </a>
                                                    <a class="fas fa-heart" 
                                                       <c:forEach items="${requestScope.checkLikeSong}" var="l">
                                                           <c:if test="${l.song_ID == album.song_ID}">hidden</c:if>
                                                       </c:forEach>
                                                           href="manageLike?songID=${album.song_ID}&operation=like&flag=0"> </a>


                                                </div>
                                                <div>
                                                    <a href="addtoplaylists?songID=${album.song_ID}">Add playlist</a>
                                                </div>
                                                <div>
                                                    <a href="addonesongtoplaylist?son=${album.song_ID}"><i class="fas fa-solid fa-ellipsis-h"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </c:forEach>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section id="newletter">
            <div class="jarallax bg-overlay bg-img" style="background-image: url('./img/core-img/jarallax.jpg');">
                <div class="container newletter__container">
                    <div class="newletter__content">
                        <h1>Sign Up To Newsletter</h1>
                        <p>Subscribe to receive info on our latest news and episodes</p>
                    </div>

                    <div class="newletter__subcribe">
                        <form action="sendnew" method="get">
                            <input type="text" placeholder="Your Email" name="email">
                            <input type="submit" value="SUBCRIBE">
                        </form>
                    </div>
                </div>
            </div>
        </section>
        <section id="footer">
            <div class="container footer__container" style="display: flex">
                <div class="footer__about"> 
                    <h2>About Us</h2>
                    <p>It is a long established fact that a reader will be distracted by the readable content.</p>
                    <p>&copy; Copyright &copy; 2022 <span>Group Four</span></p>
                </div>
                <ul class="footer__categories"> 
                    <h2>Categories</h2>
                    <li><a href="#" style="color: black">Entrepreneurship </a></li>
                    <li><a href="#" style="color: black">Media </a></li>
                    <li><a href="#" style="color: black">Tech </a></li>
                    <li>   <a href="#" style="color: black">Tutorials </a></li>
                </ul>
                <div class="footer_social"> 
                    <h2>Follow Us</h2>
                    <ul class="media">
                        <li><a class="fab fa-facebook" href="#" style="color: black"> </a></li>
                        <li><a class="fab fa-twitter" href="#" style="color: black"> </a></li>
                        <li><a class="fab fa-pinterest" href="#" style="color: black"> </a></li>
                        <li><a class="fab fa-instagram" href="#" style="color: black"> </a></li>
                        <li><a class="fab fa-youtube" href="#" style="color: black"> </a></li>
                    </ul>
                    <ul class="store"> 
                        <li> <a href=""><img src="./img/core-img/app-store.png" alt=""></a></li>
                        <li> <a href=""><img src="./img/core-img/google-play.png" alt=""></a></li>
                    </ul>
                </div>
            </div>
        </section>
        <c:if test="${user != null}">
            <section class="bg-overlay" id="user">
                <div class="user-container">
                    <div class="user-content">
                        <form action="./user/update?id=${user.user_ID}" method="POST">
                            <input type="text" name="url" value="/home" hidden>
                            <div class="user__base-info"> 
                                <div class="user_avatar bg-img" style="background-image: url('./img/core-img/avatar-default-1.jpg');"></div>
                                <div class="user__fullname"> 
                                    <div class="first_name"> 
                                        <div class="title">First name: </div>
                                        <div class="value"> <span class="active">${user.first_name}</span>
                                            <input class="edit_value" type="text" name="first-name" value="${user.first_name}" placeholder="First-name ...">
                                        </div><i class="fas fa-edit icon-setting active"> </i><i class="fal fa-window-close icon-setting-close"></i>
                                    </div>
                                    <div class="last_name"> 
                                        <div class="title">Last name: </div>
                                        <div class="value"> <span class="active">${user.last_name}</span>
                                            <input class="edit_value" type="text" name="last-name" value="${user.last_name}" placeholder="Last-name ...">
                                        </div><i class="fas fa-edit icon-setting active"> </i><i class="fal fa-window-close icon-setting-close"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="user__more-info">
                                <div class="user_username">
                                    <div class="title"> <i class="fas fa-user icon"></i><span>Username: </span><i class="fas fa-edit icon-setting active"></i><i class="fal fa-window-close icon-setting-close"></i></div>
                                    <div class="value"> 
                                        <div class="edit_value"></div><span class="active">${user.username}</span>
                                    </div>
                                </div>
                                <div class="user_password">
                                    <div class="title"> <i class="fas fa-key icon"></i><span>Password: </span><i class="fas fa-edit icon-setting active"></i><i class="fal fa-window-close icon-setting-close"></i></div>
                                    <div class="value"> <span class="active">********</span>
                                        <div class="edit_password edit_value">
                                            <input type="password" name="old-password" placeholder="Mật khẩu cũ">
                                            <input type="password" name="new-password" placeholder="Mật khẩu mới">
                                            <input type="password" name="verify-password" placeholder="Nhập lại mật khẩu mới">
                                        </div>
                                    </div>
                                </div>
                                <div class="user_email"> 
                                    <div class="title"> <i class="fas fa-envelope-open icon"></i><span>Email: </span><i class="fas fa-edit icon-setting active"></i><i class="fal fa-window-close icon-setting-close"></i></div>
                                    <div class="value"> <span class="active">${user.email}</span>
                                        <input class="edit_value" type="email" name="email" value="${user.email}" placeholder="Email ....">
                                    </div>
                                </div>
                                <div class="user_time-create">
                                    <div class="title"> <i class="fas fa-calendar-star icon"></i><span>Time create: </span><i class="fas fa-edit icon-setting active"></i><i class="fal fa-window-close icon-setting-close"></i></div>
                                    <div class="value"> 
                                        <div class="edit_value"></div><span class="active">${user.t_create}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="user__icon-summit"> 
                                <label for="user__edit_summit"><i class="fas fa-check-circle edit-summit"></i></label>
                                <input type="submit" hidden id="user__edit_summit">
                            </div>
                        </form>
                    </div>
                </div>
            </section>
        </c:if> 
        <section id="toast"></section>
        <audio id="audio" src="">    </audio>
        <script src="./node_modules/jarallax/dist/jarallax.min.js"></script>
        <script src="./js/app.js"></script>
        <script src="./js/jquery-3.2.1.min.js"></script>
        <script src="./js/handle_ajax.js"></script>
        <script src="./js/handle_toast.js"></script>
    </body>
</html>

