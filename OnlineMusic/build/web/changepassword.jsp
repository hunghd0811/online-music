<%-- 
    Document   : changepassword
    Created on : May 19, 2022, 11:22:54 PM
    Author     : anhqu
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Change Password </title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="http://netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet">
    <style type="text/css">
        
    </style>
    <script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
    <script src="http://netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1 style="text-align: center">Change Password</h1>
            </div>
        </div>
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3">
            <p class="text-center"> Your password cannot be the same as your username.</p>
            <c:set var="user" value="${requestScope.user}"/>
            <h4 style="color: red">${mess}</h4>
            <c:if test="${mess1 == 1}">
               <h4 style="color: red"> Password not match. Please send email again!!</h4>
               <a href="forgetpassword.jsp">Click Here!</a>
            </c:if>
            <form method="post" id="passwordForm" action="resetpassword">
                <input type="hidden"  value="${user.user_ID}" name="id">

                <input type="text" class="input-lg form-control" value="${user.email}" name="email"  readonly autocomplete="off">

                <input type="password" class="input-lg form-control" name="password"  placeholder="New Password" autocomplete="off">

                <input type="password" class="input-lg form-control" name="repassword"  placeholder="Repeat Password" autocomplete="off">
         
                <input type="submit" class="col-xs-12 btn btn-primary btn-load btn-lg"  >
            </form>
                </div><!--/col-sm-6-->
                </div><!--/row-->
                </div>

</body>
</html>

