package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class profile_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("<!DOCTYPE html>\r\n");
      out.write("<html>\r\n");
      out.write("    <head>\r\n");
      out.write("        <link href=\"https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css\" rel=\"stylesheet\" integrity=\"sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC\" crossorigin=\"anonymous\">\r\n");
      out.write("        <script src=\"https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js\" integrity=\"sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM\" crossorigin=\"anonymous\"></script>\r\n");
      out.write("        <script src=\"https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js\" integrity=\"sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p\" crossorigin=\"anonymous\"></script>\r\n");
      out.write("        <script src=\"https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js\" integrity=\"sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF\" crossorigin=\"anonymous\"></script>\r\n");
      out.write("        <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js\"></script>\r\n");
      out.write("\r\n");
      out.write("        <script src=\"js/profile.js\"></script>\r\n");
      out.write("        <link href=\"css/profile.css\" rel=\"stylesheet\" type=\"text/css\"/>\r\n");
      out.write("        <!--        <script>\r\n");
      out.write("                    $(document).ready(function() {\r\n");
      out.write("                    $('#new-password, #verify-password').on('keyup', function () {\r\n");
      out.write("                    if ($('#new-password').val() === $('#verify-password').val()) {\r\n");
      out.write("                    $('#message').html('Mật khẩu tương xứng').css('color', 'green');\r\n");
      out.write("                    } else\r\n");
      out.write("                            $('#message').html('Mật khẩu không tương xứng').css('color', 'red');\r\n");
      out.write("                    });\r\n");
      out.write("                    }\r\n");
      out.write("                    );\r\n");
      out.write("                </script>-->\r\n");
      out.write("    </head>\r\n");
      out.write("    <body>\r\n");
      out.write("        <div class=\"container-xl px-4 mt-4\">\r\n");
      out.write("            <!-- Account page navigation-->\r\n");
      out.write("\r\n");
      out.write("            <hr class=\"mt-0 mb-4\">\r\n");
      out.write("            <div class=\"row\">\r\n");
      out.write("                <div class=\"col-xl-4\">\r\n");
      out.write("                    <!-- Profile picture card-->\r\n");
      out.write("                    <div class=\"card mb-4 mb-xl-0\">\r\n");
      out.write("                        <div class=\"card-header\">Profile Picture</div>\r\n");
      out.write("                        <div class=\"card-body text-center\">\r\n");
      out.write("                            <!-- Profile picture image-->\r\n");
      out.write("                            <img class=\"img-account-profile rounded-circle mb-2\" src=\"http://bootdey.com/img/Content/avatar/avatar1.png\" alt=\"\">\r\n");
      out.write("                            <!-- Profile picture help block-->\r\n");
      out.write("                            <div class=\"small font-italic text-muted mb-4\">JPG or PNG no larger than 5 MB</div>\r\n");
      out.write("                            <!-- Profile picture upload button-->\r\n");
      out.write("                            <button class=\"btn btn-primary\" type=\"button\">Upload new image</button>\r\n");
      out.write("                        </div>\r\n");
      out.write("                    </div>\r\n");
      out.write("                </div>\r\n");
      out.write("                <div class=\"col-xl-8\">\r\n");
      out.write("                    <!-- Account details card-->\r\n");
      out.write("                    <div class=\"card mb-4\">\r\n");
      out.write("                        <div class=\"card-header\">Profile Detail</div>\r\n");
      out.write("                        <div class=\"card-body\">\r\n");
      out.write("                            <form id=\"profile_form\" action=\"UpdateProfile\" method=\"post\">\r\n");
      out.write("                                <!-- Form Row-->\r\n");
      out.write("                                <div class=\"row gx-3 mb-3\">\r\n");
      out.write("                                    <!-- Form Group (first name)-->\r\n");
      out.write("                                    <div class=\"col-md-6\">\r\n");
      out.write("                                        <label class=\"small mb-1\" for=\"inputFirstName\">First name</label>\r\n");
      out.write("                                        <input class=\"form-control\" id=\"inputFirstName\" name=\"firstName\" type=\"text\" placeholder=\"Enter your first name\" value=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${user.first_name}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("\">\r\n");
      out.write("                                    </div>\r\n");
      out.write("                                    <!-- Form Group (last name)-->\r\n");
      out.write("                                    <div class=\"col-md-6\">\r\n");
      out.write("                                        <label class=\"small mb-1\" for=\"inputLastName\">Last name</label>\r\n");
      out.write("                                        <input class=\"form-control\" id=\"inputLastName\" name=\"lastName\" type=\"text\" placeholder=\"Enter your last name\" value=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${user.last_name}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("\">\r\n");
      out.write("                                    </div>\r\n");
      out.write("                                </div>\r\n");
      out.write("                                <div class=\"mb-3\">\r\n");
      out.write("                                    <label class=\"small mb-2\" for=\"gender\">Gender: </label>\r\n");
      out.write("                                    <input  type=\"radio\" name=\"gender\" value=\"Male\" ");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${user.gender?\"checked\":\"\"}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write(" checked=\"checked\"/>Male\r\n");
      out.write("                                    <input type=\"radio\" name=\"gender\" value=\"Female\" ");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${user.gender?\"\":\"checked\"}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("/>Female\r\n");
      out.write("                                </div>\r\n");
      out.write("                                <!-- Form Group (username)-->\r\n");
      out.write("                                <div class=\"mb-3\">\r\n");
      out.write("                                    <label class=\"small mb-1\" for=\"inputUsername\">Username </label>\r\n");
      out.write("                                    <input class=\"form-control\" id=\"inputUsername\" type=\"text\" placeholder=\"Enter your username\" value=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${user.username}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("\" readonly=\"\">\r\n");
      out.write("                                </div>\r\n");
      out.write("                                <div class=\"mb-3\">\r\n");
      out.write("                                    <label class=\"small mb-1\" for=\"inputPassword\">Password </label>\r\n");
      out.write("\r\n");
      out.write("                                    <input class=\"form-control\" id=\"inputPassword\" required=\"\" type=\"password\" name=\"old-password\" placeholder=\"Old Password\">\r\n");
      out.write("                                    <input class=\"form-control\" id=\"inputPassword\" required=\"\" type=\"password\" id=\"new-password\" name=\"new-password\" placeholder=\"New Password\" onChange=\"onChange()\">\r\n");
      out.write("                                    <input class=\"form-control\" id=\"inputPassword\" required=\"\" type=\"password\" id=\"verify-password\" name=\"verify-password\" placeholder=\"Verify Password\" onChange=\"onChange()\">\r\n");
      out.write("                                    <span id='message'></span>\r\n");
      out.write("\r\n");
      out.write("                                </div>\r\n");
      out.write("\r\n");
      out.write("                                <!-- Form Group (email address)-->\r\n");
      out.write("                                <div class=\"mb-3\">\r\n");
      out.write("                                    <label class=\"small mb-1\" for=\"inputEmailAddress\">Email address</label>\r\n");
      out.write("                                    <input class=\"form-control\" id=\"inputEmailAddress\" required=\"\" name=\"email\" type=\"email\" placeholder=\"Enter your email address\" value=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${user.email}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("\">\r\n");
      out.write("                                </div>\r\n");
      out.write("                                <div class=\"mb-3\">\r\n");
      out.write("                                    <label class=\"small mb-1\" for=\"inputEmailAddress\">Address</label>\r\n");
      out.write("                                    <input class=\"form-control\" id=\"inputEmailAddress\" required=\"\" name=\"address\" placeholder=\"Enter your address\" value=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${user.address}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("\">\r\n");
      out.write("                                </div>\r\n");
      out.write("                                <!-- Form Row-->\r\n");
      out.write("                                <div class=\"row gx-3 mb-3\">\r\n");
      out.write("                                    <!-- Form Group (phone number)-->\r\n");
      out.write("                                    <!--                                    <div class=\"col-md-6\">\r\n");
      out.write("                                                                            <label class=\"small mb-1\" for=\"inputPhone\">Phone number</label>\r\n");
      out.write("                                                                            <input class=\"form-control\" id=\"inputPhone\" type=\"tel\" placeholder=\"Enter your phone number\" value=\"555-123-4567\">\r\n");
      out.write("                                                                        </div>-->\r\n");
      out.write("                                    <!-- Form Group (birthday)-->\r\n");
      out.write("                                    <div class=\"col-md-6\">\r\n");
      out.write("                                        <label class=\"small mb-1\" for=\"inputBirthday\">Birthday</label>\r\n");
      out.write("                                        <input class=\"form-control\" id=\"inputBirthday\" required=\"\" type=\"date\" name=\"birthday\" placeholder=\"Enter your birthday\" value=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${user.dob}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("\">\r\n");
      out.write("                                    </div>\r\n");
      out.write("                                </div>\r\n");
      out.write("                                <!-- Save changes button-->\r\n");
      out.write("                                <button class=\"btn btn-primary\" type=\"submit\">Save changes</button>\r\n");
      out.write("\r\n");
      out.write("                                <div>\r\n");
      out.write("                                    ");

                                        boolean error = false;
                                        if (request.getAttribute("erroOldPass") != null) {
                                            error = Boolean.parseBoolean(request.getAttribute("erroOldPass").toString());
                                        }
                                        if (error) {
      out.write("\r\n");
      out.write("                                    <p style=\"color: red\">mật khẩu cũ không đúng!</p>\r\n");
      out.write("                                    ");
}
                                    
      out.write("\r\n");
      out.write("                                </div>\r\n");
      out.write("                            </form>\r\n");
      out.write("                            <br>\r\n");
      out.write("                            <a href=\"home.jsp\"><button class=\"btn btn-primary\" >Back To Home</button></a>\r\n");
      out.write("                        </div>\r\n");
      out.write("                    </div>\r\n");
      out.write("                </div>\r\n");
      out.write("            </div>\r\n");
      out.write("        </div>\r\n");
      out.write("    </c:if>\r\n");
      out.write("</body>\r\n");
      out.write("</html>\r\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
