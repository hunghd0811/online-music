package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class signup_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("<link href=\"//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css\" rel=\"stylesheet\" id=\"bootstrap-css\">\r\n");
      out.write("<script src=\"//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js\"></script>\r\n");
      out.write("<script src=\"//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js\"></script>\r\n");
      out.write("<!------ Include the above in your HEAD tag ---------->\r\n");
      out.write("\r\n");
      out.write("<!DOCTYPE html>\r\n");
      out.write("<html>\r\n");
      out.write("    <head>\r\n");
      out.write("        <title>SignUp Page</title>\r\n");
      out.write("        <!--Made with love by Mutiullah Samim -->\r\n");
      out.write("\r\n");
      out.write("        <!--Bootsrap 4 CDN-->\r\n");
      out.write("        <link rel=\"stylesheet\" href=\"https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css\" integrity=\"sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO\" crossorigin=\"anonymous\">\r\n");
      out.write("\r\n");
      out.write("        <!--Fontawesome CDN-->\r\n");
      out.write("        <link rel=\"stylesheet\" href=\"https://use.fontawesome.com/releases/v5.3.1/css/all.css\" integrity=\"sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU\" crossorigin=\"anonymous\">\r\n");
      out.write("\r\n");
      out.write("        <!--Custom styles-->\r\n");
      out.write("        <link rel=\"stylesheet\" href=\"./css/login.css\">\r\n");
      out.write("    </head>\r\n");
      out.write("   \r\n");
      out.write("    <body style=\"background-image: url('./img/bg-gif/gif-22.gif');\">\r\n");
      out.write("         <style>\r\n");
      out.write("        .btn-primary {\r\n");
      out.write("            background-color: #f1b0b7;\r\n");
      out.write("            border-color: #f1b0b7;\r\n");
      out.write("        }\r\n");
      out.write("        .card{\r\n");
      out.write("            height: 700px;\r\n");
      out.write("        }\r\n");
      out.write("        .input-group {\r\n");
      out.write("            color: white;\r\n");
      out.write("        }\r\n");
      out.write("        .login_btn {\r\n");
      out.write("            background-color: #f1b0b7;\r\n");
      out.write("        }\r\n");
      out.write("        .input-group-prepend span {\r\n");
      out.write("            background-color: #f1b0b7;\r\n");
      out.write("        }\r\n");
      out.write("        .social_icon span {\r\n");
      out.write("            color: #f1b0b7;\r\n");
      out.write("        }\r\n");
      out.write("    </style>\r\n");
      out.write("        <div class=\"container\">\r\n");
      out.write("            <div class=\"d-flex justify-content-center h-100\">\r\n");
      out.write("                <div style=\"height: 35rem;\" class=\"card\">\r\n");
      out.write("                    <div class=\"card-header\">\r\n");
      out.write("                        <h3>Sign Up</h3>\r\n");
      out.write("                        <div class=\"d-flex justify-content-end social_icon\">\r\n");
      out.write("                            <span><i class=\"fab fa-facebook-square\"></i></span>\r\n");
      out.write("                            <span><i class=\"fab fa-google-plus-square\"></i></span>\r\n");
      out.write("                            <span><i class=\"fab fa-twitter-square\"></i></span>\r\n");
      out.write("                        </div>\r\n");
      out.write("                    </div>\r\n");
      out.write("                    <div class=\"card-body\" style=\"height: 600px\">\r\n");
      out.write("                        <form action=\"signup\" method=\"post\">\r\n");
      out.write("                            <p class=\"text-success\">");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${requestScope.success}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("</p>\r\n");
      out.write("                            <p class=\"text-danger\">");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${requestScope.error}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("</p>\r\n");
      out.write("                            <p class=\"text-danger\">");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${requestScope.error1}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("</p>\r\n");
      out.write("                            <div class=\"input-group form-group\">\r\n");
      out.write("                                <div class=\"input-group-prepend\">\r\n");
      out.write("                                    <span class=\"input-group-text\"><i class=\"fas fa-user\"></i></span>\r\n");
      out.write("                                </div>\r\n");
      out.write("                                <input type=\"text\" name=\"username\" class=\"form-control\" placeholder=\"Username\" required>\r\n");
      out.write("\r\n");
      out.write("                            </div>\r\n");
      out.write("                            <div class=\"input-group form-group\">\r\n");
      out.write("                                <div class=\"input-group-prepend\">\r\n");
      out.write("                                    <span class=\"input-group-text\"><i class=\"fas fa-solid fa-envelope\"></i></span>\r\n");
      out.write("                                </div>\r\n");
      out.write("                                <input type=\"email\" name=\"email\" class=\"form-control\" placeholder=\"Email\" required>\r\n");
      out.write("                            </div>   \r\n");
      out.write("                            <div class=\"input-group form-group\">\r\n");
      out.write("                                <div class=\"input-group-prepend\">\r\n");
      out.write("                                    <span class=\"input-group-text\"><i class=\"fas fa-key\"></i></span>\r\n");
      out.write("                                </div>\r\n");
      out.write("                                <input type=\"password\" name=\"password\" class=\"form-control\" placeholder=\"Password\" required>\r\n");
      out.write("                            </div>\r\n");
      out.write("                            <div class=\"input-group form-group\">\r\n");
      out.write("                                <div class=\"input-group-prepend\">\r\n");
      out.write("                                    <span class=\"input-group-text\"><i class=\"fas fa-key\"></i></span>\r\n");
      out.write("                                </div>\r\n");
      out.write("                                <input type=\"password\" name=\"repassword\" class=\"form-control\" placeholder=\"Confirm Password\" required>\r\n");
      out.write("                            </div>                                                 \r\n");
      out.write("                            <div class=\"form-group\">\r\n");
      out.write("                                <button type=\"submit\" class=\"btn btn-primary btn-lg btn-block\">SignUp</button>\r\n");
      out.write("                            </div>\r\n");
      out.write("                        </form>\r\n");
      out.write("                    </div>\r\n");
      out.write("                    <div class=\"card-footer\">\r\n");
      out.write("                        <div class=\"d-flex justify-content-center links\">\r\n");
      out.write("                            Already have an account?<a href=\"login.jsp\">Sign In</a>\r\n");
      out.write("                        </div>\r\n");
      out.write("                    </div>\r\n");
      out.write("                </div>\r\n");
      out.write("            </div>\r\n");
      out.write("        </div>\r\n");
      out.write("    </body>\r\n");
      out.write("</html>\r\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
