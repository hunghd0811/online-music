package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class adminProfileDetails_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <link href=\"https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css\" rel=\"stylesheet\" integrity=\"sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC\" crossorigin=\"anonymous\">\n");
      out.write("        <script src=\"https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js\" integrity=\"sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM\" crossorigin=\"anonymous\"></script>\n");
      out.write("        <script src=\"https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js\" integrity=\"sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p\" crossorigin=\"anonymous\"></script>\n");
      out.write("        <script src=\"https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js\" integrity=\"sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF\" crossorigin=\"anonymous\"></script>\n");
      out.write("        <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js\"></script>\n");
      out.write("\n");
      out.write("        <script src=\"js/profile.js\"></script>\n");
      out.write("        <link href=\"css/profile.css\" rel=\"stylesheet\" type=\"text/css\"/>\n");
      out.write("        <!--        <script>\n");
      out.write("                    $(document).ready(function() {\n");
      out.write("                    $('#new-password, #verify-password').on('keyup', function () {\n");
      out.write("                    if ($('#new-password').val() === $('#verify-password').val()) {\n");
      out.write("                    $('#message').html('Mật khẩu tương xứng').css('color', 'green');\n");
      out.write("                    } else\n");
      out.write("                            $('#message').html('Mật khẩu không tương xứng').css('color', 'red');\n");
      out.write("                    });\n");
      out.write("                    }\n");
      out.write("                    );\n");
      out.write("                </script>-->\n");
      out.write("    </head>\n");
      out.write("    <body>\n");
      out.write("        <div class=\"container\">\n");
      out.write("            <!-- Account page navigation-->\n");
      out.write("\n");
      out.write("\n");
      out.write("            <div class=\"row\">\n");
      out.write("\n");
      out.write("                <section class=\"vh-100\" style=\"background-color: #F7F4E9;\">\n");
      out.write("\n");
      out.write("                    <div class=\"container py-5 h-100\">\n");
      out.write("                        <div class=\"row d-flex justify-content-center  h-100\">\n");
      out.write("                            <div class=\"col col-lg-6 mb-4 mb-lg-0\">\n");
      out.write("                                <div class=\"card mb-3\" style=\"border-radius: .5rem;\">\n");
      out.write("                                    <div class=\"row g-0\">\n");
      out.write("                                        <div class=\"col-md-4 gradient-custom text-center text-white\"\n");
      out.write("                                             style=\"border-top-left-radius: .5rem; border-bottom-left-radius: .5rem;\">\n");
      out.write("                                            <img src=\"https://www.google.com/url?sa=i&url=https%3A%2F%2Ffreenice.net%2Fanh-avatar-dep-doc-chat-ngau-lam-hinh-dai-dien-fb%2F&psig=AOvVaw3AOjnFqVWumZvdC8zTGmIL&ust=1653195174336000&source=images&cd=vfe&ved=0CAwQjRxqFwoTCPj7xY_m7_cCFQAAAAAdAAAAABAE\"\n");
      out.write("                                                 alt=\"Avatar\" class=\"img-fluid my-5\" style=\"width: 80px;\" />\n");
      out.write("                                            <h5>Marie Horwitz</h5>\n");
      out.write("                                            <p>Web Designer</p>\n");
      out.write("                                            <i class=\"far fa-edit mb-5\"></i>\n");
      out.write("                                        </div>\n");
      out.write("                                        <div class=\"col-md-8\">\n");
      out.write("                                            <div class=\"card-body p-4\">\n");
      out.write("                                                <h6>Information</h6>\n");
      out.write("                                                <hr class=\"mt-0 mb-4\">\n");
      out.write("                                                <div class=\"row pt-1\">\n");
      out.write("                                                    <div class=\"col-6 mb-3\">\n");
      out.write("                                                        <h6>User Name</h6>\n");
      out.write("                                                        <p class=\"text-muted\">info@example.com</p>\n");
      out.write("                                                    </div>\n");
      out.write("                                                    <div class=\"col-6 mb-3\">\n");
      out.write("                                                        <h6>Password</h6>\n");
      out.write("                                                        <p class=\"text-muted\">123 456 789</p>\n");
      out.write("                                                    </div>\n");
      out.write("                                                </div>\n");
      out.write("                                                <div class=\"row pt-1\">\n");
      out.write("                                                    <div class=\"col-6 mb-3\">\n");
      out.write("                                                        <h6>First Name</h6>\n");
      out.write("                                                        <p class=\"text-muted\">Lorem ipsum</p>\n");
      out.write("                                                    </div>\n");
      out.write("                                                    <div class=\"col-6 mb-3\">\n");
      out.write("                                                        <h6>Last Name</h6>\n");
      out.write("                                                        <p class=\"text-muted\">Dolor sit amet</p>\n");
      out.write("                                                    </div>\n");
      out.write("                                                </div>\n");
      out.write("                                                <div class=\"row pt-1\">\n");
      out.write("                                                    <div class=\"col-6 mb-3\">\n");
      out.write("                                                        <h6>Time Create</h6>\n");
      out.write("                                                        <p class=\"text-muted\">Lorem ipsum</p>\n");
      out.write("                                                    </div>\n");
      out.write("                                                    <div class=\"col-6 mb-3\">\n");
      out.write("                                                        <h6>Last Online</h6>\n");
      out.write("                                                        <p class=\"text-muted\">Dolor sit amet</p>\n");
      out.write("                                                    </div>\n");
      out.write("                                                </div>\n");
      out.write("                                                <div class=\"row pt-1\">\n");
      out.write("                                                    <div class=\"col-6 mb-3\">\n");
      out.write("                                                        <h6>Email</h6>\n");
      out.write("                                                        <p class=\"text-muted\">Lorem ipsum</p>\n");
      out.write("                                                    </div>\n");
      out.write("                                                    <div class=\"col-6 mb-3\">\n");
      out.write("                                                        <h6>Address</h6>\n");
      out.write("                                                        <p class=\"text-muted\">Dolor sit amet</p>\n");
      out.write("                                                    </div>\n");
      out.write("                                                </div>\n");
      out.write("                                                <div class=\"row pt-1\">\n");
      out.write("                                                    <div class=\"col-6 mb-3\">\n");
      out.write("                                                        <h6>Gender</h6>\n");
      out.write("                                                        <p class=\"text-muted\">Lorem ipsum</p>\n");
      out.write("                                                    </div>\n");
      out.write("                                                    <div class=\"col-6 mb-3\">\n");
      out.write("                                                        <h6>Date Of Birth</h6>\n");
      out.write("                                                        <p class=\"text-muted\">Dolor sit amet</p>\n");
      out.write("                                                    </div>\n");
      out.write("                                                </div>\n");
      out.write("                                                <div class=\"row pt-1 d-flex justify-content-center\">\n");
      out.write("                                                    <div class=\"col-6 mb-3\">    \n");
      out.write("                                                        <a href=\"/SE1607_Group4_OnlineMusic/home\"><button class=\"btn btn-primary\" >Back To Home</button></a>\n");
      out.write("                                                    </div>\n");
      out.write("\n");
      out.write("                                                </div>\n");
      out.write("                                            </div>\n");
      out.write("                                        </div>\n");
      out.write("                                    </div>\n");
      out.write("                                </div>\n");
      out.write("                            </div>\n");
      out.write("                        </div>\n");
      out.write("                    </div>\n");
      out.write("                </section>\n");
      out.write("\n");
      out.write("\n");
      out.write("            </div>\n");
      out.write("        </div>\n");
      out.write("    </body>\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
