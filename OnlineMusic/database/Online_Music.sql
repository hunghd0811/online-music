USE [master]
GO
/****** Object:  Database [Online_Music]    Script Date: 26/07/2022 3:27:57 CH ******/
CREATE DATABASE [Online_Music]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Online_Music', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\Online_Music.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'Online_Music_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\Online_Music_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [Online_Music] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Online_Music].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Online_Music] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Online_Music] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Online_Music] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Online_Music] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Online_Music] SET ARITHABORT OFF 
GO
ALTER DATABASE [Online_Music] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [Online_Music] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Online_Music] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Online_Music] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Online_Music] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Online_Music] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Online_Music] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Online_Music] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Online_Music] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Online_Music] SET  ENABLE_BROKER 
GO
ALTER DATABASE [Online_Music] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Online_Music] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Online_Music] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Online_Music] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Online_Music] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Online_Music] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Online_Music] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Online_Music] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [Online_Music] SET  MULTI_USER 
GO
ALTER DATABASE [Online_Music] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Online_Music] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Online_Music] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Online_Music] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [Online_Music] SET DELAYED_DURABILITY = DISABLED 
GO
USE [Online_Music]
GO
/****** Object:  Table [dbo].[Albums]    Script Date: 26/07/2022 3:27:57 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Albums](
	[album_ID] [int] IDENTITY(1,1) NOT NULL,
	[image] [varchar](64) NULL,
	[description] [varchar](1000) NULL,
	[user_ID] [int] NULL,
	[name] [nvarchar](64) NULL,
	[t_create] [date] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[album_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Albums_Songs]    Script Date: 26/07/2022 3:27:57 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Albums_Songs](
	[album_ID] [int] NOT NULL,
	[song_ID] [int] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Background]    Script Date: 26/07/2022 3:27:57 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Background](
	[background_ID] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](16) NOT NULL,
	[path] [varchar](255) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[background_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Categories]    Script Date: 26/07/2022 3:27:57 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Categories](
	[category_ID] [int] IDENTITY(1,1) NOT NULL,
	[categorydetail_ID] [int] NOT NULL,
	[name] [varchar](32) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[category_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CategoryDetails]    Script Date: 26/07/2022 3:27:57 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CategoryDetails](
	[categorydetail_ID] [int] IDENTITY(1,1) NOT NULL,
	[categorydetail_name] [nvarchar](64) NULL,
PRIMARY KEY CLUSTERED 
(
	[categorydetail_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Comments]    Script Date: 26/07/2022 3:27:57 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Comments](
	[comment_ID] [int] IDENTITY(1,1) NOT NULL,
	[song_ID] [int] NULL,
	[user_ID] [int] NULL,
	[comment] [nvarchar](max) NULL,
	[status] [bit] NULL,
	[like] [bit] NULL,
	[totallikes] [int] NULL,
	[t_create] [smalldatetime] NOT NULL,
	[t_lastUpdate] [smalldatetime] NULL,
	[report] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[comment_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[History]    Script Date: 26/07/2022 3:27:57 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[History](
	[history_ID] [int] IDENTITY(1,1) NOT NULL,
	[song_name] [nvarchar](64) NULL,
	[t_lastUpdate] [smalldatetime] NULL,
	[user_ID] [int] NULL,
	[song_ID] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[HistoryView]    Script Date: 26/07/2022 3:27:57 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HistoryView](
	[history_ID] [int] IDENTITY(1,1) NOT NULL,
	[t_create] [smalldatetime] NOT NULL,
	[user_ID] [int] NULL,
	[song_ID] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[history_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Liked]    Script Date: 26/07/2022 3:27:57 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Liked](
	[like_ID] [int] IDENTITY(1,1) NOT NULL,
	[t_lastUpdate] [smalldatetime] NOT NULL,
	[user_ID] [int] NULL,
	[song_ID] [int] NULL,
	[comment_ID] [int] NULL,
	[like] [bit] NULL,
	[flag] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[like_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Lyrics]    Script Date: 26/07/2022 3:27:57 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Lyrics](
	[lyrics_ID] [int] IDENTITY(1,1) NOT NULL,
	[content] [nvarchar](max) NULL,
	[time] [varchar](6) NULL,
	[song_ID] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[lyrics_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PlayLists]    Script Date: 26/07/2022 3:27:57 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PlayLists](
	[playlist_ID] [int] IDENTITY(1,1) NOT NULL,
	[user_ID] [int] NULL,
	[name] [nvarchar](64) NULL,
	[t_create] [date] NOT NULL,
	[sharestatus] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[playlist_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PlayLists_Shares]    Script Date: 26/07/2022 3:27:57 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PlayLists_Shares](
	[playlist_ID] [int] NOT NULL,
	[user_ID_share] [int] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PlayLists_Songs]    Script Date: 26/07/2022 3:27:57 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PlayLists_Songs](
	[playlist_ID] [int] NOT NULL,
	[song_ID] [int] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ReplyComment]    Script Date: 26/07/2022 3:27:57 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ReplyComment](
	[reply_ID] [int] IDENTITY(1,1) NOT NULL,
	[song_ID] [int] NULL,
	[user_ID] [int] NULL,
	[comment_ID] [int] NULL,
	[content] [text] NULL,
	[t_create] [smalldatetime] NOT NULL,
	[t_lastUpdate] [smalldatetime] NULL,
	[report] [bit] NOT NULL,
	[status] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[reply_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Roles]    Script Date: 26/07/2022 3:27:57 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Roles](
	[role_ID] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](32) NOT NULL,
	[description] [nvarchar](1000) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[role_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Slider]    Script Date: 26/07/2022 3:27:57 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Slider](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[song_ID] [int] NOT NULL,
	[heading] [nvarchar](255) NULL,
	[text] [nvarchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Songs]    Script Date: 26/07/2022 3:27:57 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Songs](
	[song_ID] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](64) NOT NULL,
	[author] [nvarchar](64) NOT NULL,
	[duration] [varchar](10) NOT NULL,
	[image] [varchar](255) NULL,
	[path] [varchar](255) NOT NULL,
	[t_create] [smalldatetime] NOT NULL,
	[t_lastUpdate] [smalldatetime] NULL,
	[category_ID] [int] NULL,
	[like] [bit] NULL,
	[views] [int] NULL DEFAULT ((1)),
PRIMARY KEY CLUSTERED 
(
	[song_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Users]    Script Date: 26/07/2022 3:27:57 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Users](
	[user_ID] [int] IDENTITY(1,1) NOT NULL,
	[username] [varchar](32) NOT NULL,
	[password] [varchar](32) NOT NULL,
	[first_name] [nvarchar](16) NULL,
	[last_name] [nvarchar](16) NULL,
	[avatar] [varchar](255) NULL,
	[t_create] [smalldatetime] NOT NULL,
	[t_lastOnline] [smalldatetime] NULL,
	[email] [varchar](64) NOT NULL,
	[status] [bit] NOT NULL,
	[address] [nvarchar](50) NULL,
	[gender] [varchar](16) NULL,
	[DOB] [datetime] NULL,
	[role_ID] [int] NOT NULL,
	[randomVerify] [varchar](16) NOT NULL,
	[report] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[user_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Albums] ON 

INSERT [dbo].[Albums] ([album_ID], [image], [description], [user_ID], [name], [t_create]) VALUES (1, N'./img/song-img/vpop/di-ve-nha.jpg', NULL, NULL, N'Đen Vâu', CAST(N'2022-06-15' AS Date))
INSERT [dbo].[Albums] ([album_ID], [image], [description], [user_ID], [name], [t_create]) VALUES (2, N'./img/song-img/cach_mang/Chao-Em-Co-Gai-Lam-Hong-Truong-Kha.jpg', NULL, NULL, N'Truong Kha', CAST(N'2022-06-15' AS Date))
INSERT [dbo].[Albums] ([album_ID], [image], [description], [user_ID], [name], [t_create]) VALUES (3, N'./img/song-img/tru_tinh/Cung-Mot-Chu-Tinh-Manh-Quynh.jpg', NULL, NULL, N'Manh Quynh', CAST(N'2022-06-15' AS Date))
SET IDENTITY_INSERT [dbo].[Albums] OFF
INSERT [dbo].[Albums_Songs] ([album_ID], [song_ID]) VALUES (1, 9)
INSERT [dbo].[Albums_Songs] ([album_ID], [song_ID]) VALUES (1, 10)
INSERT [dbo].[Albums_Songs] ([album_ID], [song_ID]) VALUES (1, 11)
INSERT [dbo].[Albums_Songs] ([album_ID], [song_ID]) VALUES (1, 18)
INSERT [dbo].[Albums_Songs] ([album_ID], [song_ID]) VALUES (2, 29)
INSERT [dbo].[Albums_Songs] ([album_ID], [song_ID]) VALUES (2, 30)
INSERT [dbo].[Albums_Songs] ([album_ID], [song_ID]) VALUES (2, 31)
INSERT [dbo].[Albums_Songs] ([album_ID], [song_ID]) VALUES (2, 32)
INSERT [dbo].[Albums_Songs] ([album_ID], [song_ID]) VALUES (2, 34)
INSERT [dbo].[Albums_Songs] ([album_ID], [song_ID]) VALUES (2, 35)
INSERT [dbo].[Albums_Songs] ([album_ID], [song_ID]) VALUES (2, 37)
INSERT [dbo].[Albums_Songs] ([album_ID], [song_ID]) VALUES (3, 23)
INSERT [dbo].[Albums_Songs] ([album_ID], [song_ID]) VALUES (3, 25)
SET IDENTITY_INSERT [dbo].[Background] ON 

INSERT [dbo].[Background] ([background_ID], [name], [path]) VALUES (1, N'Bg-1', N'./img/bg-gif/gif-1.gif')
INSERT [dbo].[Background] ([background_ID], [name], [path]) VALUES (2, N'Bg-2', N'./img/bg-gif/gif-2.gif')
INSERT [dbo].[Background] ([background_ID], [name], [path]) VALUES (3, N'Bg-3', N'./img/bg-gif/gif-3.gif')
INSERT [dbo].[Background] ([background_ID], [name], [path]) VALUES (4, N'Bg-4', N'./img/bg-gif/gif-4.gif')
INSERT [dbo].[Background] ([background_ID], [name], [path]) VALUES (5, N'Bg-5', N'./img/bg-gif/gif-5.gif')
INSERT [dbo].[Background] ([background_ID], [name], [path]) VALUES (6, N'Bg-6', N'./img/bg-gif/gif-6.gif')
INSERT [dbo].[Background] ([background_ID], [name], [path]) VALUES (7, N'Bg-7', N'./img/bg-gif/gif-7.gif')
INSERT [dbo].[Background] ([background_ID], [name], [path]) VALUES (8, N'Bg-8', N'./img/bg-gif/gif-8.gif')
INSERT [dbo].[Background] ([background_ID], [name], [path]) VALUES (9, N'Bg-9', N'./img/bg-gif/gif-9.gif')
INSERT [dbo].[Background] ([background_ID], [name], [path]) VALUES (10, N'Bg-10', N'./img/bg-gif/gif-10.gif')
INSERT [dbo].[Background] ([background_ID], [name], [path]) VALUES (11, N'Bg-11', N'./img/bg-gif/gif-11.gif')
INSERT [dbo].[Background] ([background_ID], [name], [path]) VALUES (12, N'Bg-12', N'./img/bg-gif/gif-12.gif')
INSERT [dbo].[Background] ([background_ID], [name], [path]) VALUES (13, N'Bg-13', N'./img/bg-gif/gif-13.gif')
INSERT [dbo].[Background] ([background_ID], [name], [path]) VALUES (14, N'Bg-14', N'./img/bg-gif/gif-14.gif')
INSERT [dbo].[Background] ([background_ID], [name], [path]) VALUES (15, N'Bg-15', N'./img/bg-gif/gif-15.gif')
INSERT [dbo].[Background] ([background_ID], [name], [path]) VALUES (16, N'Bg-16', N'./img/bg-gif/gif-16.gif')
INSERT [dbo].[Background] ([background_ID], [name], [path]) VALUES (17, N'Bg-17', N'./img/bg-gif/gif-17.gif')
INSERT [dbo].[Background] ([background_ID], [name], [path]) VALUES (18, N'Bg-18', N'./img/bg-gif/gif-18.gif')
INSERT [dbo].[Background] ([background_ID], [name], [path]) VALUES (19, N'Bg-19', N'./img/bg-gif/gif-19.gif')
INSERT [dbo].[Background] ([background_ID], [name], [path]) VALUES (20, N'Bg-20', N'./img/bg-gif/gif-20.gif')
INSERT [dbo].[Background] ([background_ID], [name], [path]) VALUES (21, N'Bg-21', N'./img/bg-gif/gif-21.gif')
INSERT [dbo].[Background] ([background_ID], [name], [path]) VALUES (22, N'Bg-22', N'./img/bg-gif/gif-22.gif')
INSERT [dbo].[Background] ([background_ID], [name], [path]) VALUES (23, N'Bg-23', N'./img/bg-gif/gif-23.gif')
INSERT [dbo].[Background] ([background_ID], [name], [path]) VALUES (24, N'Bg-24', N'./img/bg-gif/gif-24.gif')
INSERT [dbo].[Background] ([background_ID], [name], [path]) VALUES (25, N'Bg-25', N'./img/bg-gif/gif-25.gif')
INSERT [dbo].[Background] ([background_ID], [name], [path]) VALUES (26, N'Bg-26', N'./img/bg-gif/gif-26.gif')
INSERT [dbo].[Background] ([background_ID], [name], [path]) VALUES (27, N'Bg-27', N'./img/bg-gif/gif-27.gif')
INSERT [dbo].[Background] ([background_ID], [name], [path]) VALUES (28, N'Bg-28', N'./img/bg-gif/gif-28.gif')
INSERT [dbo].[Background] ([background_ID], [name], [path]) VALUES (29, N'Bg-29', N'./img/bg-gif/gif-29.gif')
INSERT [dbo].[Background] ([background_ID], [name], [path]) VALUES (30, N'Bg-30', N'./img/bg-gif/gif-30.gif')
INSERT [dbo].[Background] ([background_ID], [name], [path]) VALUES (31, N'Bg-31', N'./img/bg-gif/gif-31.gif')
INSERT [dbo].[Background] ([background_ID], [name], [path]) VALUES (32, N'Bg-32', N'./img/bg-gif/gif-32.gif')
INSERT [dbo].[Background] ([background_ID], [name], [path]) VALUES (33, N'Bg-33', N'./img/bg-gif/gif-33.gif')
INSERT [dbo].[Background] ([background_ID], [name], [path]) VALUES (34, N'Bg-34', N'./img/bg-gif/gif-34.gif')
INSERT [dbo].[Background] ([background_ID], [name], [path]) VALUES (35, N'Bg-35', N'./img/bg-gif/gif-35.gif')
INSERT [dbo].[Background] ([background_ID], [name], [path]) VALUES (36, N'Bg-36', N'./img/bg-gif/gif-36.gif')
INSERT [dbo].[Background] ([background_ID], [name], [path]) VALUES (37, N'Bg-37', N'./img/bg-gif/gif-37.gif')
INSERT [dbo].[Background] ([background_ID], [name], [path]) VALUES (38, N'Bg-38', N'./img/bg-gif/gif-38.gif')
INSERT [dbo].[Background] ([background_ID], [name], [path]) VALUES (39, N'Bg-39', N'./img/bg-gif/gif-39.gif')
INSERT [dbo].[Background] ([background_ID], [name], [path]) VALUES (40, N'Bg-40', N'./img/bg-gif/gif-40.gif')
SET IDENTITY_INSERT [dbo].[Background] OFF
SET IDENTITY_INSERT [dbo].[Categories] ON 

INSERT [dbo].[Categories] ([category_ID], [categorydetail_ID], [name]) VALUES (1, 1, N'Nhac Tre')
INSERT [dbo].[Categories] ([category_ID], [categorydetail_ID], [name]) VALUES (2, 1, N'Nhac Tru Tinh')
INSERT [dbo].[Categories] ([category_ID], [categorydetail_ID], [name]) VALUES (3, 1, N'Nhac Cach Mang')
INSERT [dbo].[Categories] ([category_ID], [categorydetail_ID], [name]) VALUES (4, 2, N'Nhac US-UK')
INSERT [dbo].[Categories] ([category_ID], [categorydetail_ID], [name]) VALUES (5, 2, N'Nhac K-Pop')
INSERT [dbo].[Categories] ([category_ID], [categorydetail_ID], [name]) VALUES (6, 2, N'Nhac Anime')
INSERT [dbo].[Categories] ([category_ID], [categorydetail_ID], [name]) VALUES (7, 3, N'Nhac Piano')
INSERT [dbo].[Categories] ([category_ID], [categorydetail_ID], [name]) VALUES (8, 3, N'Nhac Guitar')
INSERT [dbo].[Categories] ([category_ID], [categorydetail_ID], [name]) VALUES (9, 3, N'Nhac Chill')
SET IDENTITY_INSERT [dbo].[Categories] OFF
SET IDENTITY_INSERT [dbo].[CategoryDetails] ON 

INSERT [dbo].[CategoryDetails] ([categorydetail_ID], [categorydetail_name]) VALUES (1, N'Nhac Viet Nam')
INSERT [dbo].[CategoryDetails] ([categorydetail_ID], [categorydetail_name]) VALUES (2, N'Nhac Quoc Te')
INSERT [dbo].[CategoryDetails] ([categorydetail_ID], [categorydetail_name]) VALUES (3, N'Nhac Lofi')
SET IDENTITY_INSERT [dbo].[CategoryDetails] OFF
SET IDENTITY_INSERT [dbo].[HistoryView] ON 

INSERT [dbo].[HistoryView] ([history_ID], [t_create], [user_ID], [song_ID]) VALUES (1, CAST(N'2022-07-26 15:02:00' AS SmallDateTime), 1, 15)
INSERT [dbo].[HistoryView] ([history_ID], [t_create], [user_ID], [song_ID]) VALUES (2, CAST(N'2022-07-26 15:05:00' AS SmallDateTime), 1, 2)
INSERT [dbo].[HistoryView] ([history_ID], [t_create], [user_ID], [song_ID]) VALUES (3, CAST(N'2022-07-26 15:22:00' AS SmallDateTime), 1, 83)
INSERT [dbo].[HistoryView] ([history_ID], [t_create], [user_ID], [song_ID]) VALUES (4, CAST(N'2022-07-26 15:22:00' AS SmallDateTime), 1, 83)
INSERT [dbo].[HistoryView] ([history_ID], [t_create], [user_ID], [song_ID]) VALUES (5, CAST(N'2022-07-26 15:22:00' AS SmallDateTime), 1, 83)
INSERT [dbo].[HistoryView] ([history_ID], [t_create], [user_ID], [song_ID]) VALUES (6, CAST(N'2022-07-26 15:24:00' AS SmallDateTime), 1, 1)
INSERT [dbo].[HistoryView] ([history_ID], [t_create], [user_ID], [song_ID]) VALUES (7, CAST(N'2022-07-26 15:24:00' AS SmallDateTime), 1, 1)
INSERT [dbo].[HistoryView] ([history_ID], [t_create], [user_ID], [song_ID]) VALUES (8, CAST(N'2022-07-26 15:24:00' AS SmallDateTime), 1, 1)
SET IDENTITY_INSERT [dbo].[HistoryView] OFF
SET IDENTITY_INSERT [dbo].[Lyrics] ON 

INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (1, N'Một bậc quân vương mang trong con tim hình hài đất nước', N'00:16', 1)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (2, N'Ngỡ như dân an ta sẽ chẳng bao giờ buồn', N'00:21', 1)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (3, N'Nào ngờ một hôm ngao du nhân gian chạm một ánh mắt', N'00:26', 1)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (4, N'Khiến cho ta say ta mê như chốn thiên đường', N'00:29', 1)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (5, N'Trời cao như đang trêu ngươi thân ta khi bông hoa ấy', N'00:33', 1)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (6, N'Trót mang con tim trao cho một nam nhân thường', N'00:37', 1)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (7, N'Giận lòng ta ban cho bông hoa thơm hồi về cung cấm', N'00:41', 1)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (8, N'Khiến em luôn luôn bên ta mãi mãi không buông', N'00:45', 1)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (9, N'Mà nào ngờ đâu thân em nơi đây tâm trí nơi nào', N'00:49', 1)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (10, N'Nhìn về quê hương em ôm tương tư nặng lòng biết bao', N'00:53', 1)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (11, N'Một người nam nhân không vinh không hoa mà có lẽ nào', N'00:57', 1)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (12, N'Người lại yêu thương quan tâm hơn ta một đế vương sao', N'01:01', 1)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (13, N'Giọt lệ quân vương không khi nào rơi khi nước chưa tàn', N'01:05', 1)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (14, N'Mà tình chưa yên nên vương trên mi giọt buồn chứa chan', N'01:09', 1)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (15, N'Đành lòng buông tay cho em ra đi với mối tình vàng', N'01:13', 1)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (16, N'Một bậc quân vương uy nghiêm oai phong nhưng tim nát tan', N'01:17', 1)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (17, N'Một bậc quân vương mang trong con tim hình hài đất nước', N'01:37', 1)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (18, N'Ngỡ như dân an ta sẽ chẳng bao giờ buồn', N'01:41', 1)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (19, N'Nào ngờ một hôm ngao du nhân gian chạm một ánh mắt', N'01:45', 1)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (20, N'Khiến cho ta say ta mê như chốn thiên đường', N'01:49', 1)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (21, N'Trời cao như đang trêu ngươi thân ta khi bông hoa ấy', N'01:53', 1)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (22, N'Trót mang con tim trao cho một nam nhân thường', N'01:57', 1)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (23, N'Giận lòng ta ban cho bông hoa thơm hồi về cung cấm', N'02:01', 1)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (24, N'Khiến em luôn luôn bên ta mãi mãi không buông', N'02:05', 1)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (25, N'Mà nào ngờ đâu thân em nơi đây tâm trí nơi nào', N'02:09', 1)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (26, N'Nhìn về quê hương em ôm tương tư nặng lòng biết bao', N'02:13', 1)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (27, N'Một người nam nhân không vinh không hoa mà có lẽ nào', N'02:17', 1)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (28, N'Người lại yêu thương quan tâm hơn ta một đế vương sao', N'02:21', 1)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (29, N'Giọt lệ quân vương không khi nào rơi khi nước chưa tàn', N'02:25', 1)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (30, N'Mà tình chưa yên nên vương trên mi giọt buồn chứa chan', N'02:29', 1)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (31, N'Đành lòng buông tay cho em ra đi với mối tình vàng', N'02:33', 1)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (32, N'Một bậc quân vương uy nghiêm oai phong nhưng tim nát tan', N'02:37', 1)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (33, N'Mà nào ngờ đâu thân em nơi đây tâm trí nơi nào', N'02:57', 1)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (34, N'Nhìn về quê hương em ôm tương tư nặng lòng biết bao', N'03:01', 1)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (35, N'Một người nam nhân không vinh không hoa mà có lẽ nào', N'03:05', 1)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (36, N'Người lại yêu thương quan tâm hơn ta một đế vương sao', N'03:09', 1)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (37, N'Giọt lệ quân vương không khi nào rơi khi nước chưa tàn', N'03:13', 1)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (38, N'Mà tình chưa yên nên vương trên mi giọt buồn chứa chan', N'03:17', 1)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (39, N'Đành lòng buông tay cho em ra đi với mối tình vàng', N'03:21', 1)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (40, N'Một bậc quân vương uy nghiêm oai phong nhưng tim nát tan', N'03:25', 1)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (41, N'Một bậc quân vương mang trong con tim hình hài đất nước', N'04:01', 1)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (42, N'Ngỡ như dân an ta sẽ chẳng bao giờ buồn', N'04:05', 1)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (43, N'Nào ngờ một hôm ngao du nhân gian chạm một ánh mắt', N'04:09', 1)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (44, N'Khiến cho ta say ta mê như chốn thiên đường', N'04:13', 1)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (45, N'Chiều hôm ấy em nói với anh', N'00:34', 15)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (46, N'Rằng mình không nên gặp nhau nữa', N'00:37', 15)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (47, N'Người ơi', N'00:40', 15)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (48, N'Em đâu biết anh đau thế nào', N'00:42', 15)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (49, N'Khoảng lặng phủ kín căn phòng ấy', N'00:46', 15)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (50, N'Tim anh như thắt lại', N'00:49', 15)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (51, N'Và mong đó chỉ là mơ', N'00:53', 15)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (52, N'Vì anh còn yêu em rất nhiều', N'00:55', 15)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (53, N'Giọt buồn làm nhòe đi dòng kẻ mắt', N'00:58', 15)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (54, N'Hòa cùng cơn mưa', N'01:02', 15)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (55, N'Là những nỗi buồn kia', N'01:04', 15)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (56, N'Em khóc cho cuộc tình chúng mình', N'01:06', 15)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (57, N'Cớ sao con yêu nhau mà mình', N'01:11', 15)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (58, N'Không thể đến được với nhau', N'01:14', 15)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (59, N'Vì anh đã sai hay bởi vì', N'01:17', 15)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (60, N'Bên em có ai kia', N'01:20', 15)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (61, N'Chẳng ai có thể', N'01:25', 15)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (62, N'Hiểu nổi được trái tim', N'01:28', 15)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (63, N'Khi đã lỡ yêu rồi', N'01:30', 15)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (64, N'Chỉ biết trách bản thân', N'01:32', 15)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (65, N'Đã mù quáng', N'01:34', 15)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (66, N'Trót yêu một người vô tâm', N'01:36', 15)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (67, N'Từng lời hứa như vết dao lạnh lùng', N'01:38', 15)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (68, N'Cắt thật sâu trái tim này', N'01:42', 15)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (69, N'Vì muốn thấy em hạnh phúc nên', N'01:45', 15)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (70, N'Anh sẽ lùi về sau', N'01:48', 15)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (71, N'Thời gian qua chúng ta', N'01:51', 15)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (72, N'Liệu sống tốt hơn', N'01:53', 15)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (73, N'Hay cứ mãi dối lừa', N'01:55', 15)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (74, N'Nhìn người mình thương', N'01:57', 15)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (75, N'Ứớt nhòe mi cay', N'01:59', 15)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (76, N'Khiến tim này càng thêm đau', N'02:00', 15)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (77, N'Người từng khiến', N'02:03', 15)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (78, N'Anh thay đổi là em', N'02:05', 15)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (79, N'Đã mãi xa rồi', N'02:07', 15)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (80, N'Thôi giấc mơ khép lại', N'02:10', 15)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (81, N'Ký ức kia gửi theo', N'02:13', 15)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (82, N'Gió bay', N'02:20', 15)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (83, N'Giọt buồn làm nhòe đi', N'02:32', 15)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (84, N'Dòng kẻ mắt', N'02:33', 15)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (85, N'Hòa cùng cơn mưa', N'02:35', 15)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (86, N'Là những nỗi buồn kia', N'02:37', 15)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (87, N'Em khóc cho cuộc tình chúng mình', N'02:40', 15)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (88, N'Cớ sao con yêu nhau mà mình', N'02:44', 15)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (89, N'Không thể đến được với nhau', N'02:47', 15)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (90, N'Vì anh đã sai hay bởi vì', N'02:51', 15)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (91, N'Bên em có ai kia', N'02:54', 15)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (92, N'Chẳng ai có thể hiểu nổi', N'03:00', 15)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (93, N'Được trái tim', N'03:02', 15)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (94, N'Khi đã lỡ yêu rồi', N'03:03', 15)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (95, N'Chỉ biết trách bản thân', N'03:06', 15)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (96, N'Đã mù quáng', N'03:07', 15)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (97, N'Trót yêu một người vô tâm', N'03:09', 15)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (98, N'Từng lời hứa như vết dao lạnh lùng', N'03:12', 15)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (99, N'Cắt thật sâu trái tim này', N'03:15', 15)
GO
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (100, N'Vì muốn thấy em hạnh phúc nên', N'03:18', 15)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (101, N'Anh sẽ lùi về sau', N'03:21', 15)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (102, N'Thời gian qua chúng ta', N'03:24', 15)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (103, N'Liệu sống tốt hơn', N'03:27', 15)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (104, N'Hay cứ mãi dối lừa', N'03:28', 15)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (105, N'Nhìn người mình thương', N'03:31', 15)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (106, N'Ướt nhòe mi cay', N'03:32', 15)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (107, N'Khiến tim này càng thêm đau', N'03:34', 15)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (108, N'Người từng khiến', N'03:37', 15)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (109, N'Anh thay đổi là em', N'03:38', 15)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (110, N'Đã mãi xa rồi', N'03:41', 15)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (111, N'Thôi giấc mơ khép lại', N'03:44', 15)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (112, N'Ký ức kia gửi theo', N'03:47', 15)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (113, N'Gió bay', N'03:50', 15)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (114, N'Chẳng ai có thể', N'03:53', 15)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (115, N'Hiểu nổi được trái tim', N'03:54', 15)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (116, N'Khi đã lỡ yêu rồi', N'03:56', 15)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (117, N'Chỉ biết trách bản thân', N'03:59', 15)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (118, N'Đã mù quáng', N'04:01', 15)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (119, N'Trót yêu một người vô tâm', N'04:02', 15)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (120, N'Từng lời hứa như vết dao lạnh lùng', N'04:05', 15)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (121, N'Cắt thật sâu trái tim này', N'04:08', 15)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (122, N'Vì muốn thấy em hạnh phúc nên', N'04:12', 15)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (123, N'Anh sẽ lùi về sau', N'04:15', 15)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (124, N'Thời gian qua chúng ta', N'04:18', 15)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (125, N'Liệu sống tốt hơn', N'04:19', 15)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (126, N'Hay cứ mãi dối lừa', N'04:21', 15)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (127, N'Nhìn người mình thương', N'04:24', 15)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (128, N'Ướt nhòe mi cay', N'04:25', 15)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (129, N'Khiến tim này', N'04:27', 15)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (130, N'Càng thêm đau', N'04:29', 15)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (131, N'Người từng khiến anh', N'04:30', 15)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (132, N'Thay đổi là em', N'04:31', 15)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (133, N'Đã mãi xa rồi', N'04:34', 15)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (134, N'Thôi giấc mơ khép lại', N'04:37', 15)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (135, N'Ký ức kia gửi theo', N'04:40', 15)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (136, N'Gió bay', N'04:47', 15)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (137, N'Đường về chiều mưa rơi', N'00:37', 2)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (138, N'Thấy anh cùng ai bước chung', N'00:40', 2)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (139, N'Bỗng nhiên bao nhiêu ký ức quen thuộc', N'00:44', 2)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (140, N'Cứ hiện lên làm em thấy buồn', N'00:47', 2)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (141, N'Hình như anh đang vui', N'00:51', 2)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (142, N'Vì thấy anh mỉm cười rất tươi', N'00:54', 2)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (143, N'Nắm tay anh rồi ai đó ôm anh', N'00:58', 2)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (144, N']Mắt nhẹ cay vì phút yếu mềm', N'01:01', 2)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (145, N'Thời gian trôi qua mà anh vẫn giữ những tấm hình của đôi ta', N'01:05', 2)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (146, N']Ngày mình chia xa và anh đã nói đằng sau anh luôn vẫn chờ', N'01:12', 2)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (147, N'Làm gì có ai thương em như vậy', N'01:18', 2)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (148, N'Có ai cần em đến thế', N'01:21', 2)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (149, N'Có ai chia tay mà vẫn mong từng ngày Mong niềm vui dù em thuộc về ai', N'01:25', 2)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (150, N'Làm gì có ai thương em như vậy', N'01:31', 2)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (151, N'Có ai vẫn luôn ở đấy', N'01:35', 2)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (152, N'Đứng sau lưng em để những khi chơi vơi', N'01:38', 2)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (153, N'Mang bình yên chẳng mong gì xa xôi', N'01:42', 2)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (154, N'Có ai thương em...', N'01:46', 2)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (155, N'Đường mình giờ chia đôi', N'02:14', 2)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (156, N'Hãy cất em vào quá khứ thôi', N'02:16', 2)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (157, N'Cứ yên lặng nhẹ như áng mây trôi', N'02:20', 2)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (158, N'Để chẳng ai buồn ai đứng đợi', N'02:24', 2)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (159, N'Thời gian trôi qua mà anh vẫn giữ những tấm hình của đôi ta', N'02:27', 2)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (160, N'Ngày mình chia xa và anh đã nói đằng sau anh luôn vẫn chờ', N'02:34', 2)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (161, N'Làm gì có ai thương em như vậy', N'02:40', 2)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (162, N'Có ai cần em đến thế', N'02:44', 2)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (163, N'Có ai chia tay mà vẫn mong từng ngày', N'02:47', 2)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (164, N'Mong niềm vui dù em thuộc về ai', N'02:51', 2)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (165, N'Làm gì có ai thương em như vậy', N'02:54', 2)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (166, N'Có ai vẫn luôn ở đấy', N'02:57', 2)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (167, N'Đứng sau lưng em để những khi chơi vơi', N'03:01', 2)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (168, N'Mang bình yên chẳng mong gì xa xôi', N'03:05', 2)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (169, N'Có ai thương em như vậy', N'03:08', 2)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (170, N'Có ai cần em đến thế', N'03:11', 2)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (171, N'Có ai chia tay mà vẫn mong từng ngày', N'03:14', 2)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (172, N'Mong niềm vui dù em thuộc về ai', N'03:18', 2)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (173, N'Có ai thương em như vậy', N'03:21', 2)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (174, N'Có ai vẫn luôn ở đấy', N'03:25', 2)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (175, N'Đứng sau lưng em để những khi chơi vơi', N'03:28', 2)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (176, N'Mang bình yên chẳng mong gì xa xôi', N'03:32', 2)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (177, N'Có ai thương em như vậy...', N'03:35', 2)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (178, N'Có ai thương em như anh...', N'03:42', 2)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (179, N'Có ai cần em như anh...', N'03:49', 2)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (222, N'Bậu ơi, em đang buồn ai lắm phải không', N'00:30', 20)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (223, N'Mà ra ngồi khóc cho mờ phai má hồng', N'00:37', 20)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (224, N'Bậu ơi em đừng khóc, bậu ơi em nhìn coi', N'00:44', 20)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (225, N'Khán giả đây rồi người ta mong thấy em cười', N'00:50', 20)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (226, N'Vòng quay đêm nay là con số gì đây', N'00:57', 20)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (227, N'Từng vòng quay mà ngỡ như đời xoay tháng ngày', N'01:04', 20)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (228, N'Bậu ơi, con đường đó dù cho ai cười khinh', N'01:11', 20)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (229, N'Khán giả thương mình thì còn sân khấu lung linh', N'01:17', 20)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (230, N'Bậu ơi, em buồn chi những lời người ta gieo buồn đau', N'01:25', 20)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (231, N'Họ nói xong rồi họ có bao giờ sống thay mình đâu', N'01:31', 20)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (232, N'Cuộc đời sinh ra mấy ai chọn nơi bắt đầu?', N'01:38', 20)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (233, N'Thế nhưng mình luôn nhắc mình', N'01:44', 20)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (234, N'Sống cho nghĩa tình sống ra con người...', N'01:47', 20)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (235, N'Bậu ơi, em mãi là em của chị thôi', N'01:55', 20)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (236, N'Phận duyên, trời cũng cho mình vui khiếp người', N'02:02', 20)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (237, N'Bậu ơi, em đừng khóc', N'02:09', 20)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (238, N'Bậu ơi, em đừng lo', N'02:13', 20)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (239, N'Còn chị, còn em còn chung em là còn tương lai...', N'02:16', 20)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (240, N'Bậu ơi, em buồn chi những lời người ta gieo buồn đau', N'02:52', 20)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (241, N'Họ nói xong rồi họ có bao giờ sống thay mình đâu?', N'02:58', 20)
GO
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (242, N'Cuộc đời sinh ra mấy ai chọn nơi bắt đầu?', N'03:05', 20)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (243, N'Thế nhưng mình luôn nhắc mình', N'03:12', 20)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (244, N'Sống cho nghĩa tình sống ra con người', N'03:14', 20)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (245, N'Bậu ơi, em mãi là em của chị thôi', N'03:23', 20)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (246, N'Phận duyên, trời cũng cho mình vui kiếp người', N'03:30', 20)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (247, N'Bậu ơi, em đừng khóc', N'03:36', 20)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (248, N'Bậu ơi, em đừng lo', N'03:40', 20)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (249, N'Còn chị, còn em còn chung vai là còn tương lai', N'03:43', 20)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (250, N'Bậu ơi, em đừng khóc', N'03:50', 20)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (251, N'Bậu ơi, em đừng lo', N'03:54', 20)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (252, N'Còn chị, còn em còn chung vai là còn tương lai', N'03:57', 20)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (253, N'Bậu ơi, em đừng khóc', N'04:04', 20)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (254, N'Bậu ơi, em đừng lo', N'04:08', 20)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (255, N'Còn chị, còn em còn chung vai là còn tương lai...', N'04:10', 20)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (256, N'rap chậm thôi', N'00:29', 12)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (257, N'muốn cắn northside nên là phải táp tận nơi', N'00:30', 12)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (258, N'cẩn thận nuốt phải lưỡi nhớ các dân chơi', N'00:32', 12)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (259, N'già rồi nghỉ đê cho đỡ tăng huyết áp', N'00:34', 12)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (260, N'yeah, tặng mày 1 tab ngậm chơi', N'00:36', 12)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (261, N'tao phải khuyên mày là rap chậm thôi', N'00:37', 12)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (262, N'muốn cắn northside nên là phải táp tận nơi', N'00:39', 12)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (263, N'già rồi nghỉ đê cho đỡ tăng huyết áp', N'00:41', 12)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (264, N'yeah, tặng mày 1 tab ngậm chơi', N'00:43', 12)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (265, N'lên xà 1 tay quay từ vai tới đầu', N'00:45', 12)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (266, N'bọn tao chơi rap rõ vibe với màu', N'00:47', 12)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (267, N'muốn nghe fastflow theo kiểu miền Bắc á?', N'00:48', 12)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (268, N'Wxrdie cho mày biết là ai mới ngầu', N'00:50', 12)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (269, N'giấy cháy khi tao ghi ra vần', N'00:52', 12)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (270, N'beef nam bắc bọn tao di 3 lần', N'00:54', 12)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (271, N'sau track của mày chữ G tao kính trọng chuyển từ GFam sang cho G-Dragon', N'00:56', 12)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (272, N'toàn bọn nhà giàu', N'01:00', 12)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (273, N'không biết dùng tiền', N'01:01', 12)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (274, N'anh em Nam Bắc bọn tao vẫn đang cùng thuyền', N'01:01', 12)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (275, N'bọn tao nhận thắng trò chơi âm nhạc', N'01:03', 12)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (276, N'còn mày giải nhất trò chơi phân biệt vùng miền', N'01:05', 12)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (277, N'bố mày vác dái bố đái vào bát quái', N'01:07', 12)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (278, N'nước rơi tong tỏng ở trên tận gác mái', N'01:09', 12)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (279, N'đại hiệp nhoe nhoét quay lên hỏi quý danh', N'01:10', 12)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (280, N'tại hạ bảo là bố mày đến từ trap phái', N'01:12', 12)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (281, N'thôi', N'01:14', 12)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (282, N'bạn bớt bớt đê', N'01:16', 12)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (283, N'lên xe', N'01:16', 12)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (284, N'(skrt skrt )', N'01:18', 12)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (285, N'ye', N'01:19', 12)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (286, N'mày ngồi im xem bố mày lái trap (mày im)', N'01:19', 12)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (287, N'đây là trường phái khác', N'01:20', 12)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (288, N'dẫn mày thử đồ', N'01:22', 12)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (289, N'xong rủ vô', N'01:23', 12)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (290, N'trong thủ đô', N'01:24', 12)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (291, N'tao cầm đồ mày chỉ được cái mác', N'01:25', 12)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (292, N'tao cầm đồ còn mày chỉ được cái (woo)', N'01:26', 12)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (293, N'nếu rapper có hạng A,B', N'01:28', 12)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (294, N'(dit me may, dit me may)', N'01:30', 12)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (295, N'C,D,E,F thì đếm vãi lồn nó mới đến G', N'01:32', 12)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (296, N'nếu rapper có hạng A,B', N'01:35', 12)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (297, N'(dit me may, dit me may)', N'01:38', 12)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (298, N'C,D,E,F thì đếm vãi lồn nó mới đến G', N'01:39', 12)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (299, N'rap chậm thôi', N'01:43', 12)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (300, N'muốn cắn northside nên là phải táp tận nơi', N'01:44', 12)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (301, N'cẩn thận nuốt phải lưỡi nhớ các dân chơi', N'01:45', 12)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (302, N'già rồi nghỉ đê cho đỡ tăng huyết áp', N'01:47', 12)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (303, N'yeah, tặng mày 1 tab ngậm chơi', N'01:49', 12)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (304, N'tao phải khuyên mày là rap chậm thôi', N'01:52', 12)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (305, N'muốn cắn northside nên là phải táp tận nơi', N'01:53', 12)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (306, N'già rồi nghỉ đê cho đỡ tăng huyết áp', N'01:55', 12)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (307, N'yeah, tặng mày 1 tab ngậm chơi', N'01:57', 12)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (308, N'thủ đô northside bọn tao nắm quyền farm', N'01:59', 12)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (309, N'trình độ miền bắc là giải phóng miền nam', N'02:00', 12)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (310, N'lịch sử đã khắc mày đâu có quyền than', N'02:01', 12)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (311, N'lịch sử là phẩm chất của công dân việt nam', N'02:03', 12)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (312, N'muốn táp northside mày nên chỉnh lại ngày', N'02:05', 12)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (313, N'tặng bạn 1 tab cho bạn tỉnh lại này', N'02:07', 12)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (314, N'ôi bạn ei cứ đến đây mà thỉnh lại thầy', N'02:09', 12)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (315, N'northside bọn tao chuyên điều trị loại lầy', N'02:11', 12)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (316, N'ối tổ sư bố cái thằng già đầu', N'02:12', 12)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (317, N'mặt mày già đét vẫn phân biệt vùng miền', N'02:14', 12)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (318, N'mày là đấng nam nhi mang tâm hồn bà bầu', N'02:16', 12)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (319, N'tao dạy mày cách rap dạy cả cách mày dùng tiền', N'02:18', 12)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (320, N'điểm g là chỗ tao dùng ặc chọc vào', N'02:20', 12)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (321, N'đến giờ dạy dỗ mấy thằng giặc học nào', N'02:22', 12)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (322, N'nhắc nhở mày nhớ cả tổ đội mày khỏi quên', N'02:23', 12)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (323, N'cờ đỏ sao vàng thì làm đéo có sọc nào?', N'02:25', 12)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (324, N'tao vỗ vào mép lũ càm ràm ồn ào (thôi)', N'02:27', 12)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (325, N'mày câm mẹ mồm vào', N'02:29', 12)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (326, N'chơi rap như mày đòi cân cả miền bắc á? (tuổi lồn à?)', N'02:31', 12)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (327, N'gọi cả đội mày vào', N'02:33', 12)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (328, N'để tao vỗ vào mép mấy thằng xàm lồn nào (chừa nhá)', N'02:35', 12)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (329, N'ngậm con mẹ mồm vào', N'02:36', 12)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (330, N'trình độ của mày em tao bảo là quá non (non)', N'02:38', 12)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (331, N'gọi cả đội mày vào', N'02:41', 12)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (332, N'rap chậm thôi', N'02:42', 12)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (333, N'muốn cắn northside nên là phải táp tận nơi', N'02:43', 12)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (334, N'cẩn thận nuốt phải lưỡi nhớ các dân chơi', N'02:44', 12)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (335, N'già rồi nghỉ đê cho đỡ tăng huyết áp', N'02:46', 12)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (336, N'yeah, tặng mày 1 tab ngậm chơi', N'02:48', 12)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (337, N'tao phải khuyên mày là rap chậm thôi', N'02:50', 12)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (338, N'muốn cắn northside nên là phải táp tận nơi', N'02:52', 12)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (339, N'già rồi nghỉ đê cho đỡ tăng huyết áp', N'02:54', 12)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (340, N'yeah, tặng mày 1 tab ngậm chơi', N'02:56', 12)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (341, N'miền bắc ngoài hải phòng đâu ai chửi mày đâu?', N'02:58', 12)
GO
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (342, N'nếu mày tính miền bắc là mỗi hải phòng thì ta nói vụ này sau', N'02:59', 12)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (343, N'rapper miền nam nào cũng flow được y hệt như cái cách mà mày flow', N'03:03', 12)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (344, N'mày muốn tổ đội trẻ tôn thờ nên mới toàn thằng rap giống mày wow (wow wow)', N'03:07', 12)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (345, N'mặc kệ bọn ngu', N'03:11', 12)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (346, N'làm chuyện bọn ngu', N'03:13', 12)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (347, N'mày, già đầu rồi còn ngu tư duy không bằng được một thằng cu', N'03:14', 12)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (348, N'thằng nào chửi mày thì mày chửi lại nó mày lôi miền bắc này ra ăn lồn', N'03:18', 12)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (349, N'so sánh fast flow với cả vú to vì ai rồi cũng sẽ chọn tâm hồn', N'03:22', 12)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (350, N'ôi, hay cái thằng cơ hội lại định khơi mào cái trò nam bắc', N'03:26', 12)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (351, N'ngoài rap fan thích xem chửi nhau ra thì còn thằng rapper nào ham chắc?', N'03:29', 12)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (352, N'ai cũng muốn làm một bài nhạc mà sau này vẫn sẽ còn vang khắp', N'03:33', 12)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (353, N'lời của tao lúc chua lúc ngọt nhưng luôn chất lượng như ly cam vắt', N'03:36', 12)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (354, N'nên là đừng làm như thế, uhm', N'03:39', 12)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (355, N'đừng làm như thế', N'03:41', 12)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (356, N'đã có một người trẻ bảo mày rằng là đừng làm như thế', N'03:43', 12)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (357, N'đang có thêm một người nữa bảo mày là đừng làm như thế', N'03:48', 12)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (358, N'rồi mọi người ai cũng sẽ bảo mày là đừng làm như thế', N'03:51', 12)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (359, N'(ê, thằng ngu', N'03:57', 12)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (360, N'nhà quê', N'04:01', 12)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (361, N'Sau những con đường quen ta đã vô tình đến', N'00:16', 8)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (362, N'Là nụ cười em quẩn quanh với giấc mơ', N'00:20', 8)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (363, N'Nơi những ánh đèn sáng', N'00:23', 8)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (364, N'Ta với khung hình khác', N'00:25', 8)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (365, N'Là bình yên cất giấu trước cuộc đời', N'00:27', 8)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (366, N'Nhìn xem lần sau cuối là bao điều tiếc nuối', N'00:31', 8)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (367, N'Tình yêu là thứ khiến em quên đi vài lần yếu đuối', N'00:35', 8)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (368, N'Lặng nhìn giọt sương rơi lạc trong màu u tối', N'00:38', 8)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (369, N'Là khi tình yêu ấy đã khiến em thôi những mộng mơ', N'00:42', 8)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (370, N'Anh vẫn thức giấc trên giường với giấc mơ vừa tàn', N'00:46', 8)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (371, N'Bản nhạc vụt tắt bộ phim kia dừng lại', N'00:50', 8)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (372, N'Nghe tiếng mưa rơi bên thềm anh ngước mắt lặng nhìn', N'00:53', 8)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (373, N'Rồi chờ đợi mãi vẫn không quay lại', N'00:57', 8)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (374, N'No no no', N'01:02', 8)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (375, N'Baby let me know', N'01:06', 8)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (376, N'No no no', N'01:10', 8)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (377, N'Baby let me know', N'01:13', 8)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (378, N'Điều gì xảy ra khi chia đôi cơn mơ', N'01:17', 8)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (379, N'Một thực tại kia không có em đợi chờ (đợi chờ)', N'01:20', 8)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (380, N'Nhìn từng hạt mưa rơi bên hiên vỡ tan', N'01:24', 8)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (381, N'Từng ký ức lỡ mang sao nỡ quên vội vàng', N'01:27', 8)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (382, N'Ở bên anh thêm một đêm thôi một đêm thôi', N'01:31', 8)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (383, N'Anh đã từng định nói nhưng rồi lại lặng im thôi lặng im thôi', N'01:34', 8)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (384, N'Vì anh biết không thể trói buộc', N'01:38', 8)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (385, N'Phía trước là bầu trời cao sâu', N'01:40', 8)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (386, N'Sống với những mơ ước thì chẳng được bao lâu', N'01:41', 8)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (387, N'Và tất cả đã hết sẽ chẳng có hồi kết', N'01:43', 8)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (388, N'Không một câu tạm biệt nhưng cũng chẳng sao đâu', N'01:45', 8)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (389, N'Anh vẫn thức giấc trên giường với giấc mơ vừa tàn', N'01:47', 8)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (390, N'Bản nhạc vụt tắt bộ phim kia dừng lại', N'01:50', 8)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (391, N'Nghe tiếng mưa rơi bên thềm anh ngước mắt lặng nhìn', N'01:54', 8)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (392, N'Rồi chờ đợi mãi vẫn không quay lại', N'01:58', 8)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (393, N'No no no', N'02:02', 8)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (394, N'Baby let me know', N'02:06', 8)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (395, N'No no no', N'02:10', 8)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (396, N'Baby let me know', N'02:14', 8)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (397, N'Lênh đênh trên ranh giới giữa thực tại', N'02:18', 8)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (398, N'Giật mình tỉnh giấc trống không cô đơn', N'02:20', 8)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (399, N'Hay mơ tiếp những giấc mơ chẳng thành', N'02:26', 8)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (400, N'Nhặt nhạnh từng chút hơi ấm em còn đâu đây', N'02:28', 8)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (401, N'Lại là một ngày mới anh thức giấc với thở dài', N'02:32', 8)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (402, N'Lại là một ngày mới đánh thức anh bằng nỗi đau', N'02:36', 8)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (403, N'Dù biết không có phép màu níu em quay trở lại', N'02:39', 8)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (404, N'Chỉ một lần sau cuối cho anh được thấy hình bóng em', N'02:43', 8)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (405, N'Yeah yeah ah', N'02:48', 8)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (406, N'Làm sao anh biết mình đang mơ hay thực tại', N'02:50', 8)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (407, N'Ah it feels so real', N'02:52', 8)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (408, N'Anh quay con quay mong con quay không dừng lại', N'02:54', 8)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (409, N'Ah nếu em hiện ra thì liệu anh có ngần ngại', N'02:56', 8)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (410, N'Thả mình để anh ngã chìm đắm trên đôi vai', N'02:58', 8)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (411, N'Hay là vùng dậy để tỉnh giấc không bên ai nghe nỗi đau thêm dài', N'03:00', 8)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (412, N'Ah càng muốn quên càng nhớ kỹ ghi lâu', N'03:02', 8)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (413, N'Trong giấc mơ liệu ta có bên nhau', N'03:05', 8)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (414, N'Khi anh thấy ở trong vòng tay anh chẳng hề có em', N'03:07', 8)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (415, N'Anh có nên nhắm mắt lại rồi lại đi xuống thêm sâu ah', N'03:09', 8)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (416, N'Một mai thức giấc ah', N'03:11', 8)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (417, N'Hay sẽ mãi mơ ah', N'03:13', 8)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (418, N'Đoạn phim lặp đi lặp lại trong đầu', N'03:15', 8)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (419, N'Anh không biết làm sao để mà anh thoát ra mau', N'03:17', 8)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (420, N'Anh vẫn thức giấc trên giường với giấc mơ vừa tàn', N'03:18', 8)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (421, N'Bản nhạc vụt tắt bộ phim kia dừng lại', N'03:21', 8)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (422, N'Nghe tiếng mưa rơi bên thềm anh ngước mắt lặng nhìn', N'03:25', 8)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (423, N'Rồi chờ đợi mãi vẫn không quay lại', N'03:28', 8)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (424, N'Anh vẫn thức giấc trên giường với giấc mơ vừa tàn', N'03:33', 8)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (425, N'Bản nhạc vụt tắt bộ phim kia dừng lại', N'03:36', 8)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (426, N'Nghe tiếng mưa rơi bên thềm anh ngước mắt lặng nhìn', N'03:40', 8)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (427, N'Rồi chờ đợi mãi vẫn không quay lại', N'03:44', 8)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (428, N'No no no', N'03:49', 8)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (429, N'Baby let me know (let me know)', N'03:52', 8)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (430, N'No no no', N'03:56', 8)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (431, N'Baby let me know (baby let me know)', N'04:00', 8)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (432, N'Nhìn xem lần sau cuối là bao điều tiếc nuối', N'04:03', 8)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (433, N'Tình yêu là thứ khiến em quên đi vài lần yếu đuối', N'04:07', 8)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (434, N'Lặng nhìn giọt sương rơi lạc trong màu u tối', N'04:10', 8)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (435, N'Là khi tình yêu ấy đã khiến em thôi những mộng mơ oh oh', N'04:14', 8)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (436, N'Nắng hạ đi ,mây trôi lang thang cho hạ buồn', N'00:37', 22)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (437, N'rong cỏi đốt đồng, để ngậm ngùi chim nhớ lá rừng', N'00:45', 22)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (438, N'Ai biết mẹ buồn vui khi mẹ kêu cậu tới gần', N'00:52', 22)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (439, N'biểu cậu ngồi, mẹ nhổ tóc sâu, hai chị em tóc bạc như nhau', N'00:59', 22)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (440, N'Đôi mắt cậu buồn hiu, phiêu lưu rong chơi những ngày đầu chừa ba vá miếng dừa đường làng xưa, dang nắng dầm mưa', N'01:07', 22)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (441, N'Ai cách xa cội nguồn, để mình ngồi', N'01:22', 22)
GO
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (442, N'nhớ lũy tre xanh dạo quanh, khung trời kỷ niệm', N'01:26', 22)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (443, N'chợt thèm rau đắng nấu canh', N'01:33', 22)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (444, N'Xin được làm mây mà bay khắp nơi giang hồ', N'01:40', 22)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (445, N'Ghé chốn quê hương xa rời từ cất bước ly thương', N'01:48', 22)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (446, N'Xin được làm gió dập dìu đưa điệu ca dao', N'01:55', 22)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (447, N'Trái bể phiến sau cũng ngọt ngào một lời cho nhau', N'02:03', 22)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (448, N'Xin sống lại tình yêu đơn sơ,', N'02:13', 22)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (449, N'rong chơi những ngày đầu chừa ba vá miếng dừa đường mòn xưa, dang nắng dầm mưa', N'02:17', 22)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (450, N'in nắng hạ cội nguồn để mình ngồi nhớ lũy tre xanh dạo quanh, khung trời kỷ niệm', N'02:28', 22)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (451, N'Chợt thèm rau đắng nấu canh ...', N'02:39', 22)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (452, N'Có một cuộc tình, không gọi là tình yêu', N'00:30', 23)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (453, N'Chẳng phải tình nhân hay ước hẹn duyên lứa đôi!', N'00:37', 23)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (454, N'Năm tháng bên nhau, ấp yêu từng kỷ niệm.', N'00:43', 23)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (455, N'Đắng cay ngọt ngào nồng thắm gửi vào tim.', N'00:49', 23)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (456, N'Có một cuộc tình, không buột ràng đời nhau!', N'00:58', 23)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (457, N'Vẫn nhớ vẫn thương tuy tháng ngày xa cách nhau.', N'01:05', 23)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (458, N'Thoáng chút bâng khuâng khi đêm về giá lạnh.', N'01:11', 23)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (459, N'Biết ai lòng buồn nên nhắn gửi chút niềm riêng.', N'01:17', 23)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (460, N'Tình Em và Tôi như nước trên đầu nguồn', N'01:26', 23)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (461, N'Tình Tôi và Em như làn gió mát trong đêm', N'01:32', 23)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (462, N'Lòng Em mừng vui khi nhìn tôi hạnh phúc', N'01:38', 23)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (463, N'Nhìn Em vui cười tôi thấy cả trời mộng mơ!', N'01:44', 23)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (464, N'Đó là cuộc tình cao đẹp hơn cả tình yêu', N'01:53', 23)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (465, N'Không gối chăn như nghĩa vợ tình chồng.', N'02:00', 23)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (466, N'Nhưng ấm nồng khi ta hiểu thấu đời nhau.', N'02:07', 23)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (467, N'Đó là cuộc tình xin đặt tên ”TRI KỶ”', N'02:16', 23)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (468, N'Đâu dễ nhòa phai khi nẻo đời bao trái ngang.', N'02:21', 23)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (469, N'Luôn giữ cho nhau tin yêu và chấp nhận.', N'02:28', 23)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (470, N'Để cho mình tình muôn ngàn kiếp vẹn nguyên.', N'02:34', 23)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (471, N'Người ơi! Nếu yêu rồi', N'00:28', 21)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (472, N'Chớ để buồn người trai nơi xa xôi', N'00:33', 21)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (473, N'Người ơi! Nếu thương rồi', N'00:39', 21)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (474, N'Chớ để nhạt màu son trên đôi môi', N'00:45', 21)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (475, N'Tôi kể rằng đêm nao sương rơi', N'00:51', 21)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (476, N'Rơi trên áo ai kết nên chuyện đời', N'00:55', 21)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (477, N'Và giữa khi tâm tư rét mướt', N'01:02', 21)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (478, N'Hồi còi hững hờ xé nát đêm dài', N'01:07', 21)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (479, N'Hành trang kín vai gầy', N'01:14', 21)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (480, N'Có một người mà tôi chưa quen tên', N'01:19', 21)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (481, N'Nhẹ vuốt mái tóc bồng', N'01:25', 21)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (482, N'Ngước nhìn bầu trời cao', N'01:31', 21)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (483, N'Ôi mênh mông', N'01:34', 21)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (484, N'Xe dập dồn băng đi trong sương', N'01:37', 21)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (485, N'Xa xa lướt nhanh cuốn theo bụi đường', N'01:42', 21)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (486, N'Và người đi trong cơn lốc xoáy', N'01:48', 21)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (487, N'Một mình ai còn', N'01:53', 21)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (488, N'Ngơ ngác trong đêm trường', N'01:56', 21)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (489, N'Tôi biết rồi đây có nhiều đêm', N'02:01', 21)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (490, N'Thức trọn vì tình yêu', N'02:05', 21)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (491, N'Vì thương anh vắng xa', N'02:08', 21)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (492, N'Biết có người bên song nhớ mong', N'02:13', 21)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (493, N'Khấn nguyện cùng trời cao', N'02:16', 21)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (494, N'Ước đẹp một vì sao', N'02:19', 21)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (495, N'Nhớ thương vô bờ', N'02:22', 21)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (496, N'Vì tình yêu như nụ hoa', N'02:25', 21)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (497, N'Chỉ nở một lần thôi', N'02:29', 21)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (498, N'Chỉ đẹp môt lần thôi', N'02:31', 21)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (499, N'Rồi ngày mai giữa muôn hoa', N'02:35', 21)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (500, N'Người về vơi thương nhớ', N'02:39', 21)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (501, N'Cho câu hát nên thơ', N'02:43', 21)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (502, N'Người đi biết đâu rằng', N'02:47', 21)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (503, N'Giữa lạnh lùng', N'02:52', 21)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (504, N'Mình ai trong sương đêm', N'02:54', 21)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (505, N'Tìm hái cánh hoa mềm', N'02:58', 21)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (506, N'Ép vào lòng mà nghe yêu thương thêm', N'03:03', 21)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (507, N'Đêm dần tàn, tâm tư miên man', N'03:10', 21)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (508, N'Thênh thang lối đi', N'03:14', 21)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (509, N'Nắng mai chập chùng', N'03:17', 21)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (510, N'Một lòng tin mai đây nắng ấm', N'03:21', 21)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (511, N'Tìm vào đôi lòng', N'03:26', 21)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (512, N'Giữa phút tương phùng', N'03:29', 21)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (513, N'Tôi biết rồi đây có nhiều đêm', N'03:45', 21)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (514, N'Thức trọn vì tình yêu', N'03:49', 21)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (515, N'Vì thương anh vắng xa', N'03:52', 21)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (516, N'Biết có người bên song nhớ mong', N'03:57', 21)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (517, N'Khấn nguyện cùng trời cao', N'04:00', 21)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (518, N'Ước đẹp một vì sao', N'04:04', 21)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (519, N'Nhớ thương vô bờ', N'04:07', 21)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (520, N'Vì tình yêu như nụ hoa', N'04:09', 21)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (521, N'Chỉ nở một lần thôi', N'04:12', 21)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (522, N'Chỉ đẹp môt lần thôi', N'04:15', 21)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (523, N'Rồi ngày mai giữa muôn hoa', N'04:19', 21)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (524, N'Người về vơi thương nhớ', N'04:24', 21)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (525, N'Cho câu hát nên thơ', N'04:27', 21)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (526, N'Người đi biết đâu rằng', N'04:31', 21)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (527, N'Giữa lạnh lùng', N'04:36', 21)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (528, N'Mình ai trong sương đêm', N'04:38', 21)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (529, N'Tìm hái cánh hoa mềm', N'04:43', 21)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (530, N'Ép vào lòng mà nghe yêu thương thêm', N'04:47', 21)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (531, N'Đêm dần tàn, tâm tư miên man', N'04:54', 21)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (532, N'Thênh thang lối đi', N'04:59', 21)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (533, N'Nắng mai chập chùng', N'05:01', 21)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (534, N'Một lòng tin mai đây nắng ấm', N'05:05', 21)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (535, N'Tìm vào đôi lòng', N'05:10', 21)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (536, N'Giữa phút tương phùng', N'05:12', 21)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (537, N'Một lòng tin mai đây nắng ấm', N'05:17', 21)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (538, N'Tìm vào đôi lòng', N'05:22', 21)
INSERT [dbo].[Lyrics] ([lyrics_ID], [content], [time], [song_ID]) VALUES (539, N'Giữa phút tương phùng', N'05:26', 21)
SET IDENTITY_INSERT [dbo].[Lyrics] OFF
SET IDENTITY_INSERT [dbo].[Roles] ON 

INSERT [dbo].[Roles] ([role_ID], [name], [description]) VALUES (1, N'Admin', N'Admin')
INSERT [dbo].[Roles] ([role_ID], [name], [description]) VALUES (2, N'Manager', N'Manager')
INSERT [dbo].[Roles] ([role_ID], [name], [description]) VALUES (3, N'Customer', N'Customer')
SET IDENTITY_INSERT [dbo].[Roles] OFF
SET IDENTITY_INSERT [dbo].[Slider] ON 

INSERT [dbo].[Slider] ([id], [song_ID], [heading], [text]) VALUES (1, 50, N'Listen Now', N'Listen to deep, lyrical V-Pop songs together!')
INSERT [dbo].[Slider] ([id], [song_ID], [heading], [text]) VALUES (2, 52, N'Discover Today', N'Discover today with upbeat US-UK music!')
INSERT [dbo].[Slider] ([id], [song_ID], [heading], [text]) VALUES (3, 54, N'Subscribe Today', N'Sign up to follow the gentle lofi music, relax after work!')
INSERT [dbo].[Slider] ([id], [song_ID], [heading], [text]) VALUES (4, 51, N'Listen Now', N'Listen to deep, lyrical V-Pop songs together!')
INSERT [dbo].[Slider] ([id], [song_ID], [heading], [text]) VALUES (5, 53, N'Discover Today', N'Discover today with upbeat US-UK music!')
INSERT [dbo].[Slider] ([id], [song_ID], [heading], [text]) VALUES (6, 55, N'Subscribe Today', N'Sign up to follow the gentle lofi music, relax after work!')
SET IDENTITY_INSERT [dbo].[Slider] OFF
SET IDENTITY_INSERT [dbo].[Songs] ON 

INSERT [dbo].[Songs] ([song_ID], [name], [author], [duration], [image], [path], [t_create], [t_lastUpdate], [category_ID], [like], [views]) VALUES (1, N'Đế vương', N'Đình Dũng', N'04:22', N'./img/song-img/vpop/de-vuong.jpg', N'./song-mp3/vpop/de-vuong.mp3', CAST(N'2022-02-22 13:25:00' AS SmallDateTime), CAST(N'2022-03-21 10:10:00' AS SmallDateTime), 1, NULL, 1)
INSERT [dbo].[Songs] ([song_ID], [name], [author], [duration], [image], [path], [t_create], [t_lastUpdate], [category_ID], [like], [views]) VALUES (2, N'#Catena ', N'Tóc Tiên, Tourliver', N'04:29', N'./img/song-img/vpop/catena.jpg', N'./song-mp3/vpop/catena.mp3', CAST(N'2022-02-22 13:25:00' AS SmallDateTime), CAST(N'2022-07-26 15:05:00' AS SmallDateTime), 1, NULL, 1)
INSERT [dbo].[Songs] ([song_ID], [name], [author], [duration], [image], [path], [t_create], [t_lastUpdate], [category_ID], [like], [views]) VALUES (3, N'Anh ơi ở lại ', N'ChiPu', N'05:41', N'./img/song-img/vpop/anh-oi-o-lai.jpg', N'./song-mp3/vpop/anh-oi-o-lai.mp3', CAST(N'2022-02-22 13:25:00' AS SmallDateTime), CAST(N'2022-03-14 15:34:00' AS SmallDateTime), 1, NULL, 1)
INSERT [dbo].[Songs] ([song_ID], [name], [author], [duration], [image], [path], [t_create], [t_lastUpdate], [category_ID], [like], [views]) VALUES (4, N'Ái nộ', N'Masew, Khôi Vũ', N'03:20', N'./img/song-img/vpop/ai-no.jpg', N'./song-mp3/vpop/ai-no.mp3', CAST(N'2022-02-22 13:25:00' AS SmallDateTime), CAST(N'2022-03-11 13:50:00' AS SmallDateTime), 1, NULL, 1)
INSERT [dbo].[Songs] ([song_ID], [name], [author], [duration], [image], [path], [t_create], [t_lastUpdate], [category_ID], [like], [views]) VALUES (5, N'Thê lương ', N'Phúc Chinh', N'05:13', N'./img/song-img/vpop/the-luong.jpg', N'./song-mp3/vpop/the-luong.mp3', CAST(N'2022-02-22 13:25:00' AS SmallDateTime), CAST(N'2022-03-14 15:32:00' AS SmallDateTime), 1, NULL, 1)
INSERT [dbo].[Songs] ([song_ID], [name], [author], [duration], [image], [path], [t_create], [t_lastUpdate], [category_ID], [like], [views]) VALUES (6, N'SG đau lòng quá', N'Hứa Kim Tuyền, Hoàng Duyên', N'05:08', N'./img/song-img/vpop/sg-dau-long-qua.jpg', N'./song-mp3/vpop/sg-dau-long-qua.mp3', CAST(N'2022-07-02 13:25:00' AS SmallDateTime), CAST(N'2022-03-14 00:07:00' AS SmallDateTime), 1, NULL, 1)
INSERT [dbo].[Songs] ([song_ID], [name], [author], [duration], [image], [path], [t_create], [t_lastUpdate], [category_ID], [like], [views]) VALUES (7, N'Cưới thôi ?', N'Masiu, Masew', N'03:01', N'./img/song-img/vpop/cuoi-thoi.jpg', N'./song-mp3/vpop/cuoi-thoi.mp3', CAST(N'2022-02-22 13:25:00' AS SmallDateTime), CAST(N'2022-03-14 15:35:00' AS SmallDateTime), 1, NULL, 1)
INSERT [dbo].[Songs] ([song_ID], [name], [author], [duration], [image], [path], [t_create], [t_lastUpdate], [category_ID], [like], [views]) VALUES (8, N'Thức giấc', N'DaLAB', N'04:29', N'./img/song-img/vpop/thuc-giac.jpg', N'./song-mp3/vpop/thuc-giac.mp3', CAST(N'2022-02-22 13:25:00' AS SmallDateTime), CAST(N'2022-07-26 15:16:00' AS SmallDateTime), 1, NULL, 1)
INSERT [dbo].[Songs] ([song_ID], [name], [author], [duration], [image], [path], [t_create], [t_lastUpdate], [category_ID], [like], [views]) VALUES (9, N'Đi về nhà', N'Đen, Justa Tee', N'03:25', N'./img/song-img/vpop/di-ve-nha.jpg', N'./song-mp3/vpop/di-ve-nha.mp3', CAST(N'2022-02-22 13:25:00' AS SmallDateTime), CAST(N'2022-07-02 09:25:00' AS SmallDateTime), 1, NULL, 1)
INSERT [dbo].[Songs] ([song_ID], [name], [author], [duration], [image], [path], [t_create], [t_lastUpdate], [category_ID], [like], [views]) VALUES (10, N'Gieo quẻ', N'Đen, Hoàng Thùy Linh', N'04:19', N'./img/song-img/vpop/gieo-que.jpg', N'./song-mp3/vpop/gieo-que.mp3', CAST(N'2022-02-22 13:25:00' AS SmallDateTime), CAST(N'2022-03-11 13:50:00' AS SmallDateTime), 1, NULL, 1)
INSERT [dbo].[Songs] ([song_ID], [name], [author], [duration], [image], [path], [t_create], [t_lastUpdate], [category_ID], [like], [views]) VALUES (11, N'Mang tiền về cho mẹ', N'Đen', N'06:41', N'./img/song-img/vpop/mang-tien-ve-cho-me.jpg', N'./song-mp3/vpop/mang-tien-ve-cho-me.mp3', CAST(N'2022-02-22 13:25:00' AS SmallDateTime), CAST(N'2022-03-11 13:50:00' AS SmallDateTime), 1, NULL, 1)
INSERT [dbo].[Songs] ([song_ID], [name], [author], [duration], [image], [path], [t_create], [t_lastUpdate], [category_ID], [like], [views]) VALUES (12, N'Rap chậm thôi', N'RPT MCK, RPT Jinn, RZ Ma$', N'04:09', N'./img/song-img/vpop/rap-cham-thoi.jpg', N'./song-mp3/vpop/rap-cham-thoi.mp3', CAST(N'2022-02-22 13:25:00' AS SmallDateTime), CAST(N'2022-07-26 15:15:00' AS SmallDateTime), 1, NULL, 1)
INSERT [dbo].[Songs] ([song_ID], [name], [author], [duration], [image], [path], [t_create], [t_lastUpdate], [category_ID], [like], [views]) VALUES (13, N'Thủ đô Cypher', N'RPT MCK, Low G,...', N'02:54', N'./img/song-img/vpop/thu-do-cypher.jpg', N'./song-mp3/vpop/thu-do-cypher.mp3', CAST(N'2022-02-22 13:25:00' AS SmallDateTime), CAST(N'2022-03-11 13:50:00' AS SmallDateTime), 1, NULL, 1)
INSERT [dbo].[Songs] ([song_ID], [name], [author], [duration], [image], [path], [t_create], [t_lastUpdate], [category_ID], [like], [views]) VALUES (14, N'Tháng năm', N'SooBin', N'03:56', N'./img/song-img/vpop/thang-nam.jpg', N'./song-mp3/vpop/thang-nam.mp3', CAST(N'2022-02-22 13:25:00' AS SmallDateTime), CAST(N'2022-03-11 13:50:00' AS SmallDateTime), 1, NULL, 1)
INSERT [dbo].[Songs] ([song_ID], [name], [author], [duration], [image], [path], [t_create], [t_lastUpdate], [category_ID], [like], [views]) VALUES (15, N'Chiều hôm ấy', N'JayKii', N'05:17', N'./img/song-img/vpop/chieu-hom-ay.jpg', N'./song-mp3/vpop/chieu-hom-ay.mp3', CAST(N'2022-06-25 13:25:00' AS SmallDateTime), CAST(N'2022-03-14 09:44:00' AS SmallDateTime), 1, NULL, 1)
INSERT [dbo].[Songs] ([song_ID], [name], [author], [duration], [image], [path], [t_create], [t_lastUpdate], [category_ID], [like], [views]) VALUES (16, N'Nàng thơ', N'Hoàng Dũng', N'05:21', N'./img/song-img/vpop/nang-tho.jpg', N'./song-mp3/vpop/nang-tho.mp3', CAST(N'2022-06-20 13:25:00' AS SmallDateTime), CAST(N'2022-03-21 10:18:00' AS SmallDateTime), 1, NULL, 1)
INSERT [dbo].[Songs] ([song_ID], [name], [author], [duration], [image], [path], [t_create], [t_lastUpdate], [category_ID], [like], [views]) VALUES (17, N'Dân chơi xóm', N'RPT MCK, Justa Tee', N'03:30', N'./img/song-img/vpop/dan-choi-xom.jpg', N'./song-mp3/vpop/dan-choi-xom.mp3', CAST(N'2022-02-22 13:25:00' AS SmallDateTime), CAST(N'2022-03-11 13:50:00' AS SmallDateTime), 1, NULL, 1)
INSERT [dbo].[Songs] ([song_ID], [name], [author], [duration], [image], [path], [t_create], [t_lastUpdate], [category_ID], [like], [views]) VALUES (18, N'Bài Này Chill Phết', N'Đen, MIN', N'04:33', N'./img/song-img/vpop/bai-nay-child-phet.jpg', N'./song-mp3/vpop/bai-nay-child-phet.mp3', CAST(N'2022-06-18 13:25:00' AS SmallDateTime), CAST(N'2022-03-11 13:50:00' AS SmallDateTime), 1, NULL, 1)
INSERT [dbo].[Songs] ([song_ID], [name], [author], [duration], [image], [path], [t_create], [t_lastUpdate], [category_ID], [like], [views]) VALUES (19, N'OK', N'BINZ', N'02:34', N'./img/song-img/vpop/ok.jpg', N'./song-mp3/vpop/ok.mp3', CAST(N'2016-10-07 13:25:00' AS SmallDateTime), CAST(N'2022-03-11 13:50:00' AS SmallDateTime), 1, NULL, 1)
INSERT [dbo].[Songs] ([song_ID], [name], [author], [duration], [image], [path], [t_create], [t_lastUpdate], [category_ID], [like], [views]) VALUES (20, N'Bậu ơi đừng khóc', N'Phi Nhung', N'04:51', N'./img/song-img/tru_tinh/Bau-Oi-Dung-Khoc-Phi-Nhung.jpg', N'./song-mp3/tru_tinh/Bau-Oi-Dung-Khoc-Phi-Nhung.mp3', CAST(N'2022-06-15 09:01:00' AS SmallDateTime), CAST(N'2022-07-26 15:11:00' AS SmallDateTime), 2, NULL, 1)
INSERT [dbo].[Songs] ([song_ID], [name], [author], [duration], [image], [path], [t_create], [t_lastUpdate], [category_ID], [like], [views]) VALUES (21, N'Chuyến đi về sáng ', N'Thúy Hà', N'05:50', N'./img/song-img/tru_tinh/Chuyen-Di-Ve-Sang-Khanh-Binh-Thuy-Ha.jpg', N'./song-mp3/tru_tinh/Chuyen-Di-Ve-Sang-Khanh-Binh-Thuy-Ha.mp3', CAST(N'2022-06-15 09:01:00' AS SmallDateTime), CAST(N'2022-07-26 15:18:00' AS SmallDateTime), 2, NULL, 1)
INSERT [dbo].[Songs] ([song_ID], [name], [author], [duration], [image], [path], [t_create], [t_lastUpdate], [category_ID], [like], [views]) VALUES (22, N'Còn thương rau đắng mọc sau hè', N'Dương Hồng Loan', N'05:02', N'./img/song-img/tru_tinh/Con-Thuong-Rau-Dang-Moc-Sau-He-Duong-Hong-Loan.jpg', N'./song-mp3/tru_tinh/Con-Thuong-Rau-Dang-Moc-Sau-He-Duong-Hong-Loan.mp3', CAST(N'2022-06-15 09:01:00' AS SmallDateTime), CAST(N'2022-07-26 15:17:00' AS SmallDateTime), 2, NULL, 1)
INSERT [dbo].[Songs] ([song_ID], [name], [author], [duration], [image], [path], [t_create], [t_lastUpdate], [category_ID], [like], [views]) VALUES (23, N'Cùng Một chữ tình', N'Mạnh Quỳnh', N'05:36', N'./img/song-img/tru_tinh/Cung-Mot-Chu-Tinh-Manh-Quynh.jpg', N'./song-mp3/tru_tinh/Cung-Mot-Chu-Tinh-Manh-Quynh.mp3', CAST(N'2022-06-15 09:01:00' AS SmallDateTime), CAST(N'2022-07-26 15:17:00' AS SmallDateTime), 2, NULL, 1)
INSERT [dbo].[Songs] ([song_ID], [name], [author], [duration], [image], [path], [t_create], [t_lastUpdate], [category_ID], [like], [views]) VALUES (24, N'Liên khúc trúc phương', N'Tố My', N'05:39', N'./img/song-img/tru_tinh/Lien-Khuc-Truc-Phuong-Nua-Dem-Ngoai-Pho-To-My-Phi-Nhung.jpg', N'./song-mp3/tru_tinh/Lien-Khuc-Truc-Phuong-Nua-Dem-Ngoai-Pho-To-My-Phi-Nhung.mp3', CAST(N'2022-06-15 09:01:00' AS SmallDateTime), CAST(N'2022-06-15 10:10:00' AS SmallDateTime), 2, NULL, 1)
INSERT [dbo].[Songs] ([song_ID], [name], [author], [duration], [image], [path], [t_create], [t_lastUpdate], [category_ID], [like], [views]) VALUES (25, N'Nỗi nhớ mưa phai', N'Mạnh Quỳnh', N'05:44', N'./img/song-img/tru_tinh/Noi-Nho-Mua-Phai-Manh-Quynh.jpg', N'./song-mp3/tru_tinh/Noi-Nho-Mua-Phai-Manh-Quynh.mp3', CAST(N'2022-06-15 09:01:00' AS SmallDateTime), CAST(N'2022-06-15 10:10:00' AS SmallDateTime), 2, NULL, 1)
INSERT [dbo].[Songs] ([song_ID], [name], [author], [duration], [image], [path], [t_create], [t_lastUpdate], [category_ID], [like], [views]) VALUES (26, N'Ốc đắng buồn ai', N'Đan Trường', N'05:42', N'./img/song-img/tru_tinh/Oc-Dang-Buon-Ai-Dan-Truong-To-My.jpg', N'./song-mp3/tru_tinh/Oc-Dang-Buon-Ai-Dan-Truong-To-My.mp3', CAST(N'2022-06-15 09:01:00' AS SmallDateTime), CAST(N'2022-06-15 10:10:00' AS SmallDateTime), 2, NULL, 1)
INSERT [dbo].[Songs] ([song_ID], [name], [author], [duration], [image], [path], [t_create], [t_lastUpdate], [category_ID], [like], [views]) VALUES (27, N'Phận đời con gái', N'Phi Nhung', N'05:07', N'./img/song-img/tru_tinh/Phan-Doi-Con-Gai-Phi-Nhung.jpg', N'./song-mp3/tru_tinh/Phan-Doi-Con-Gai-Phi-Nhung.mp3', CAST(N'2022-06-15 09:01:00' AS SmallDateTime), CAST(N'2022-06-15 10:10:00' AS SmallDateTime), 2, NULL, 1)
INSERT [dbo].[Songs] ([song_ID], [name], [author], [duration], [image], [path], [t_create], [t_lastUpdate], [category_ID], [like], [views]) VALUES (28, N'Trọn nghĩa phu thê', N'Quang Lê', N'04:34', N'./img/song-img/tru_tinh/Tron-Nghia-Phu-The-To-My-Quang-Le.jpg', N'./song-mp3/tru_tinh/Tron-Nghia-Phu-The-To-My-Quang-Le.mp3', CAST(N'2022-06-15 09:01:00' AS SmallDateTime), CAST(N'2022-06-15 10:10:00' AS SmallDateTime), 2, NULL, 1)
INSERT [dbo].[Songs] ([song_ID], [name], [author], [duration], [image], [path], [t_create], [t_lastUpdate], [category_ID], [like], [views]) VALUES (29, N'Chào em cô gái lam hồng', N'Trường Kha', N'04:44', N'./img/song-img/cach_mang/Chao-Em-Co-Gai-Lam-Hong-Truong-Kha.jpg', N'./song-mp3/tru_tinh/Chao-Em-Co-Gai-Lam-Hong-Truong-Kha.mp3', CAST(N'2022-06-15 09:01:00' AS SmallDateTime), CAST(N'2022-06-15 10:10:00' AS SmallDateTime), 3, NULL, 1)
INSERT [dbo].[Songs] ([song_ID], [name], [author], [duration], [image], [path], [t_create], [t_lastUpdate], [category_ID], [like], [views]) VALUES (30, N'Cùng anh tiến quân trên đường dài', N'Trường Kha', N'04:58', N'./img/song-img/cach_mang/Cung-Anh-Tien-Quan-Tren-Duong-Dai-Truong-Kha.jpg', N'./song-mp3/tru_tinh/Cung-Anh-Tien-Quan-Tren-Duong-Dai-Truong-Kha.mp3', CAST(N'2022-06-15 09:01:00' AS SmallDateTime), CAST(N'2022-06-15 10:10:00' AS SmallDateTime), 3, NULL, 1)
INSERT [dbo].[Songs] ([song_ID], [name], [author], [duration], [image], [path], [t_create], [t_lastUpdate], [category_ID], [like], [views]) VALUES (31, N'Đường tôi đi dài theo đất nước', N'Trường Kha', N'04:54', N'./img/song-img/cach_mang/Duong-Toi-Di-Dai-Theo-Dat-Nuoc-Truong-Kha.jpg', N'Duong-Toi-Di-Dai-Theo-Dat-Nuoc-Truong-Kha./song-mp3/tru_tinh/.mp3', CAST(N'2022-06-15 09:01:00' AS SmallDateTime), CAST(N'2022-06-15 10:10:00' AS SmallDateTime), 3, NULL, 1)
INSERT [dbo].[Songs] ([song_ID], [name], [author], [duration], [image], [path], [t_create], [t_lastUpdate], [category_ID], [like], [views]) VALUES (32, N'Đường trường sơn xe anh qua', N'Trường Kha', N'05:14', N'./img/song-img/cach_mang/Duong-Truong-Son-Xe-Anh-Qua-Truong-Kha.jpg', N'./song-mp3/tru_tinh/Duong-Truong-Son-Xe-Anh-Qua-Truong-Kha.mp3', CAST(N'2022-06-15 09:01:00' AS SmallDateTime), CAST(N'2022-06-15 10:10:00' AS SmallDateTime), 3, NULL, 1)
INSERT [dbo].[Songs] ([song_ID], [name], [author], [duration], [image], [path], [t_create], [t_lastUpdate], [category_ID], [like], [views]) VALUES (33, N'Màu hoa đỏ', N'Vũ Hoàng', N'05:05', N'./img/song-img/cach_mang/Mau-Hoa-Do-Vu-Hoang.jpg', N'./song-mp3/cach_mang/Mau-Hoa-Do-Vu-Hoang.mp3', CAST(N'2022-06-15 09:01:00' AS SmallDateTime), CAST(N'2022-06-15 10:10:00' AS SmallDateTime), 3, NULL, 1)
INSERT [dbo].[Songs] ([song_ID], [name], [author], [duration], [image], [path], [t_create], [t_lastUpdate], [category_ID], [like], [views]) VALUES (34, N'Ngọn đèn đứng gác', N'Trường Kha', N'05:04', N'./img/song-img/cach_mang/Ngon-Den-Dung-Gac-Truong-Kha.jpg', N'./song-mp3/cach_mang/Ngon-Den-Dung-Gac-Truong-Kha.mp3', CAST(N'2022-06-15 09:01:00' AS SmallDateTime), CAST(N'2022-06-15 10:10:00' AS SmallDateTime), 3, NULL, 1)
INSERT [dbo].[Songs] ([song_ID], [name], [author], [duration], [image], [path], [t_create], [t_lastUpdate], [category_ID], [like], [views]) VALUES (35, N'Nổi Lửa lên em', N'Trường Kha', N'05:04', N'./img/song-img/cach_mang/Noi-Lua-Len-Em-Truong-Kha.jpg', N'./song-mp3/cach_mang/Noi-Lua-Len-Em-Truong-Kha.mp3', CAST(N'2022-06-15 09:01:00' AS SmallDateTime), CAST(N'2022-06-15 10:10:00' AS SmallDateTime), 3, NULL, 1)
INSERT [dbo].[Songs] ([song_ID], [name], [author], [duration], [image], [path], [t_create], [t_lastUpdate], [category_ID], [like], [views]) VALUES (36, N'Tấm áo mẹ vá năm xưa', N'Trường Kha', N'04:47', N'./img/song-img/cach_mang/Tam-Ao-Me-Va-Nam-Xua-Truong-Kha.jpg', N'./song-mp3/cach_mang/Tam-Ao-Me-Va-Nam-Xua-Truong-Kha.mp3', CAST(N'2022-06-15 09:01:00' AS SmallDateTime), CAST(N'2022-06-15 10:10:00' AS SmallDateTime), 3, NULL, 1)
INSERT [dbo].[Songs] ([song_ID], [name], [author], [duration], [image], [path], [t_create], [t_lastUpdate], [category_ID], [like], [views]) VALUES (37, N'Tình ca', N'Trường Kha', N'04:44', N'./img/song-img/cach_mang/Tinh-Ca-Truong-Kha.jpg', N'./song-mp3/cach_mang/Tinh-Ca-Truong-Kha.mp3', CAST(N'2022-06-15 09:01:00' AS SmallDateTime), CAST(N'2022-06-15 10:10:00' AS SmallDateTime), 3, NULL, 1)
INSERT [dbo].[Songs] ([song_ID], [name], [author], [duration], [image], [path], [t_create], [t_lastUpdate], [category_ID], [like], [views]) VALUES (38, N'Memories', N'Maroon 5', N'03:15', N'./img/song-img/us-uk/memories.jpg', N'./song-mp3/us-uk/memories.mp3', CAST(N'2022-02-22 13:25:00' AS SmallDateTime), CAST(N'2022-03-14 21:48:00' AS SmallDateTime), 4, NULL, 1)
INSERT [dbo].[Songs] ([song_ID], [name], [author], [duration], [image], [path], [t_create], [t_lastUpdate], [category_ID], [like], [views]) VALUES (39, N'On my way', N'Alan Walker, Sabrina,..', N'03:36', N'./img/song-img/us-uk/on-my-way.jpg', N'./song-mp3/us-uk/on-my-way.mp3', CAST(N'2022-02-22 13:25:00' AS SmallDateTime), CAST(N'2022-03-11 13:50:00' AS SmallDateTime), 4, NULL, 1)
INSERT [dbo].[Songs] ([song_ID], [name], [author], [duration], [image], [path], [t_create], [t_lastUpdate], [category_ID], [like], [views]) VALUES (40, N'Señorita', N'Shawn Mendes, Camila,..', N'03:25', N'./img/song-img/us-uk/senorita.jpg', N'./song-mp3/us-uk/senorita.mp3', CAST(N'2022-02-22 13:25:00' AS SmallDateTime), CAST(N'2022-03-11 13:50:00' AS SmallDateTime), 4, NULL, 1)
INSERT [dbo].[Songs] ([song_ID], [name], [author], [duration], [image], [path], [t_create], [t_lastUpdate], [category_ID], [like], [views]) VALUES (41, N'Im Not Her', N'Clara Mae', N'03:12', N'./img/song-img/us-uk/im-not-her.jpg', N'./song-mp3/us-uk/im-not-her.mp3', CAST(N'2022-02-22 13:25:00' AS SmallDateTime), CAST(N'2022-03-11 13:50:00' AS SmallDateTime), 4, NULL, 1)
INSERT [dbo].[Songs] ([song_ID], [name], [author], [duration], [image], [path], [t_create], [t_lastUpdate], [category_ID], [like], [views]) VALUES (42, N'Someone You Loved ', N'Lewis Capaldi', N'03:01', N'./img/song-img/us-uk/someone-you-loved.jpg', N'./song-mp3/us-uk/someone-you-loved.mp3', CAST(N'2022-02-22 13:25:00' AS SmallDateTime), CAST(N'2022-03-17 23:37:00' AS SmallDateTime), 4, NULL, 1)
INSERT [dbo].[Songs] ([song_ID], [name], [author], [duration], [image], [path], [t_create], [t_lastUpdate], [category_ID], [like], [views]) VALUES (43, N'Dont Wanna Know', N'Maroon 5', N'06:18', N'./img/song-img/us-uk/dont-wanna-know.jpg', N'./song-mp3/us-uk/dont-wanna-know.mp3', CAST(N'2022-02-22 13:25:00' AS SmallDateTime), CAST(N'2022-03-11 13:50:00' AS SmallDateTime), 4, NULL, 1)
INSERT [dbo].[Songs] ([song_ID], [name], [author], [duration], [image], [path], [t_create], [t_lastUpdate], [category_ID], [like], [views]) VALUES (44, N'Blinding Lights', N'The Weeknd', N'03:23', N'./img/song-img/us-uk/blinding-lights.jpg', N'./song-mp3/us-uk/blinding-lights.mp3', CAST(N'2022-07-02 13:25:00' AS SmallDateTime), CAST(N'2022-03-11 13:50:00' AS SmallDateTime), 4, NULL, 1)
INSERT [dbo].[Songs] ([song_ID], [name], [author], [duration], [image], [path], [t_create], [t_lastUpdate], [category_ID], [like], [views]) VALUES (45, N'Dusk Till Dawn', N'ZAYN  Sia', N'03:55', N'./img/song-img/us-uk/dusk-till-dawn.jpg', N'./song-mp3/us-uk/dusk-till-dawn.mp3', CAST(N'2022-02-22 13:25:00' AS SmallDateTime), CAST(N'2022-03-11 13:50:00' AS SmallDateTime), 4, NULL, 1)
INSERT [dbo].[Songs] ([song_ID], [name], [author], [duration], [image], [path], [t_create], [t_lastUpdate], [category_ID], [like], [views]) VALUES (46, N'Cheap Thrills', N'Sia', N'04:05', N'./img/song-img/us-uk/cheap-thrills.jpg', N'./song-mp3/us-uk/cheap-thrills.mp3', CAST(N'2022-02-22 13:25:00' AS SmallDateTime), CAST(N'2022-03-11 13:50:00' AS SmallDateTime), 4, NULL, 1)
INSERT [dbo].[Songs] ([song_ID], [name], [author], [duration], [image], [path], [t_create], [t_lastUpdate], [category_ID], [like], [views]) VALUES (47, N'Treat You Better', N'Shawn Mendes', N'04:16', N'./img/song-img/us-uk/treat-you-better.jpg', N'./song-mp3/us-uk/treat-you-better.mp3', CAST(N'2022-02-22 13:25:00' AS SmallDateTime), CAST(N'2022-03-11 13:50:00' AS SmallDateTime), 4, NULL, 1)
INSERT [dbo].[Songs] ([song_ID], [name], [author], [duration], [image], [path], [t_create], [t_lastUpdate], [category_ID], [like], [views]) VALUES (48, N'Shape of You', N'Ed Sheeran', N'04:23', N'./img/song-img/us-uk/shape-of-you.jpg', N'./song-mp3/us-uk/shape-of-you.mp3', CAST(N'2022-02-22 13:25:00' AS SmallDateTime), CAST(N'2022-03-11 13:50:00' AS SmallDateTime), 4, NULL, 1)
INSERT [dbo].[Songs] ([song_ID], [name], [author], [duration], [image], [path], [t_create], [t_lastUpdate], [category_ID], [like], [views]) VALUES (49, N'Hello', N'Adele', N'06:06', N'./img/song-img/us-uk/hello.jpg', N'./song-mp3/us-uk/hello.mp3', CAST(N'2022-02-22 13:25:00' AS SmallDateTime), CAST(N'2022-03-11 13:50:00' AS SmallDateTime), 4, NULL, 1)
INSERT [dbo].[Songs] ([song_ID], [name], [author], [duration], [image], [path], [t_create], [t_lastUpdate], [category_ID], [like], [views]) VALUES (50, N'Yummy', N'Justin Bieber', N'03:50', N'./img/song-img/us-uk/yummy.jpg', N'./song-mp3/us-uk/yummy.mp3', CAST(N'2022-02-22 13:25:00' AS SmallDateTime), CAST(N'2022-07-02 13:50:00' AS SmallDateTime), 4, NULL, 1)
INSERT [dbo].[Songs] ([song_ID], [name], [author], [duration], [image], [path], [t_create], [t_lastUpdate], [category_ID], [like], [views]) VALUES (51, N'Send My Love', N'Adele', N'02:22', N'./img/song-img/us-uk/send-my-love.jpg', N'./song-mp3/us-uk/send-my-love.mp3', CAST(N'2022-02-22 13:25:00' AS SmallDateTime), CAST(N'2022-03-11 13:50:00' AS SmallDateTime), 4, NULL, 1)
INSERT [dbo].[Songs] ([song_ID], [name], [author], [duration], [image], [path], [t_create], [t_lastUpdate], [category_ID], [like], [views]) VALUES (52, N'FRIENDS', N'Marshmello, AnneMarie', N'03:51', N'./img/song-img/us-uk/friends.jpg', N'./song-mp3/us-uk/friends.mp3', CAST(N'2022-02-22 13:25:00' AS SmallDateTime), CAST(N'2022-03-11 13:50:00' AS SmallDateTime), 4, NULL, 1)
INSERT [dbo].[Songs] ([song_ID], [name], [author], [duration], [image], [path], [t_create], [t_lastUpdate], [category_ID], [like], [views]) VALUES (53, N'Perfect', N'Ed Sheeran', N'04:39', N'./img/song-img/us-uk/perfect.jpg', N'./song-mp3/us-uk/perfect.mp3', CAST(N'2016-08-18 13:25:00' AS SmallDateTime), CAST(N'2022-03-11 13:50:00' AS SmallDateTime), 4, NULL, 1)
INSERT [dbo].[Songs] ([song_ID], [name], [author], [duration], [image], [path], [t_create], [t_lastUpdate], [category_ID], [like], [views]) VALUES (54, N'Legends Never Die', N'Against The Current', N'02:59', N'./img/song-img/us-uk/legends-never-die.jpg', N'./song-mp3/us-uk/legends-never-die.mp3', CAST(N'2017-09-09 13:25:00' AS SmallDateTime), CAST(N'2022-03-11 13:50:00' AS SmallDateTime), 4, NULL, 1)
INSERT [dbo].[Songs] ([song_ID], [name], [author], [duration], [image], [path], [t_create], [t_lastUpdate], [category_ID], [like], [views]) VALUES (55, N'Come Back Again', N'Infinite', N'03:06', N'./img/song-img/kpop/Come-Back-Again-Infinite.jpg', N'./song-mp3/kpop/Come-Back-Again-Infinite.mp3', CAST(N'2022-07-02 09:01:00' AS SmallDateTime), CAST(N'2022-06-15 10:10:00' AS SmallDateTime), 5, NULL, 1)
INSERT [dbo].[Songs] ([song_ID], [name], [author], [duration], [image], [path], [t_create], [t_lastUpdate], [category_ID], [like], [views]) VALUES (56, N'Day By Day', N'T-ARA', N'03:29', N'./img/song-img/kpop/Day-By-Day-T-ARA.jpg', N'./song-mp3/kpop/Day-By-Day-T-ARA.mp3', CAST(N'2022-06-15 09:01:00' AS SmallDateTime), CAST(N'2022-06-15 10:10:00' AS SmallDateTime), 5, NULL, 1)
INSERT [dbo].[Songs] ([song_ID], [name], [author], [duration], [image], [path], [t_create], [t_lastUpdate], [category_ID], [like], [views]) VALUES (57, N'How Dare You', N'SISTAR', N'03:06', N'./img/song-img/kpop/How-Dare-You-SISTAR.jpg', N'./song-mp3/kpop/How-Dare-You-SISTAR.mp3', CAST(N'2022-06-15 09:01:00' AS SmallDateTime), CAST(N'2022-06-15 10:10:00' AS SmallDateTime), 5, NULL, 1)
INSERT [dbo].[Songs] ([song_ID], [name], [author], [duration], [image], [path], [t_create], [t_lastUpdate], [category_ID], [like], [views]) VALUES (58, N'Im a Loner', N'CNBlue', N'03:39', N'./img/song-img/kpop/I-m-a-Loner-CNBlue.jpg', N'./song-mp3/kpop/I-m-a-Loner-CNBlue.mp3', CAST(N'2022-06-15 09:01:00' AS SmallDateTime), CAST(N'2022-06-15 10:10:00' AS SmallDateTime), 5, NULL, 1)
INSERT [dbo].[Songs] ([song_ID], [name], [author], [duration], [image], [path], [t_create], [t_lastUpdate], [category_ID], [like], [views]) VALUES (59, N'Love Ya', N'SS501', N'03:25', N'./img/song-img/kpop/Love-Ya-SS501.jpg', N'./song-mp3/kpop/Love-Ya-SS501.mp3', CAST(N'2022-06-15 09:01:00' AS SmallDateTime), CAST(N'2022-06-15 10:10:00' AS SmallDateTime), 5, NULL, 1)
INSERT [dbo].[Songs] ([song_ID], [name], [author], [duration], [image], [path], [t_create], [t_lastUpdate], [category_ID], [like], [views]) VALUES (60, N'Muzik', N'4MINUTE', N'03:45', N'./img/song-img/kpop/Muzik-4MINUTE.jpg', N'./song-mp3/kpop/Muzik-4MINUTE.mp3', CAST(N'2022-06-15 09:01:00' AS SmallDateTime), CAST(N'2022-06-15 10:10:00' AS SmallDateTime), 5, NULL, 1)
INSERT [dbo].[Songs] ([song_ID], [name], [author], [duration], [image], [path], [t_create], [t_lastUpdate], [category_ID], [like], [views]) VALUES (61, N'On Rainy Days', N'BEAST', N'03:45', N'./img/song-img/kpop/On-Rainy-Days-BEAST.jpg', N'./song-mp3/kpop/On-Rainy-Days-BEAST.mp3', CAST(N'2022-06-15 09:01:00' AS SmallDateTime), CAST(N'2022-06-15 10:10:00' AS SmallDateTime), 5, NULL, 1)
INSERT [dbo].[Songs] ([song_ID], [name], [author], [duration], [image], [path], [t_create], [t_lastUpdate], [category_ID], [like], [views]) VALUES (62, N'Psycho Red', N'Velvet', N'03:30', N'./img/song-img/kpop/Psycho-Red-Velvet.jpg', N'./song-mp3/kpop/Psycho-Red-Velvet.mp3', CAST(N'2022-06-15 09:01:00' AS SmallDateTime), CAST(N'2022-06-15 10:10:00' AS SmallDateTime), 5, NULL, 1)
INSERT [dbo].[Songs] ([song_ID], [name], [author], [duration], [image], [path], [t_create], [t_lastUpdate], [category_ID], [like], [views]) VALUES (63, N'ACUTE', N'Hatsune-Miku-Megurine-Luka-KAITO', N'04:38', N'./img/song-img/anime/ACUTE-Hatsune-Miku-Megurine-Luka-KAITO.jpg', N'./song-mp3/anime/ACUTE-Hatsune-Miku-Megurine-Luka-KAITO.mp3', CAST(N'2022-06-15 09:01:00' AS SmallDateTime), CAST(N'2022-06-15 10:10:00' AS SmallDateTime), 6, NULL, 1)
INSERT [dbo].[Songs] ([song_ID], [name], [author], [duration], [image], [path], [t_create], [t_lastUpdate], [category_ID], [like], [views]) VALUES (64, N'Electric-Angel', N'Hatsune-Miku', N'03:21', N'./img/song-img/anime/Electric-Angel-Hatsune-Miku.jpg', N'./song-mp3/anime/Electric-Angel-Hatsune-Miku.mp3', CAST(N'2022-06-15 09:01:00' AS SmallDateTime), CAST(N'2022-06-15 10:10:00' AS SmallDateTime), 6, NULL, 1)
INSERT [dbo].[Songs] ([song_ID], [name], [author], [duration], [image], [path], [t_create], [t_lastUpdate], [category_ID], [like], [views]) VALUES (65, N'Euterpe', N'EGOIST', N'03:48', N'./img/song-img/anime/Euterpe-EGOIST.jpg', N'./song-mp3/anime/Euterpe-EGOIST.mp3', CAST(N'2022-06-15 09:01:00' AS SmallDateTime), CAST(N'2022-06-15 10:10:00' AS SmallDateTime), 6, NULL, 1)
INSERT [dbo].[Songs] ([song_ID], [name], [author], [duration], [image], [path], [t_create], [t_lastUpdate], [category_ID], [like], [views]) VALUES (66, N'Hotaru', N'Fujita-Maiko', N'04:55', N'./img/song-img/anime/Hotaru-Fujita-Maiko.jpg', N'./song-mp3/anime/Hotaru-Fujita-Maiko.mp3', CAST(N'2022-06-15 09:01:00' AS SmallDateTime), CAST(N'2022-06-15 10:10:00' AS SmallDateTime), 6, NULL, 1)
INSERT [dbo].[Songs] ([song_ID], [name], [author], [duration], [image], [path], [t_create], [t_lastUpdate], [category_ID], [like], [views]) VALUES (67, N'L-L-L', N'MYTH-amp-ROID', N'03:32', N'./img/song-img/anime/L-L-L-MYTH-amp-ROID.jpg', N'./song-mp3/anime/L-L-L-MYTH-amp-ROID.mp3', CAST(N'2022-06-15 09:01:00' AS SmallDateTime), CAST(N'2022-06-15 10:10:00' AS SmallDateTime), 6, NULL, 1)
INSERT [dbo].[Songs] ([song_ID], [name], [author], [duration], [image], [path], [t_create], [t_lastUpdate], [category_ID], [like], [views]) VALUES (68, N'Owari no Sekai Kara', N'Yanagi-Nagi', N'05:57', N'./img/song-img/anime/Owari-no-Sekai-Kara-Yanagi-Nagi.jpg', N'./song-mp3/anime/Owari-no-Sekai-Kara-Yanagi-Nagi.mp3', CAST(N'2022-06-15 09:01:00' AS SmallDateTime), CAST(N'2022-06-15 10:10:00' AS SmallDateTime), 6, NULL, 1)
INSERT [dbo].[Songs] ([song_ID], [name], [author], [duration], [image], [path], [t_create], [t_lastUpdate], [category_ID], [like], [views]) VALUES (69, N'ReAct', N'Hatsune-Miku-Kagamine-Rin-Kagamine-Len', N'04:47', N'./img/song-img/anime/ReAct-Hatsune-Miku-Kagamine-Rin-Kagamine-Len.jpg', N'./song-mp3/anime/ReAct-Hatsune-Miku-Kagamine-Rin-Kagamine-Len.mp3', CAST(N'2022-06-15 09:01:00' AS SmallDateTime), CAST(N'2022-06-15 10:10:00' AS SmallDateTime), 6, NULL, 1)
INSERT [dbo].[Songs] ([song_ID], [name], [author], [duration], [image], [path], [t_create], [t_lastUpdate], [category_ID], [like], [views]) VALUES (70, N'Renai Circulation', N'Kana-Hanazawa', N'04:13', N'./img/song-img/anime/Renai-Circulation-Kana-Hanazawa.jpg', N'./song-mp3/anime/Renai-Circulation-Kana-Hanazawa.mp3', CAST(N'2022-06-15 09:01:00' AS SmallDateTime), CAST(N'2022-06-15 10:10:00' AS SmallDateTime), 6, NULL, 1)
INSERT [dbo].[Songs] ([song_ID], [name], [author], [duration], [image], [path], [t_create], [t_lastUpdate], [category_ID], [like], [views]) VALUES (71, N'Sakura', N'Ikimono-Gakari', N'05:51', N'./img/song-img/anime/Sakura-Ikimono-Gakari.jpg', N'./song-mp3/anime/Sakura-Ikimono-Gakari.mp3', CAST(N'2022-06-15 09:01:00' AS SmallDateTime), CAST(N'2022-06-15 10:10:00' AS SmallDateTime), 6, NULL, 1)
INSERT [dbo].[Songs] ([song_ID], [name], [author], [duration], [image], [path], [t_create], [t_lastUpdate], [category_ID], [like], [views]) VALUES (72, N'Nothin On You', N'BoB, Bruno Mars', N'03:50', N'./img/song-img/lofi/nothin-on-you.jpg', N'./song-mp3/lofi/nothin-on-you.mp3', CAST(N'2022-02-22 13:25:00' AS SmallDateTime), CAST(N'2022-03-13 23:23:00' AS SmallDateTime), 7, NULL, 1)
INSERT [dbo].[Songs] ([song_ID], [name], [author], [duration], [image], [path], [t_create], [t_lastUpdate], [category_ID], [like], [views]) VALUES (73, N'Lemon Tree ', N'Fools Garden', N'03:44', N'./img/song-img/lofi/lemon-tree.jpg', N'./song-mp3/lofi/lemon-tree.mp3', CAST(N'2022-02-22 13:25:00' AS SmallDateTime), CAST(N'2022-03-14 00:17:00' AS SmallDateTime), 7, NULL, 1)
INSERT [dbo].[Songs] ([song_ID], [name], [author], [duration], [image], [path], [t_create], [t_lastUpdate], [category_ID], [like], [views]) VALUES (74, N'At My Worst', N'Pink Sweat', N'03:05', N'./img/song-img/lofi/at-my-worst.jpg', N'./song-mp3/lofi/at-my-worst.mp3', CAST(N'2022-02-22 13:25:00' AS SmallDateTime), CAST(N'2022-03-11 13:50:00' AS SmallDateTime), 7, NULL, 1)
INSERT [dbo].[Songs] ([song_ID], [name], [author], [duration], [image], [path], [t_create], [t_lastUpdate], [category_ID], [like], [views]) VALUES (75, N'Mood', N'24kGoldn', N'02:30', N'./img/song-img/lofi/mood.jpg', N'./song-mp3/lofi/mood.mp3', CAST(N'2022-02-22 13:25:00' AS SmallDateTime), CAST(N'2022-03-11 13:50:00' AS SmallDateTime), 7, NULL, 1)
INSERT [dbo].[Songs] ([song_ID], [name], [author], [duration], [image], [path], [t_create], [t_lastUpdate], [category_ID], [like], [views]) VALUES (76, N'SugarCrash', N'ElyOtto', N'01:20', N'./img/song-img/lofi/sugarcrash.jpg', N'./song-mp3/lofi/sugarcrash.mp3', CAST(N'2022-02-22 13:25:00' AS SmallDateTime), CAST(N'2022-03-11 13:50:00' AS SmallDateTime), 7, NULL, 1)
INSERT [dbo].[Songs] ([song_ID], [name], [author], [duration], [image], [path], [t_create], [t_lastUpdate], [category_ID], [like], [views]) VALUES (77, N'Positions', N'Ariana Grande', N'02:57', N'./img/song-img/lofi/positions.jpg', N'./song-mp3/lofi/positions.mp3', CAST(N'2022-02-22 13:25:00' AS SmallDateTime), CAST(N'2022-03-11 13:50:00' AS SmallDateTime), 7, NULL, 1)
INSERT [dbo].[Songs] ([song_ID], [name], [author], [duration], [image], [path], [t_create], [t_lastUpdate], [category_ID], [like], [views]) VALUES (78, N'I Love You Baby', N'Frank Sinatra', N'03:56', N'./img/song-img/lofi/i-love-you-baby.jpg', N'./song-mp3/lofi/i-love-you-baby.mp3', CAST(N'2022-02-22 13:25:00' AS SmallDateTime), CAST(N'2022-03-11 13:50:00' AS SmallDateTime), 7, NULL, 1)
INSERT [dbo].[Songs] ([song_ID], [name], [author], [duration], [image], [path], [t_create], [t_lastUpdate], [category_ID], [like], [views]) VALUES (79, N'What A Wonderful World', N'Louis Armstrong', N'02:17', N'./img/song-img/lofi/what-a-wonderful-world.jpg', N'./song-mp3/lofi/what-a-wonderful-world.mp3', CAST(N'2022-02-22 13:25:00' AS SmallDateTime), CAST(N'2022-03-11 13:50:00' AS SmallDateTime), 7, NULL, 1)
INSERT [dbo].[Songs] ([song_ID], [name], [author], [duration], [image], [path], [t_create], [t_lastUpdate], [category_ID], [like], [views]) VALUES (80, N'I Want To Hold Your', N'The Beatles', N'02:36', N'./img/song-img/lofi/i-want-to-hold-your.jpg', N'./song-mp3/lofi/i-want-to-hold-your.mp3', CAST(N'2022-02-22 13:25:00' AS SmallDateTime), CAST(N'2022-03-11 13:50:00' AS SmallDateTime), 7, NULL, 1)
INSERT [dbo].[Songs] ([song_ID], [name], [author], [duration], [image], [path], [t_create], [t_lastUpdate], [category_ID], [like], [views]) VALUES (81, N'Death Bed Coffee', N'Powfu', N'02:53', N'./img/song-img/lofi/death-bed-coffee.jpg', N'./song-mp3/lofi/death-bed-coffee.mp3', CAST(N'2022-02-22 13:25:00' AS SmallDateTime), CAST(N'2022-03-11 13:50:00' AS SmallDateTime), 7, NULL, 1)
INSERT [dbo].[Songs] ([song_ID], [name], [author], [duration], [image], [path], [t_create], [t_lastUpdate], [category_ID], [like], [views]) VALUES (82, N'Cant Help Falling In Love ', N'Elvis Presley', N'03:00', N'./img/song-img/lofi/cant-help-falling-in-love.jpg', N'./song-mp3/lofi/cant-help-falling-in-love.mp3', CAST(N'2022-02-22 13:25:00' AS SmallDateTime), CAST(N'2022-03-14 00:22:00' AS SmallDateTime), 7, NULL, 1)
INSERT [dbo].[Songs] ([song_ID], [name], [author], [duration], [image], [path], [t_create], [t_lastUpdate], [category_ID], [like], [views]) VALUES (83, N'Crystal Dolphin', N'Engelwood', N'01:53', N'./img/song-img/lofi/crystal-dolphin.jpg', N'./song-mp3/lofi/crystal-dolphin.mp3', CAST(N'2022-02-22 13:25:00' AS SmallDateTime), CAST(N'2022-03-11 13:50:00' AS SmallDateTime), 7, NULL, 1)
INSERT [dbo].[Songs] ([song_ID], [name], [author], [duration], [image], [path], [t_create], [t_lastUpdate], [category_ID], [like], [views]) VALUES (84, N'Someone You Loved', N'Lewis Capaldi', N'03:01', N'./img/song-img/lofi/someone-you-loved.jpg', N'./song-mp3/lofi/someone-you-loved.mp3', CAST(N'2022-02-22 13:25:00' AS SmallDateTime), CAST(N'2022-03-11 13:50:00' AS SmallDateTime), 7, NULL, 1)
INSERT [dbo].[Songs] ([song_ID], [name], [author], [duration], [image], [path], [t_create], [t_lastUpdate], [category_ID], [like], [views]) VALUES (85, N'Let Me Down Slowly', N'Alec Benjamin', N'02:57', N'./img/song-img/lofi/let-me-down-slowly.jpg', N'./song-mp3/lofi/let-me-down-slowly.mp3', CAST(N'2022-02-22 13:25:00' AS SmallDateTime), CAST(N'2022-03-11 13:50:00' AS SmallDateTime), 7, NULL, 1)
INSERT [dbo].[Songs] ([song_ID], [name], [author], [duration], [image], [path], [t_create], [t_lastUpdate], [category_ID], [like], [views]) VALUES (86, N'Star Shopping', N'Lil Peep, Kryptik', N'02:22', N'./img/song-img/lofi/star-shopping.jpg', N'./song-mp3/lofi/star-shopping.mp3', CAST(N'2022-02-22 13:25:00' AS SmallDateTime), CAST(N'2022-03-11 13:50:00' AS SmallDateTime), 7, NULL, 1)
INSERT [dbo].[Songs] ([song_ID], [name], [author], [duration], [image], [path], [t_create], [t_lastUpdate], [category_ID], [like], [views]) VALUES (87, N'Be Like That', N'Kane Brown, Swae Lee', N'03:11', N'./img/song-img/lofi/be-like-that.jpg', N'./song-mp3/lofi/be-like-that.mp3', CAST(N'2022-02-22 13:25:00' AS SmallDateTime), CAST(N'2022-03-11 13:50:00' AS SmallDateTime), 7, NULL, 1)
INSERT [dbo].[Songs] ([song_ID], [name], [author], [duration], [image], [path], [t_create], [t_lastUpdate], [category_ID], [like], [views]) VALUES (88, N'Runaway', N'AURORA', N'04:08', N'./img/song-img/lofi/runaway.jpg', N'./song-mp3/lofi/runaway.mp3', CAST(N'2022-02-22 13:25:00' AS SmallDateTime), CAST(N'2022-07-02 13:50:00' AS SmallDateTime), 7, NULL, 1)
INSERT [dbo].[Songs] ([song_ID], [name], [author], [duration], [image], [path], [t_create], [t_lastUpdate], [category_ID], [like], [views]) VALUES (89, N'The Girl Ive Never Met', N'Gustixa', N'04:33', N'./img/song-img/lofi/the-girl-ive-never-met.jpg', N'./song-mp3/lofi/the-girl-ive-never-met.mp3', CAST(N'2019-06-20 13:25:00' AS SmallDateTime), CAST(N'2022-03-11 13:50:00' AS SmallDateTime), 7, NULL, 1)
INSERT [dbo].[Songs] ([song_ID], [name], [author], [duration], [image], [path], [t_create], [t_lastUpdate], [category_ID], [like], [views]) VALUES (90, N'Surrender', N'Natalie Taylor', N'03:39', N'./img/song-img/lofi/surrender.jpg', N'./song-mp3/lofi/surrender.mp3', CAST(N'2019-10-19 13:25:00' AS SmallDateTime), CAST(N'2022-03-11 13:50:00' AS SmallDateTime), 7, NULL, 1)
INSERT [dbo].[Songs] ([song_ID], [name], [author], [duration], [image], [path], [t_create], [t_lastUpdate], [category_ID], [like], [views]) VALUES (91, N'Dem Sao', N'Vu Dang Quoc Viet', N'04:31', N'./img/song-img/piano/Dem-Sao-Vu-Dang-Quoc-Viet.jpg', N'./song-mp3/piano/Dem-Sao-Vu-Dang-Quoc-Viet.mp3', CAST(N'2022-06-15 09:01:00' AS SmallDateTime), CAST(N'2022-06-15 10:10:00' AS SmallDateTime), 8, NULL, 1)
INSERT [dbo].[Songs] ([song_ID], [name], [author], [duration], [image], [path], [t_create], [t_lastUpdate], [category_ID], [like], [views]) VALUES (92, N'Hymn Piano', N'Various Artists', N'03:13', N'./img/song-img/piano/Hymn-Piano-Various-Artists.jpg', N'./song-mp3/piano/Hymn-Piano-Various-Artists.mp3', CAST(N'2022-06-15 09:01:00' AS SmallDateTime), CAST(N'2022-06-15 10:10:00' AS SmallDateTime), 8, NULL, 1)
INSERT [dbo].[Songs] ([song_ID], [name], [author], [duration], [image], [path], [t_create], [t_lastUpdate], [category_ID], [like], [views]) VALUES (93, N'Little Comfort', N'The Daydream', N'04:16', N'./img/song-img/piano/Little-Comfort-The-Daydream.jpg', N'./song-mp3/piano/Little-Comfort-The-Daydream.mp3', CAST(N'2022-06-15 09:01:00' AS SmallDateTime), CAST(N'2022-06-15 10:10:00' AS SmallDateTime), 8, NULL, 1)
INSERT [dbo].[Songs] ([song_ID], [name], [author], [duration], [image], [path], [t_create], [t_lastUpdate], [category_ID], [like], [views]) VALUES (94, N'Niem Vui Cua Em', N'Vu Dang Quoc Viet', N'03:26', N'./img/song-img/piano/Niem-Vui-Cua-Em-Vu-Dang-Quoc-Viet.jpg', N'./song-mp3/piano/Niem-Vui-Cua-Em-Vu-Dang-Quoc-Viet.mp3', CAST(N'2022-06-15 09:01:00' AS SmallDateTime), CAST(N'2022-06-15 10:10:00' AS SmallDateTime), 8, NULL, 1)
INSERT [dbo].[Songs] ([song_ID], [name], [author], [duration], [image], [path], [t_create], [t_lastUpdate], [category_ID], [like], [views]) VALUES (95, N'Night Of The Piano 6', N'Jin Shi', N'01:48', N'./img/song-img/piano/Night-Of-The-Piano-6-Jin-Shi.jpg', N'./song-mp3/piano/Night-Of-The-Piano-6-Jin-Shi.mp3', CAST(N'2022-06-15 09:01:00' AS SmallDateTime), CAST(N'2022-06-15 10:10:00' AS SmallDateTime), 8, NULL, 1)
INSERT [dbo].[Songs] ([song_ID], [name], [author], [duration], [image], [path], [t_create], [t_lastUpdate], [category_ID], [like], [views]) VALUES (96, N'Right Here Waiting For You', N'Richard Clayderman', N'03:47', N'./img/song-img/piano/Right-Here-Waiting-For-You-Richard-Clayderman.jpg', N'./song-mp3/piano/Right-Here-Waiting-For-You-Richard-Clayderman.mp3', CAST(N'2022-06-15 09:01:00' AS SmallDateTime), CAST(N'2022-06-15 10:10:00' AS SmallDateTime), 8, NULL, 1)
INSERT [dbo].[Songs] ([song_ID], [name], [author], [duration], [image], [path], [t_create], [t_lastUpdate], [category_ID], [like], [views]) VALUES (97, N'Smile Smile Smile', N'Jeon Soo Yeon', N'03:04', N'./img/song-img/piano/Smile-Smile-Smile-Jeon-Soo-Yeon.jpg', N'./song-mp3/piano/Smile-Smile-Smile-Jeon-Soo-Yeon.mp3', CAST(N'2022-06-15 09:01:00' AS SmallDateTime), CAST(N'2022-06-15 10:10:00' AS SmallDateTime), 8, NULL, 1)
INSERT [dbo].[Songs] ([song_ID], [name], [author], [duration], [image], [path], [t_create], [t_lastUpdate], [category_ID], [like], [views]) VALUES (98, N'Thang Cuoi', N'Vu Dang Quoc Viet', N'03:07', N'./img/song-img/piano/Thang-Cuoi-Vu-Dang-Quoc-Viet.jpg', N'./song-mp3/piano/Thang-Cuoi-Vu-Dang-Quoc-Viet.mp3', CAST(N'2022-06-15 09:01:00' AS SmallDateTime), CAST(N'2022-06-15 10:10:00' AS SmallDateTime), 8, NULL, 1)
INSERT [dbo].[Songs] ([song_ID], [name], [author], [duration], [image], [path], [t_create], [t_lastUpdate], [category_ID], [like], [views]) VALUES (99, N'Trai Dat Nay La Cua Chung Minh', N'Vu Dang Quoc Viet', N'02:39', N'./img/song-img/piano/Trai-Dat-Nay-La-Cua-Chung-Minh-Vu-Dang-Quoc-Viet.jpg', N'./song-mp3/piano/Trai-Dat-Nay-La-Cua-Chung-Minh-Vu-Dang-Quoc-Viet.mp3', CAST(N'2022-06-15 09:01:00' AS SmallDateTime), CAST(N'2022-06-15 10:10:00' AS SmallDateTime), 8, NULL, 1)
GO
INSERT [dbo].[Songs] ([song_ID], [name], [author], [duration], [image], [path], [t_create], [t_lastUpdate], [category_ID], [like], [views]) VALUES (100, N'Bien Nho', N'Do Dinh Phuong', N'03:27', N'./img/song-img/guitar/Bien-Nho-Do-Dinh-Phuong.jpg', N'./song-mp3/guitar/Bien-Nho-Do-Dinh-Phuong.mp3', CAST(N'2022-06-15 09:01:00' AS SmallDateTime), CAST(N'2022-06-15 10:10:00' AS SmallDateTime), 9, NULL, 1)
INSERT [dbo].[Songs] ([song_ID], [name], [author], [duration], [image], [path], [t_create], [t_lastUpdate], [category_ID], [like], [views]) VALUES (101, N'Chieu Mot Minh Qua Pho', N'Vo Ta Han', N'02:40', N'./img/song-img/guitar/Chieu-Mot-Minh-Qua-Pho-Vo-Ta-Han.jpg', N'./song-mp3/guitar/Chieu-Mot-Minh-Qua-Pho-Vo-Ta-Han.mp3', CAST(N'2022-06-15 09:01:00' AS SmallDateTime), CAST(N'2022-06-15 10:10:00' AS SmallDateTime), 9, NULL, 1)
INSERT [dbo].[Songs] ([song_ID], [name], [author], [duration], [image], [path], [t_create], [t_lastUpdate], [category_ID], [like], [views]) VALUES (102, N'Con Tuoi Nao Cho Em', N'Various-Artists', N'04:33', N'./img/song-img/guitar/Con-Tuoi-Nao-Cho-Em-Various-Artists.jpg', N'./song-mp3/guitar/Con-Tuoi-Nao-Cho-Em-Various-Artists.mp3', CAST(N'2022-06-15 09:01:00' AS SmallDateTime), CAST(N'2022-06-15 10:10:00' AS SmallDateTime), 9, NULL, 1)
INSERT [dbo].[Songs] ([song_ID], [name], [author], [duration], [image], [path], [t_create], [t_lastUpdate], [category_ID], [like], [views]) VALUES (103, N'Diem Xua', N'Kim Tuan', N'04:28', N'./img/song-img/guitar/Diem-Xua-Kim-Tuan.jpg', N'./song-mp3/guitar/Diem-Xua-Kim-Tuan.mp3', CAST(N'2022-06-15 09:01:00' AS SmallDateTime), CAST(N'2022-06-15 10:10:00' AS SmallDateTime), 9, NULL, 1)
INSERT [dbo].[Songs] ([song_ID], [name], [author], [duration], [image], [path], [t_create], [t_lastUpdate], [category_ID], [like], [views]) VALUES (104, N'Em Con Nho Hay Em Da Quen', N'Tran Hoai Phuong', N'07:36', N'./img/song-img/guitar/Em-Con-Nho-Hay-Em-Da-Quen-Tran-Hoai-Phuong.jpg', N'./song-mp3/guitar/Em-Con-Nho-Hay-Em-Da-Quen-Tran-Hoai-Phuong.mp3', CAST(N'2022-06-15 09:01:00' AS SmallDateTime), CAST(N'2022-06-15 10:10:00' AS SmallDateTime), 9, NULL, 1)
INSERT [dbo].[Songs] ([song_ID], [name], [author], [duration], [image], [path], [t_create], [t_lastUpdate], [category_ID], [like], [views]) VALUES (105, N'Em Di Trong Chieu', N'Tran Hoai Phuong', N'06:20', N'./img/song-img/guitar/Em-Di-Trong-Chieu-Tran-Hoai-Phuong.jpg', N'./song-mp3/guitar/Em-Di-Trong-Chieu-Tran-Hoai-Phuong.mp3', CAST(N'2022-06-15 09:01:00' AS SmallDateTime), CAST(N'2022-06-15 10:10:00' AS SmallDateTime), 9, NULL, 1)
INSERT [dbo].[Songs] ([song_ID], [name], [author], [duration], [image], [path], [t_create], [t_lastUpdate], [category_ID], [like], [views]) VALUES (106, N'Ha Trang ', N'Do Dinh Phuong', N'03:25', N'./img/song-img/guitar/Ha-Trang-Do-Dinh-Phuong.jpg', N'./song-mp3/guitar/Ha-Trang-Do-Dinh-Phuong.mp3', CAST(N'2022-06-15 09:01:00' AS SmallDateTime), CAST(N'2022-06-15 10:10:00' AS SmallDateTime), 9, NULL, 1)
INSERT [dbo].[Songs] ([song_ID], [name], [author], [duration], [image], [path], [t_create], [t_lastUpdate], [category_ID], [like], [views]) VALUES (107, N'Hoa Vang May Do ', N'Do Dinh Phuong', N'03:35', N'./img/song-img/guitar/Hoa-Vang-May-Do-Do-Dinh-Phuong.jpg', N'./song-mp3/guitar/Hoa-Vang-May-Do-Do-Dinh-Phuong.mp3', CAST(N'2022-06-15 09:01:00' AS SmallDateTime), CAST(N'2022-06-15 10:10:00' AS SmallDateTime), 9, NULL, 1)
INSERT [dbo].[Songs] ([song_ID], [name], [author], [duration], [image], [path], [t_create], [t_lastUpdate], [category_ID], [like], [views]) VALUES (108, N'Ru Em Tung Ngon Xuan Nong', N'Vo Ta Han', N'02:03', N'./img/song-img/guitar/Ru-Em-Tung-Ngon-Xuan-Nong-Vo-Ta-Han.jpg', N'./song-mp3/guitar/Ru-Em-Tung-Ngon-Xuan-Nong-Vo-Ta-Han.mp3', CAST(N'2022-06-15 09:01:00' AS SmallDateTime), CAST(N'2022-06-15 10:10:00' AS SmallDateTime), 9, NULL, 1)
INSERT [dbo].[Songs] ([song_ID], [name], [author], [duration], [image], [path], [t_create], [t_lastUpdate], [category_ID], [like], [views]) VALUES (109, N'Lupin', N'KARA', N'03:12', N'./img/song-img/kpop/Lupin-KARA.jpg', N'./song-mp3/kpop/Lupin-KARA.mp3', CAST(N'2022-06-15 09:01:00' AS SmallDateTime), CAST(N'2022-06-15 10:10:00' AS SmallDateTime), 5, NULL, 1)
SET IDENTITY_INSERT [dbo].[Songs] OFF
SET IDENTITY_INSERT [dbo].[Users] ON 

INSERT [dbo].[Users] ([user_ID], [username], [password], [first_name], [last_name], [avatar], [t_create], [t_lastOnline], [email], [status], [address], [gender], [DOB], [role_ID], [randomVerify], [report]) VALUES (1, N'admin', N'123', N'Admin', N'Manager', NULL, CAST(N'2022-05-17 10:32:00' AS SmallDateTime), CAST(N'2022-07-24 15:12:00' AS SmallDateTime), N'admin@gmail.com', 1, N'Admin''s Home', N'Female', CAST(N'2002-04-11 00:00:00.000' AS DateTime), 1, N'fwf2hsuefq', 0)
INSERT [dbo].[Users] ([user_ID], [username], [password], [first_name], [last_name], [avatar], [t_create], [t_lastOnline], [email], [status], [address], [gender], [DOB], [role_ID], [randomVerify], [report]) VALUES (2, N'huanviptq', N'huanzip46', N'', N'', NULL, CAST(N'2022-07-24 15:09:00' AS SmallDateTime), CAST(N'2022-07-24 15:25:00' AS SmallDateTime), N'huanviptq46@gmail.com', 1, N'Home', N'Male', CAST(N'2002-04-11 00:00:00.000' AS DateTime), 3, N'00ZZto9h', 0)
SET IDENTITY_INSERT [dbo].[Users] OFF
ALTER TABLE [dbo].[Liked] ADD  CONSTRAINT [DF_Liked_t_lastUpdate]  DEFAULT (getdate()) FOR [t_lastUpdate]
GO
ALTER TABLE [dbo].[PlayLists] ADD  CONSTRAINT [DF_PlayLists_t_create]  DEFAULT (getdate()) FOR [t_create]
GO
ALTER TABLE [dbo].[ReplyComment] ADD  CONSTRAINT [DF_ReplyComment_t_create]  DEFAULT (getdate()) FOR [t_create]
GO
ALTER TABLE [dbo].[ReplyComment] ADD  CONSTRAINT [DF_ReplyComment_t_lastUpdate]  DEFAULT (getdate()) FOR [t_lastUpdate]
GO
ALTER TABLE [dbo].[Albums]  WITH CHECK ADD FOREIGN KEY([user_ID])
REFERENCES [dbo].[Users] ([user_ID])
GO
ALTER TABLE [dbo].[Albums_Songs]  WITH CHECK ADD FOREIGN KEY([album_ID])
REFERENCES [dbo].[Albums] ([album_ID])
GO
ALTER TABLE [dbo].[Albums_Songs]  WITH CHECK ADD FOREIGN KEY([song_ID])
REFERENCES [dbo].[Songs] ([song_ID])
GO
ALTER TABLE [dbo].[Categories]  WITH CHECK ADD FOREIGN KEY([categorydetail_ID])
REFERENCES [dbo].[CategoryDetails] ([categorydetail_ID])
GO
ALTER TABLE [dbo].[Comments]  WITH CHECK ADD FOREIGN KEY([song_ID])
REFERENCES [dbo].[Songs] ([song_ID])
GO
ALTER TABLE [dbo].[Comments]  WITH CHECK ADD FOREIGN KEY([user_ID])
REFERENCES [dbo].[Users] ([user_ID])
GO
ALTER TABLE [dbo].[HistoryView]  WITH CHECK ADD FOREIGN KEY([song_ID])
REFERENCES [dbo].[Songs] ([song_ID])
GO
ALTER TABLE [dbo].[HistoryView]  WITH CHECK ADD FOREIGN KEY([user_ID])
REFERENCES [dbo].[Users] ([user_ID])
GO
ALTER TABLE [dbo].[Liked]  WITH CHECK ADD FOREIGN KEY([comment_ID])
REFERENCES [dbo].[Comments] ([comment_ID])
GO
ALTER TABLE [dbo].[Liked]  WITH CHECK ADD FOREIGN KEY([song_ID])
REFERENCES [dbo].[Songs] ([song_ID])
GO
ALTER TABLE [dbo].[Liked]  WITH CHECK ADD FOREIGN KEY([user_ID])
REFERENCES [dbo].[Users] ([user_ID])
GO
ALTER TABLE [dbo].[Lyrics]  WITH CHECK ADD FOREIGN KEY([song_ID])
REFERENCES [dbo].[Songs] ([song_ID])
GO
ALTER TABLE [dbo].[PlayLists]  WITH CHECK ADD FOREIGN KEY([user_ID])
REFERENCES [dbo].[Users] ([user_ID])
GO
ALTER TABLE [dbo].[PlayLists_Songs]  WITH CHECK ADD FOREIGN KEY([playlist_ID])
REFERENCES [dbo].[PlayLists] ([playlist_ID])
GO
ALTER TABLE [dbo].[PlayLists_Songs]  WITH CHECK ADD FOREIGN KEY([song_ID])
REFERENCES [dbo].[Songs] ([song_ID])
GO
ALTER TABLE [dbo].[ReplyComment]  WITH CHECK ADD FOREIGN KEY([comment_ID])
REFERENCES [dbo].[Comments] ([comment_ID])
GO
ALTER TABLE [dbo].[ReplyComment]  WITH CHECK ADD FOREIGN KEY([song_ID])
REFERENCES [dbo].[Songs] ([song_ID])
GO
ALTER TABLE [dbo].[ReplyComment]  WITH CHECK ADD FOREIGN KEY([user_ID])
REFERENCES [dbo].[Users] ([user_ID])
GO
ALTER TABLE [dbo].[Songs]  WITH CHECK ADD FOREIGN KEY([category_ID])
REFERENCES [dbo].[Categories] ([category_ID])
GO
ALTER TABLE [dbo].[Users]  WITH CHECK ADD FOREIGN KEY([role_ID])
REFERENCES [dbo].[Roles] ([role_ID])
GO
USE [master]
GO
ALTER DATABASE [Online_Music] SET  READ_WRITE 
GO
