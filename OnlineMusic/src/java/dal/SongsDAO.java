/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import model.Categories;
import model.CategoryDetails;
import model.Songs;

/**
 *
 * @author Black
 */
public class SongsDAO extends BaseDAO<Songs> {

    @Override
    public ArrayList<Songs> getAll() {
        ArrayList<Songs> songs = new ArrayList<>();
        try {
            String sql = "SELECT * FROM Songs";
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Songs s = new Songs();
                s.setSong_ID(rs.getInt(1));
                s.setName(rs.getString(2));
                s.setAuthor(rs.getString(3));
                s.setDuration(rs.getString(4));
                s.setImage(rs.getString(5));
                s.setPath(rs.getString(6));
                s.setT_create(rs.getTimestamp(7));
                s.setT_lastUpdate(rs.getTimestamp(8));
                s.setCategory_ID(rs.getInt(9));

                songs.add(s);
            }
        } catch (SQLException ex) {

        }
        return songs;
    }

    public ArrayList<Categories> getAllCategories() {
        ArrayList<Categories> categories = new ArrayList<>();
        try {
            String sql = "SELECT * FROM Categories";
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Categories s = new Categories();
                s.setCategoryID(rs.getInt(1));
                s.setCategoryName(rs.getString(2));

                categories.add(s);
            }
        } catch (SQLException ex) {

        }
        return categories;
    }

    public ArrayList<CategoryDetails> getAllCategoryDetails() {
        ArrayList<CategoryDetails> categoryDetails = new ArrayList<>();
        try {
            String sql = "select b.category_ID,b.categorydetail_ID,b.name from CategoryDetails a inner join Categories b on a.categorydetail_ID = b.categorydetail_ID";

            PreparedStatement st = connection.prepareStatement(sql);

            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                CategoryDetails s = new CategoryDetails();
                s.setCategoryDetail_name(rs.getString(3));
                s.setCategoryID(rs.getInt(1));
                categoryDetails.add(s);
            }
        } catch (SQLException ex) {

        }
        return categoryDetails;
    }

    public int insertSongs(String name, String author, String duration, String image,
            String timeCreate, String path, int categoryID) {
        try {
            if (categoryID != 0) {
                String sql = "INSERT into Songs(name, author, duration, image, path, t_create, category_ID)\n"
                        + "VALUES(?,?,?,?,?,?,?)";
                PreparedStatement st = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
                st.setString(1, name);
                st.setString(2, author);
                st.setString(3, duration);
                st.setString(4, image);
                st.setString(5, path);
                st.setString(6, timeCreate);
                st.setInt(7, categoryID);
                st.executeUpdate();
                ResultSet rs = st.getGeneratedKeys();
                if (rs.next()) {
                    return rs.getInt(1);
                }
            } else {
                String sql = "INSERT into Songs(name, author, duration, image, path, t_create)\n"
                        + "VALUES(?,?,?,?,?,?)";
                PreparedStatement st = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
                st.setString(1, name);
                st.setString(2, author);
                st.setString(3, duration);
                st.setString(4, image);
                st.setString(5, path);
                st.setString(6, timeCreate);
                st.setInt(7, categoryID);
                st.executeUpdate();
                ResultSet rs = st.getGeneratedKeys();
                if (rs.next()) {
                    return rs.getInt(1);
                }
            }
        } catch (Exception e) {
        }
        return 0;
    }

    public void updateSongs(int songID, String name, String author, String duration, String image,
            String timeUpdate, String path, int categoryID) {
        try {

            if (categoryID != 0) {
                String sql = "UPDATE Songs\n"
                        + "SET name=?,author=?,duration=?,[image]=?,[path]=?,t_lastUpdate=?,category_ID=?\n"
                        + "WHERE song_ID=?";
                PreparedStatement st = connection.prepareStatement(sql);
                st.setString(1, name);
                st.setString(2, author);
                st.setString(3, duration);
                st.setString(4, image);
                st.setString(5, path);
                st.setString(6, timeUpdate);
                st.setInt(7, categoryID);
                st.setInt(8, songID);
                st.executeUpdate();
            } else {
                String sql = "UPDATE Songs\n"
                        + "SET name=?,author=?,duration=?,[image]=?,[path]=?,t_lastUpdate=?\n"
                        + "WHERE song_ID=?";
                PreparedStatement st = connection.prepareStatement(sql);
                st.setString(1, name);
                st.setString(2, author);
                st.setString(3, duration);
                st.setString(4, image);
                st.setString(5, path);
                st.setString(6, timeUpdate);
                st.setInt(7, songID);
                st.executeUpdate();
            }
        } catch (Exception e) {
        }
    }

    public void deleteSongs(int songID) {
        try {
            String sql = "delete from Comments where song_ID = ?";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, songID);
            st.executeUpdate();

            String sql1 = "delete from Albums_Songs where song_ID= ?";
            PreparedStatement st1 = connection.prepareStatement(sql1);
            st1.setInt(1, songID);
            st1.executeUpdate();

            String sql2 = "delete from PlayLists_Songs where song_ID = ?";
            PreparedStatement st2 = connection.prepareStatement(sql2);
            st2.setInt(1, songID);
            st2.executeUpdate();

            String sql3 = "delete from Liked where song_ID = ?";
            PreparedStatement st3 = connection.prepareStatement(sql3);
            st3.setInt(1, songID);
            st3.executeUpdate();

            String sql4 = "delete from History where song_ID = ?";
            PreparedStatement st4 = connection.prepareStatement(sql4);
            st4.setInt(1, songID);
            st4.executeUpdate();
            
             String sql5 = "delete from Lyrics where song_ID = ?";
            PreparedStatement st5 = connection.prepareStatement(sql5);
            st5.setInt(1, songID);
            st5.executeUpdate();

            String sql6 = "DELETE FROM Songs WHERE song_ID = ?";
            PreparedStatement st6 = connection.prepareStatement(sql6);
            st6.setInt(1, songID);
            st6.executeUpdate();

        } catch (Exception e) {
        }
    }

    public List<Songs> searchByLyric(String lyric) {
        List<Songs> list = new ArrayList();
        String query = "select distinct a.song_ID, a.name, a.author, a.duration, a.image, a.path, a.t_create, a.t_lastUpdate, a.category_ID\n"
                + "  from Songs a inner join Lyrics b on a.song_ID = b.song_ID  where  [content] like  N'%" + lyric+ "%'";
        try {
            PreparedStatement st = connection.prepareStatement(query);
            ResultSet rs = st.executeQuery();
            try {
                while (rs.next()) {
                    Songs s = new Songs();
                    s.setSong_ID(rs.getInt(1));
                    s.setName(rs.getString(2));
                    s.setAuthor(rs.getString(3));
                    s.setDuration(rs.getString(4));
                    s.setImage(rs.getString(5));
                    s.setPath(rs.getString(6));
                    s.setT_create(rs.getTimestamp(7));
                    s.setT_lastUpdate(rs.getTimestamp(8));
                    s.setCategory_ID(rs.getInt(9));
                    list.add(s);
                }
            } catch (SQLException e) {
            }
        } catch (SQLException e) {
        }
        return list;
    }

    public List<Songs> searchByAuthorAndName(String authName) {
        List<Songs> list = new ArrayList();
        String query = "select song_ID, name, author, duration, image, path, t_create, t_lastUpdate, category_ID\n"
                        +" from Songs where author like N'%" + authName + "%' or name like N'%" + authName + "%' ";
        try {
            PreparedStatement st = connection.prepareStatement(query);
            ResultSet rs = st.executeQuery();
            try {
                while (rs.next()) {
                    Songs s = new Songs();
                    s.setSong_ID(rs.getInt("song_ID"));
                    s.setName(rs.getString("name"));
                    s.setAuthor(rs.getString("author"));
                    s.setDuration(rs.getString("duration"));
                    s.setImage(rs.getString("image"));
                    s.setPath(rs.getString("path"));
                    s.setT_create(rs.getTimestamp("t_create"));
                    s.setT_lastUpdate(rs.getTimestamp("t_lastUpdate"));
                    s.setCategory_ID(rs.getInt("category_ID"));

                    list.add(s);
                }
            } catch (SQLException e) {
            }
        } catch (SQLException e) {
        }
        return list;
    }

//    public List<Songs> searchByName(String name) {
//        List<Songs> list = new ArrayList();
//        String query = "select song_ID, name, author, duration, image, path, t_create, t_lastUpdate, category_ID\n"
//                        +"from Songs where name like N'%" + name + "%' ";
//        try {
//            PreparedStatement st = connection.prepareStatement(query);
//            ResultSet rs = st.executeQuery();
//            try {
//                while (rs.next()) {
//                    Songs s = new Songs();
//                    s.setSong_ID(rs.getInt(1));
//                    s.setName(rs.getString(2));
//                    s.setAuthor(rs.getString(3));
//                    s.setDuration(rs.getString(4));
//                    s.setImage(rs.getString(5));
//                    s.setPath(rs.getString(6));
//                    s.setT_create(rs.getTimestamp(7));
//                    s.setT_lastUpdate(rs.getTimestamp(8));
//                    s.setCategory_ID(rs.getInt(9));
//
//                    list.add(s);
//                }
//            } catch (SQLException e) {
//            }
//        } catch (SQLException e) {
//        }
//        return list;
//    }

    public Songs getSongsByID(int id) {
        try {
            String sql = "select * \n"
                    + " from songs\n"
                    + " where  song_ID = ?";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Songs s = new Songs();
                s.setSong_ID(rs.getInt(1));
                s.setName(rs.getString(2));
                s.setAuthor(rs.getString(3));
                s.setDuration(rs.getString(4));
                s.setImage(rs.getString(5));
                s.setPath(rs.getString(6));
                s.setT_create(rs.getTimestamp(7));
                s.setT_lastUpdate(rs.getTimestamp(8));
                s.setCategory_ID(rs.getInt(9));
                s.setViews(rs.getInt("views"));
                return s;
            }
        } catch (SQLException ex) {
            System.out.println("abc");
        }
        return null;
    }

    public ArrayList<Songs> getSongsOfCategory(int category_ID) {
        ArrayList<Songs> songs = new ArrayList<>();
        try {
            String sql = "SELECT song_ID, s.name, author, duration, image, path, t_create, t_lastUpdate, s.category_ID \n"
                    + "FROM Songs s inner join Categories a on s.category_ID = a.category_ID\n"
                    + "WHERE a.categorydetail_ID = ?";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, category_ID);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Songs s = new Songs();
                s.setSong_ID(rs.getInt("song_ID"));
                s.setName(rs.getString("name"));
                s.setAuthor(rs.getString("author"));
                s.setDuration(rs.getString("duration"));
                s.setImage(rs.getString("image"));
                s.setPath(rs.getString("path"));
                s.setT_create(rs.getTimestamp("t_create"));
                s.setT_lastUpdate(rs.getTimestamp("t_lastUpdate"));
                s.setCategory_ID(rs.getInt("category_ID"));
                songs.add(s);
            }
        } catch (SQLException ex) {

        }
        return songs;
    }

    public ArrayList<Songs> getSongsOfCate(int cateID) {
        ArrayList<Songs> songs = new ArrayList<>();
        try {
            String sql = "SELECT b.song_ID, b.name, b.author, b.duration, b.image, b.path, b.t_create, b.t_lastUpdate, b.category_ID \n"
                    + "from Categories a inner join Songs b on a.category_ID = b.category_ID\n"
                    + "where a.category_ID = ?";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, cateID);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Songs s = new Songs();
                s.setSong_ID(rs.getInt("song_ID"));
                s.setName(rs.getString("name"));
                s.setAuthor(rs.getString("author"));
                s.setDuration(rs.getString("duration"));
                s.setImage(rs.getString("image"));
                s.setPath(rs.getString("path"));
                s.setT_create(rs.getTimestamp("t_create"));
                s.setT_lastUpdate(rs.getTimestamp("t_lastUpdate"));
                s.setCategory_ID(rs.getInt("category_ID"));
                songs.add(s);
            }
        } catch (SQLException ex) {

        }
        return songs;
    }

    public ArrayList<Songs> getSongsByUser(int userID) {
        ArrayList<Songs> songs = new ArrayList<>();
        try {
            String sql = "SELECT s.song_ID, s.name, s.author, s.duration, s.image, s.path, s.t_create, s.t_lastUpdate, s.category_ID FROM Songs s JOIN History h\n"
                    + "ON s.song_ID = h.song_ID \n"
                    + "JOIN Users u ON u.user_ID = h.user_ID\n"
                    + "WHERE h.user_ID = ?";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, userID);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Songs s = new Songs();
                s.setSong_ID(rs.getInt("song_ID"));
                s.setName(rs.getString("name"));
                s.setAuthor(rs.getString("author"));
                s.setDuration(rs.getString("duration"));
                s.setImage(rs.getString("image"));
                s.setPath(rs.getString("path"));
                s.setT_create(rs.getTimestamp("t_create"));
                s.setT_lastUpdate(rs.getTimestamp("t_lastUpdate"));
                s.setCategory_ID(rs.getInt("category_ID"));
                songs.add(s);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return songs;
    }

    public void insertHistory(String songName, String timeUpdate, int userID, int songID) {
        try {
            String sql = "INSERT INTO History(song_name, t_lastUpdate, user_ID, song_ID) VALUES(?,?,?,?)";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, songName);
            st.setString(2, timeUpdate);
            st.setInt(3, userID);
            st.setInt(4, songID);
            st.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public ArrayList<Songs> getAllSongbyAlbumId(int albumid) {
        ArrayList<Songs> songs = new ArrayList<>();
        try {
            String sql = "select * from Songs\n"
                    + "where song_ID not in (\n"
                    + "select a.song_ID from Songs a \n"
                    + "inner join Albums_Songs s on \n"
                    + "a.song_ID = s.song_ID \n"
                    + "where s.album_ID = ? ) ";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, albumid);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Songs s = new Songs();
                s.setSong_ID(rs.getInt(1));
                s.setName(rs.getString(2));
                s.setAuthor(rs.getString(3));
                s.setDuration(rs.getString(4));
                s.setImage(rs.getString(5));
                s.setPath(rs.getString(6));
                s.setT_create(rs.getTimestamp(7));
                s.setT_lastUpdate(rs.getTimestamp(8));
                s.setCategory_ID(rs.getInt(9));

                songs.add(s);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return songs;
    }
    
    public ArrayList<Categories> getCategoryName(){
        ArrayList<Categories> catename = new ArrayList<>();
        try {
            String sql = "select distinct c.category_ID, c.name from Categories c right join Songs s on c.category_ID=s.category_ID";
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Categories c = new Categories();
                c.setCategoryID(rs.getInt("category_ID"));
                c.setCategoryName(rs.getString("name"));
                catename.add(c);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return catename;
    }

    public ArrayList<Songs> getNewSong() {
        ArrayList<Songs> songs = new ArrayList<>();
        try {
            String sql = "SELECT top 5 s.song_ID, s.name, s.author, s.duration, s.[image], s.[path], s.t_create, s.t_lastUpdate, s.category_ID FROM Songs s "
                    + "WHERE s.t_create >= DATEADD(day, -30, getdate())";
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Songs s = new Songs();
                s.setSong_ID(rs.getInt("song_ID"));
                s.setName(rs.getString("name"));
                s.setAuthor(rs.getString("author"));
                s.setDuration(rs.getString("duration"));
                s.setImage(rs.getString("image"));
                s.setPath(rs.getString("path"));
                s.setT_create(rs.getTimestamp("t_create"));
                s.setT_lastUpdate(rs.getTimestamp("t_lastUpdate"));
                s.setCategory_ID(rs.getInt("category_ID"));
                songs.add(s);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return songs;
    }

    public ArrayList<Songs> getSongHot() {
        ArrayList<Songs> songs = new ArrayList<>();
        try {
            String sql = "select top 5 [views], song_ID, name, author, image, t_create, category_ID from Songs  order by [views] desc ";
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Songs s = new Songs();
                s.setSong_ID(rs.getInt("song_ID"));
                s.setName(rs.getString("name"));
                s.setAuthor(rs.getString("author"));
                s.setImage(rs.getString("image"));
                s.setT_create(rs.getTimestamp("t_create"));
                s.setCategory_ID(rs.getInt("category_ID"));
                songs.add(s);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return songs;
    }
    
    public ArrayList<Songs> getAllNewSong() {
        ArrayList<Songs> songs = new ArrayList<>();
        try {
            String sql = "SELECT s.song_ID, s.name, s.author, s.duration, s.[image], s.[path], s.t_create, s.t_lastUpdate, s.category_ID FROM Songs s  "
                    + "WHERE s.t_create >= DATEADD(day, -30, getdate())";
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Songs s = new Songs();
                s.setSong_ID(rs.getInt("song_ID"));
                s.setName(rs.getString("name"));
                s.setAuthor(rs.getString("author"));
                s.setDuration(rs.getString("duration"));
                s.setImage(rs.getString("image"));
                s.setPath(rs.getString("path"));
                s.setT_create(rs.getTimestamp("t_create"));
                s.setT_lastUpdate(rs.getTimestamp("t_lastUpdate"));
                s.setCategory_ID(rs.getInt("category_ID"));
                songs.add(s);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return songs;
    }

    public ArrayList<Songs> getAllSongHot() {
        ArrayList<Songs> songs = new ArrayList<>();
        try {
            String sql = "select [views], song_ID, name, author, image, t_create, category_ID from Songs  order by [views] desc  ";
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Songs s = new Songs();
                s.setSong_ID(rs.getInt("song_ID"));
                s.setName(rs.getString("name"));
                s.setAuthor(rs.getString("author"));
                s.setImage(rs.getString("image"));
                s.setT_create(rs.getTimestamp("t_create"));
                s.setCategory_ID(rs.getInt("category_ID"));
                songs.add(s);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return songs;
    }

    public void addViewsBySongID(int song_ID){
        String sql = "update Songs set views = views + 1 where song_ID = ?";
        try{
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, song_ID);
            st.executeUpdate();
        }catch(SQLException e){
            System.out.println(e);
        }
    }

    public static void main(String[] args) {
        SongsDAO sd = new SongsDAO();
        System.out.println(sd.getCategoryName());

    }
}
