/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import model.Like;

/**
 *
 * @author Auriat
 */
public class LikeDAO extends BaseDAO<Like> {

    public ArrayList<Like> getCheckLikeComment(int userid) {
        ArrayList<Like> like = new ArrayList<>();
        try {
            String sql = "Select comment_ID\n"
                    + "From Liked\n"
                    + "where user_ID = ? and comment_ID is not null";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, userid);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Like s = new Like();
                s.setComment_ID(rs.getInt(1));
                like.add(s);
            }
        } catch (SQLException ex) {

        }
        return like;
    }
    
    public ArrayList<Like> getCheckLikeSong(int userid) {
        ArrayList<Like> like = new ArrayList<>();
        try {
            String sql = "Select song_ID\n"
                    + "From Liked\n"
                    + "where user_ID = ? and flag = 0";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, userid);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Like s = new Like();
                s.setSong_ID(rs.getInt(1));
                like.add(s);
            }
        } catch (SQLException ex) {

        }
        return like;
    }

    public ArrayList<Like> getSongByLike() {
        ArrayList<Like> song = new ArrayList<Like>();
        try {
            String sql = "Select top 5 COUNT(Liked.[like]) as [like], Songs.name, Songs.image, Songs.author, Liked.song_ID, Songs.category_ID, Songs.t_create\n"
                    + "From Liked \n"
                    + "Join Songs on Songs.song_ID = Liked.song_ID\n"
                    + "Where flag = 0\n"
                    + "group by Liked.song_ID, Songs.name, Songs.image, Songs.author, Liked.song_ID, Songs.category_ID, Songs.t_create";
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Like s = new Like();
                s.setTotalLike(rs.getInt(1));
                s.setName(rs.getString(2));
                s.setImage(rs.getString(3));
                s.setAuthor(rs.getString(4));
                s.setSong_ID(rs.getInt(5));
                s.setCategory_ID(rs.getInt(6));
                s.setT_create(rs.getTimestamp(7));
                song.add(s);
            }
        } catch (SQLException ex) {

        }
        return song;
    }
    
    public ArrayList<Like> getAllSongByLike() {
        ArrayList<Like> song = new ArrayList<Like>();
        try {
            String sql = "Select COUNT(Liked.[like]) as [like], Songs.name, Songs.image, Songs.author, Liked.song_ID, Songs.category_ID, Songs.t_create \n"
                    + "From Liked \n"
                    + "Join Songs on Songs.song_ID = Liked.song_ID\n"
                    + "Where flag = 0\n"
                    + "group by Liked.song_ID, Songs.name, Songs.image, Songs.author, Liked.song_ID, Songs.category_ID, Songs.t_create";
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Like s = new Like();
                s.setTotalLike(rs.getInt(1));
                s.setName(rs.getString(2));
                s.setImage(rs.getString(3));
                s.setAuthor(rs.getString(4));
                s.setSong_ID(rs.getInt(5));
                s.setCategory_ID(rs.getInt(6));
                s.setT_create(rs.getTimestamp(7));
                song.add(s);
            }
        } catch (SQLException ex) {

        }
        return song;
    }

    public void insertLike(int userID, int songID, boolean like) {
        try {
            String sql = "INSERT INTO [dbo].[Liked]\n"
                    + "           ([user_ID]\n"
                    + "           ,[song_ID]\n"
                    + "           ,[flag]\n"
                    + "           ,[like])\n"
                    + "     VALUES(?,?,0,?)";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, userID);
            st.setInt(2, songID);
            st.setBoolean(3, like);
            st.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public void updateLikeComment(int userID, int commentID, int songID, boolean like) {
        try {
            String sql = "INSERT INTO [dbo].[Liked]\n"
                    + "           ([user_ID]\n"
                    + "           ,[song_ID]\n"
                    + "           ,[like]\n"
                    + "           ,[flag]\n"
                    + "           ,[comment_ID])\n"
                    + "     VALUES(?,?,?,1,?)";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, userID);
            st.setInt(2, songID);
            st.setBoolean(3, like);
            st.setInt(4, commentID);

            st.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public void cancelLike(int userID, int songID) {
        try {
            String sql = "DELETE FROM Liked\n"
                    + " WHERE user_ID = ? and song_ID = ?";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, userID);
            st.setInt(2, songID);
            st.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public boolean duplicateLike(int userID, int songID) {
        try {
            String sql = "SELECT [like] FROM Liked \n"
                    + " WHERE user_ID = ? and song_ID = ? and flag = 0";
            PreparedStatement st = connection.prepareStatement(sql);

            st.setInt(1, userID);
            st.setInt(2, songID);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getBoolean(1);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return false;
    }

    public void cancelLikeComment(int userID, int songID, int commentID) {
        try {
            String sql = "DELETE FROM Liked\n"
                    + " WHERE user_ID = ? and song_ID = ? and comment_ID = ?";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, userID);
            st.setInt(2, songID);
            st.setInt(3, commentID);
            st.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public ArrayList<Like> duplicateLikeComment(int userID, int songID) {
        ArrayList<Like> listCommentID = new ArrayList<>();
        try {
            String sql = "Select Liked.comment_ID\n"
                    + "From Liked\n"
                    + "Where user_ID = ? and song_ID = ? and flag = 1";
            PreparedStatement st = connection.prepareStatement(sql);

            st.setInt(1, userID);
            st.setInt(2, songID);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Like l = new Like();
                l.setComment_ID(rs.getInt(1));
                listCommentID.add(l);
                return listCommentID;
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public ArrayList<Like> getAll() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
