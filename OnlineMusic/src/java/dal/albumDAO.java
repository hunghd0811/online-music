/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.Albums;
import model.Categories;
import model.CategoryDetails;
import model.Songs;
import model.Users;


/**
 *
 * @author anhqu
 */
public class albumDAO extends BaseDAO<Albums>{

    @Override
    public ArrayList<Albums> getAll() {
            ArrayList<Albums> list = new ArrayList<Albums>();
            try {
            String sql = "select album_ID,image,user_ID,name,t_create from Albums";               
                PreparedStatement st = connection.prepareStatement(sql);               
                ResultSet rs = st.executeQuery();
                while (rs.next()) {                    
                    Albums a = new Albums();
                    a.setAlbums_ID(rs.getInt(1));
                    a.setImage(rs.getString(2));
                    a.setUserid(rs.getInt(3));
                    a.setName(rs.getString(4));
                    a.setT_create(rs.getTimestamp(5));                   
                    list.add(a);
                }
        } catch (Exception e) {
        }
            return list;    
    }
   
  
      
     
      public void deleteAlbum(int albumid){
          try {
              String sql = "delete from Albums_Songs where album_ID = ?";
              PreparedStatement st = connection.prepareStatement(sql);
              st.setInt(1, albumid);
              st.executeUpdate();
              String sql1 = "delete from Albums where album_ID = ?";
              PreparedStatement st1 = connection.prepareStatement(sql1);
              st1.setInt(1, albumid);
              st1.executeUpdate();
          } catch (Exception e) {
          }
      }
      public void deleteSongAlbum(int albumid, int songid){
          try {
              String sql = "delete from Albums_Songs where song_ID = ? and album_ID = ?";
              PreparedStatement st = connection.prepareStatement(sql);
              st.setInt(1, albumid);
              st.setInt(2, songid);
              st.executeUpdate();             
          } catch (Exception e) {
          }
      }
      
      public List<Songs> getAllMusic(int albumid){
          List<Songs> list = new ArrayList<>();
          try {
              String sql = "select s.song_ID,s.name,s.author,s.duration,s.image,s.path,s.t_create,s.t_lastUpdate,s.category_ID,a.album_ID from Albums_Songs a inner join Songs s on a.song_ID = s.song_ID where a.album_ID = ?";
              PreparedStatement st = connection.prepareStatement(sql);
              st.setInt(1, albumid);
              ResultSet rs = st.executeQuery();
              while (rs.next()) {                  
                 Songs s = new Songs();
                    s.setSong_ID(rs.getInt(1));
                    s.setName(rs.getString(2));
                    s.setAuthor(rs.getString(3));
                    s.setDuration(rs.getString(4));
                    s.setImage(rs.getString(5));
                    s.setPath(rs.getString(6));
                    s.setT_create(rs.getTimestamp(7));
                    s.setT_lastUpdate(rs.getTimestamp(8));
                    s.setCategory_ID(rs.getInt(9));
                    s.setAlbum_ID(rs.getInt(10));
                    list.add(s); 
              }
          } catch (Exception e) {
          }
          return list;
      }
      public ArrayList<CategoryDetails> getAllCategoryDetails() {
        ArrayList<CategoryDetails> categoryDetails = new ArrayList<>();
        try {
        String sql="select b.category_ID,b.categorydetail_ID,b.name from CategoryDetails a inner join Categories b on a.categorydetail_ID = b.categorydetail_ID";

          PreparedStatement st = connection.prepareStatement(sql);
          
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                CategoryDetails s = new CategoryDetails();
                s.setCategoryDetail_name(rs.getString(3));
                s.setCategoryID(rs.getInt(1));
                categoryDetails.add(s);
            }
        } catch (SQLException ex) {

        }
        return categoryDetails;
    } 
    
       
       
    public ArrayList<Albums> getAlbumsByID() {
        ArrayList<Albums> album = new ArrayList<>();
        try {
            String sql = "select album_ID, image,description, name, t_create from Albums ";
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Albums s = new Albums();
                s.setAlbums_ID(rs.getInt("album_ID"));
                s.setImage(rs.getString("image"));
                s.setDescription(rs.getString("description"));
                s.setName(rs.getString("name"));
                s.setT_create(rs.getTimestamp("t_create"));
                album.add(s);
            }
        } catch (SQLException ex) {

        }
        return album;
    }
    
    public ArrayList<Songs> getSongsOfAlbums(int album_ID) {
        ArrayList<Songs> songs = new ArrayList<>();
        try {
            String sql = "select s.song_ID, s.name, s.author, s.duration, s.[image], s.[path], s.t_create, s.t_lastUpdate \n"
                    +" from Albums_Songs als inner join Albums a on als.album_ID = a.album_ID\n" 
                    +" inner join Songs s on als.song_ID = s.song_ID\n"
                    +" WHERE als.album_ID = ?";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, album_ID);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Songs s = new Songs();
                s.setSong_ID(rs.getInt("song_ID"));
                s.setName(rs.getString("name"));
                s.setAuthor(rs.getString("author"));
                s.setDuration(rs.getString("duration"));
                s.setImage(rs.getString("image"));
                s.setPath(rs.getString("path"));
                s.setT_create(rs.getTimestamp("t_create"));
                s.setT_lastUpdate(rs.getTimestamp("t_lastUpdate"));
                songs.add(s);
            }
        } catch (SQLException ex) {

        }
        return songs;
    }
    public void insertAlbum(String name,String description,String image,int userId){
        String sql = "insert into Albums([image],[description], [name],[t_create],[user_ID]) values (?,?,?,GETDATE(),?)";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, image);
            st.setString(2, description);
            st.setString(3, name);           
            st.setInt(4, userId);
            st.executeUpdate();
            st.close();
        } catch (Exception e) {
        }
    }
  
    
    public void insertAlbum_Song(int albumid, int songid){
        String sql = "insert into Albums_Songs(album_ID,song_ID) values (?,?)";
        try {
           PreparedStatement st = connection.prepareStatement(sql);         
            st.setInt(1, albumid);
            st.setInt(2, songid);
            st.executeUpdate();
            st.close();
        } catch (Exception e) {
        }
    }
   
    
    public static void main(String[] args) {
        albumDAO ad = new albumDAO();
        System.out.println(ad.getSongsOfAlbums(1));
    }
       
}
