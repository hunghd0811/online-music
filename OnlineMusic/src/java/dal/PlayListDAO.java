/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Map;
import model.PlayLists;
import model.PlayListsSongs;
import model.Songs;
import model.Users;

/**
 *
 * @author Black
 */
public class PlayListDAO extends BaseDAO<PlayLists> {

    public void deletePlayList(int playlist_ID) {
        try {
            String sql = "delete from PlayLists_Songs where playlist_ID = ?";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, playlist_ID);
            st.executeUpdate();
            String sql1 = "delete from PlayLists where playlist_ID = ?";
            PreparedStatement st1 = connection.prepareStatement(sql1);
            st1.setInt(1, playlist_ID);
            st1.executeUpdate();
        } catch (SQLException e) {
        }
    }

    public ArrayList<Songs> getSongsOfPlayList(int playlist_ID) {
        ArrayList<Songs> songs = new ArrayList<>();
        try {
            String sql = "select s.song_ID, s.name, s.author, s.duration, s.[image], s.[path], s.t_create, s.t_lastUpdate \n"
                    + " from PlayLists_Songs als inner join PlayLists a on als.playlist_ID = a.playlist_ID\n"
                    + " inner join Songs s on als.song_ID = s.song_ID\n"
                    + " WHERE als.playlist_ID = ?";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, playlist_ID);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Songs s = new Songs();
                s.setSong_ID(rs.getInt("song_ID"));
                s.setName(rs.getString("name"));
                s.setAuthor(rs.getString("author"));
                s.setDuration(rs.getString("duration"));
                s.setImage(rs.getString("image"));
                s.setPath(rs.getString("path"));
                s.setT_create(rs.getTimestamp("t_create"));
                s.setT_lastUpdate(rs.getTimestamp("t_lastUpdate"));
                songs.add(s);
            }
        } catch (SQLException ex) {

        }
        return songs;
    }

    public ArrayList<PlayLists> getPlayListByUser_ID(int user_ID) {
        ArrayList<PlayLists> playlist = new ArrayList<>();
        try {
            String sql = "select p.playlist_ID, p.user_ID, p.name, p.t_create,p.sharestatus from PlayLists p \n"
                    + "inner join PlayLists_Songs s \n"
                    + "on p.playlist_ID = s.playlist_ID \n"
                    + "where p.user_ID = ? \n"
                    + "group by p.playlist_ID, p.user_ID, p.name, p.t_create,p.sharestatus ";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, user_ID);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                PlayLists s = new PlayLists();
                s.setPlaylist_ID(rs.getInt("playlist_ID"));
                s.setUser_ID(rs.getInt("user_ID"));
                s.setName(rs.getString("name"));
                s.setT_create(rs.getTimestamp("t_create"));
                s.setSharestatus(rs.getBoolean("sharestatus"));

                playlist.add(s);
            }
        } catch (SQLException e) {

        }
        return playlist;
    }

    public ArrayList<Songs> getSongByPlayListID(int playlist_ID) {
        ArrayList<Songs> songs = new ArrayList<>();
        try {
            String sql = "select s1.song_ID, s1.name, s1.author, s1.duration, s1.image, s1.path from PlayLists p \n"
                    + "inner join PlayLists_Songs s on p.playlist_ID = s.playlist_ID \n"
                    + "inner join Songs s1 on s.song_ID = s1.song_ID \n"
                    + "where p.playlist_ID = ?";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, playlist_ID);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Songs s = new Songs();
                s.setSong_ID(rs.getInt("song_ID"));
                s.setName(rs.getString("name"));
                s.setAuthor(rs.getString("author"));
                s.setDuration(rs.getString("duration"));
                s.setImage(rs.getString("image"));
                s.setPath(rs.getString("path"));
                songs.add(s);
            }
        } catch (SQLException e) {

        }
        return songs;
    }

    public int insertPlayList(PlayLists playlistt) {
        try {
            String sql = "insert into PlayLists (user_ID,name,sharestatus) values (?,?,0)";

            PreparedStatement st = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            st.setInt(1, playlistt.getUser_ID());
            st.setString(2, playlistt.getName());
            st.executeUpdate();

            ResultSet rs = st.getGeneratedKeys();
            if (rs.next()) {
                return rs.getInt(1);
            }
        } catch (SQLException e) {
        }
        return 0;
    }

    public void addplaylist(int plid, Map<Integer, PlayListsSongs> playl) {
        try {
            String sql = "insert into PlayLists_Songs (playlist_ID,song_ID) values (?, ?)";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, plid);
            for (Map.Entry<Integer, PlayListsSongs> entry : playl.entrySet()) {
                Integer song_ID = entry.getKey();
                PlayListsSongs play = entry.getValue();
                st.setInt(2, play.getSongs().getSong_ID());
                st.executeUpdate();
            }
        } catch (SQLException e) {
        }
    }

    public void updateShareStatus(int playlistid, boolean status) {
        try {
            String sql = "update PlayLists set sharestatus = ? where playlist_ID = ?";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setBoolean(1, status);
            st.setInt(2, playlistid);
            st.executeUpdate();
        } catch (Exception e) {
        }
    }

    public ArrayList<PlayLists> getPlayListByShareUser_ID(int user_ID) {
        ArrayList<PlayLists> playlist = new ArrayList<>();
        try {
            String sql = "select p.playlist_ID, p.user_ID, p.name, p.t_create,p.sharestatus from PlayLists p \n"
                    + "inner join PlayLists_Songs s \n"
                    + "on p.playlist_ID = s.playlist_ID \n"
                    + "inner join PlayLists_Shares m on\n"
                    + "m.playlist_ID = p.playlist_ID\n"
                    + "where m.user_ID_share = ?\n"
                    + "group by p.playlist_ID, p.user_ID, p.name, p.t_create,p.sharestatus ";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, user_ID);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                PlayLists s = new PlayLists();
                s.setPlaylist_ID(rs.getInt("playlist_ID"));
                s.setUser_ID(rs.getInt("user_ID"));
                s.setName(rs.getString("name"));
                s.setT_create(rs.getTimestamp("t_create"));
                s.setSharestatus(rs.getBoolean("sharestatus"));

                playlist.add(s);
            }
        } catch (SQLException e) {

        }
        return playlist;
    }

    public void updateUserIDShare(int userid, int playlistid) {
        try {
            String sql = "insert into PlayLists_Shares (playlist_ID,user_ID_share) values (?, ?)";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(2, userid);
            st.setInt(1, playlistid);
            st.executeUpdate();
        } catch (Exception e) {
        }
    }
    public void DeleteUserIDShare( int playlistid) {
        try {
            String sql = "delete from PlayLists_Shares where playlist_ID = ?";
            PreparedStatement st = connection.prepareStatement(sql);            
            st.setInt(1, playlistid);
            st.executeUpdate();
        } catch (Exception e) {
        }
    }

    public ArrayList<PlayLists> getAll() {
        ArrayList<PlayLists> playlist = new ArrayList<>();
        try {
            String sql = "select p.playlist_ID, p.user_ID, p.name, p.t_create,p.sharestatus from PlayLists p where p.playlist_ID = ?\n";

            PreparedStatement st = connection.prepareStatement(sql);

            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                PlayLists s = new PlayLists();
                s.setPlaylist_ID(rs.getInt("playlist_ID"));
                s.setUser_ID(rs.getInt("user_ID"));
                s.setName(rs.getString("name"));
                s.setT_create(rs.getTimestamp("t_create"));
                s.setSharestatus(rs.getBoolean("sharestatus"));

                playlist.add(s);
            }
        } catch (SQLException e) {

        }
        return playlist;
    }

    public PlayLists getAllbyplaylistid(int playlistid) {
        try {
            String sql = "select p.playlist_ID, p.user_ID, p.name, p.t_create,p.sharestatus from PlayLists p where p.playlist_ID = ?\n";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, playlistid);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                PlayLists s = new PlayLists();
                s.setPlaylist_ID(rs.getInt("playlist_ID"));
                s.setUser_ID(rs.getInt("user_ID"));
                s.setName(rs.getString("name"));
                s.setT_create(rs.getTimestamp("t_create"));
                s.setSharestatus(rs.getBoolean("sharestatus"));

                return s;
            }
        } catch (SQLException e) {

        }
        return null;
    }

    public Users getUserbyUserid() {

        try {
            String sql = "select u.user_ID,u.first_name,u.last_name from Users u inner join PlayLists p on p.user_ID = u.user_ID ";
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Users u = new Users();
                u.setUser_ID(rs.getInt("user_ID"));
                u.setFirst_name(rs.getString("first_name"));
                u.setLast_name(rs.getString("last_name"));
                return u;
            }
        } catch (SQLException e) {

        }
        return null;
    }

    public void addOneSongToPlayList(int userID, int playlistid, int songid) {

        try {
            String sql = "UPDATE [dbo].[PlayLists] SET [user_ID] = ? WHERE playlist_ID = ?";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, userID);
            st.setInt(2, playlistid);
            st.executeUpdate();
            String sql1 = "INSERT INTO [dbo].[PlayLists_Songs] ([playlist_ID] ,[song_ID])VALUES (?, ?)";
            PreparedStatement st1 = connection.prepareStatement(sql1);
            st1.setInt(1, playlistid);
            st1.setInt(2, songid);
            st1.executeUpdate();
            st.close();
        } catch (SQLException e) {
        }

    }

}
