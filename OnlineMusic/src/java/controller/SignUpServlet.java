/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dal.UserDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.MessagingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.RandomString;
import model.Users;
import smtp.Email;

/**
 *
 * @author huanv
 */
public class SignUpServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet SignUpServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet SignUpServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
       request.getRequestDispatcher("signup.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String username = request.getParameter("username");
        String email = request.getParameter("email");
        String gender = request.getParameter("gender");
        String date = request.getParameter("date");
        String password = request.getParameter("password");
        String repassword = request.getParameter("repassword");
        if (!password.equals(repassword)) {
            request.setAttribute("error", "Password does not match!");
            request.getRequestDispatcher("signup.jsp").forward(request, response);
        } else {
            UserDAO ud = new UserDAO();
            RandomString rand = new RandomString();
            
            if (ud.checkAccountExist(username, email) == null) {
                if ( ud.signup(username, password, email,date,gender,rand.randomAlphaNumeric(8))) {
                    int userId = ud.getNewestUser().getUser_ID();
                    String randomstring = ud.getNewestUser().getRandom();
                try {
                    
                 Email.sendEmail(email, "[?] Check Verify account #" , "Your account verify!! To Login, click link <a href='http://localhost:8080/SE1607_Group4_OnlineMusic/verify?id=" + randomstring + "'>here...</a>" );                                      
                 } catch (MessagingException e) {
                 Logger.getLogger(forgotPasswordServlet.class.getName()).log(Level.SEVERE, null, e);       
                 }
               
                request.setAttribute("success", "Account created successfully! Please check your mail for activation!");
                request.getRequestDispatcher("signup.jsp").forward(request, response); 
                
                } else {
                    request.setAttribute("error1", "Sign up fail!!");
                request.getRequestDispatcher("signup.jsp").forward(request, response);
                }
        
            } else {
                request.setAttribute("error1", "Username or email already existed!");
                request.getRequestDispatcher("signup.jsp").forward(request, response);
            }
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
