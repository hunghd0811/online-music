/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dal.PlayListDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.PlayLists;
import model.PlayListsSongs;
import model.Users;

/**
 *
 * @author Black
 */
@WebServlet(name = "PlayListAddOneServlet", urlPatterns = {"/playlistaddone"})
public class PlayListAddOneServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet PlayListAddOneServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet PlayListAddOneServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //   processRequest(request, response);
        HttpSession session = request.getSession();
        Map<Integer, PlayListsSongs> playa = (Map<Integer, PlayListsSongs>) session.getAttribute("playa");
        if (playa == null) {
            playa = new LinkedHashMap<>();
        }
        Users u = (Users) session.getAttribute("user");
        PlayListDAO dao = new PlayListDAO();
        if (u != null) {
            ArrayList<PlayLists> playlist = dao.getPlayListByUser_ID(u.getUser_ID());
            request.setAttribute("viewplayy", playlist);
        }
        request.setAttribute("playa", playa);
        request.getRequestDispatcher("AddPlay.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //   processRequest(request, response);
        HttpSession session = request.getSession();
        int userID = Integer.parseInt(request.getParameter("userid"));
        int playlistid = Integer.parseInt(request.getParameter("playlistid"));
        int songid = Integer.parseInt(request.getParameter("id"));
//        String pname = request.getParameter("pname");

        PlayListDAO dao = new PlayListDAO();
        dao.addOneSongToPlayList(userID, playlistid, songid);
        session.removeAttribute("playa");
                
        response.sendRedirect("home");
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
