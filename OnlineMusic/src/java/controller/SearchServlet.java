/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dal.SongsDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Songs;

/**
 *
 * @author Black
 */
@WebServlet(name = "SearchServlet", urlPatterns = {"/search"})
public class SearchServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
//        String txtSearch = request.getParameter("txtsearch");
//        SongsDAO dao = new SongsDAO();
//        String searchType = request.getParameter("searchType");
//        if (searchType.equals("3")) {
//            List<Songs> list = dao.searchByAuthor(txtSearch);
//            request.setAttribute("txtS", txtSearch);
//            request.setAttribute("searchType", 3);
//            request.setAttribute("listP", list);
//        }
//
//        request.getRequestDispatcher("searchDetail.jsp").forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        // processRequest(request, response);
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        String txtSearch = request.getParameter("txtsearch");
        SongsDAO dao = new SongsDAO();
        String searchType = request.getParameter("searchType");
        if (searchType.equals("1")) {
            List<Songs> listAuthor = dao.searchByAuthorAndName(txtSearch);
            request.setAttribute("searchType", 1);
            request.setAttribute("listAuthor", listAuthor);
        }
        if (searchType.equals("2")) {
            List<Songs> listAuthor = dao.searchByLyric(txtSearch);
            request.setAttribute("searchType", 2);
            request.setAttribute("listAuthor", listAuthor);
        }
        if (searchType.equals("3")) {
            List<Songs> listAuthor = dao.searchByAuthorAndName(txtSearch);
            request.setAttribute("searchType", 3);
            request.setAttribute("listAuthor", listAuthor);
        }
        request.setAttribute("txtS", txtSearch);
        request.getRequestDispatcher("searchDetail.jsp").forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
