/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dal.UserDAO;
import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;
import model.Users;

/**
 *
 * @author Thanh Thao
 */
@MultipartConfig()
public class UpdateProfileServlet extends HttpServlet {

    private static String getPath(String filePath) {

        StringBuilder builder = new StringBuilder();

        try (BufferedReader buffer = new BufferedReader(
                new FileReader(filePath))) {

            String str;

            while ((str = buffer.readLine()) != null) {

                builder.append(str).append("");
            }
        } catch (IOException e) {
            System.out.println(e);
        }
        return builder.toString();
    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet UpdateProfileServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet UpdateProfileServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
//        processRequest(request, response);
        request.getRequestDispatcher("profile.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        Users u = (Users) session.getAttribute("user");
        if (u == null) {
            PrintWriter out = response.getWriter();
            out.println("access denied");
        } else {
            UserDAO ud = new UserDAO();

            int id = u.getUser_ID();
            String first_name = request.getParameter("firstName");
            String last_name = request.getParameter("lastName");
            String gender = request.getParameter("gender");
            String old_password = request.getParameter("old-password");
            String new_password = "";
            if (request.getParameter("new-password") != null) {
                new_password = request.getParameter("new-password");
            }
            String verify_password = "";
            if (request.getParameter("verify-password") != null) {
                verify_password = request.getParameter("verify-password");
            }
            if (!old_password.equals(u.getPassword())) {
                request.setAttribute("erroOldPass", true);
//            request.getRequestDispatcher("profile").forward(request, response);
                request.getRequestDispatcher("profile.jsp").forward(request, response);
                return;
            }

            String email = request.getParameter("email");
            if (!u.getEmail().equalsIgnoreCase(email) && ud.getEmail(email)) {
                request.setAttribute("erroEmail", true);
//            request.getRequestDispatcher("profile").forward(request, response);
                request.getRequestDispatcher("profile.jsp").forward(request, response);
                return;
            }
            String address = request.getParameter("address");
            String dob = request.getParameter("birthday");
            Part file = request.getPart("photo");
            String filePath = "C:\\path.txt";
            String imageFileName = file.getSubmittedFileName();
            if (!imageFileName.contains(u.getUsername())) {
                String image = u.getUsername() + "-" + imageFileName;
                String uploadPath = getPath(filePath) + "/" + image;
                if (imageFileName != "") {
                    if (imageFileName.endsWith(".jpeg") || imageFileName.endsWith(".jpg") || imageFileName.endsWith(".png") || imageFileName.endsWith(".gif")) {
                        if (file.getSize() > 1024 * 1024 * 10) {
                            String error = "File must be smaller than 10MB";
                            request.setAttribute("error", error);
                            request.getRequestDispatcher("profile.jsp").forward(request, response);
                        } else {
                            try {
                                FileOutputStream fos = new FileOutputStream(uploadPath);
                                InputStream is = file.getInputStream();

                                byte[] data = new byte[is.available()];
                                is.read(data);
                                fos.write(data);
                                fos.close();

                            } catch (IOException e) {
                                System.out.println(e);
                            }
                            Users uNew = ud.updateUser(u.getUsername(), first_name, last_name, u.getPassword(), new_password, email, address, gender, dob, image, id);
                            session.setAttribute("user", uNew);
                            String mess = "Update Profile Successfully!";
                            session.setAttribute("mess", mess);
                            request.getRequestDispatcher("profile.jsp").forward(request, response);
                        }
                    } else {
                        String error = "Please choose file JPEG, JPG, PNG or GIF";
                        request.setAttribute("error", error);
                        request.getRequestDispatcher("profile.jsp").forward(request, response);
                    }
                }
                if (imageFileName == "") {
                    Users uNew = ud.updateUser2(u.getUsername(), first_name, last_name, u.getPassword(), new_password, email, address, gender, dob, id);
                    session.setAttribute("user", uNew);
                    String mess = "Update Profile Successfully!";
                    session.setAttribute("mess", mess);
                    request.getRequestDispatcher("profile.jsp").forward(request, response);
                }
            }
            if(imageFileName.contains(u.getUsername())){
                if (imageFileName != "") {
                    String uploadPath = getPath(filePath) + "/" + imageFileName;
                    if (imageFileName.endsWith(".jpeg") || imageFileName.endsWith(".jpg") || imageFileName.endsWith(".png") || imageFileName.endsWith(".gif")) {
                        if (file.getSize() > 1024 * 1024 * 10) {
                            String error = "File must be smaller than 10MB";
                            request.setAttribute("error", error);
                            request.getRequestDispatcher("profile.jsp").forward(request, response);
                        } else {
                            try {
                                FileOutputStream fos = new FileOutputStream(uploadPath);
                                InputStream is = file.getInputStream();

                                byte[] data = new byte[is.available()];
                                is.read(data);
                                fos.write(data);
                                fos.close();

                            } catch (IOException e) {
                                System.out.println(e);
                            }
                            Users uNew = ud.updateUser(u.getUsername(), first_name, last_name, u.getPassword(), new_password, email, address, gender, dob, imageFileName, id);
                            session.setAttribute("user", uNew);
                            String mess = "Update Profile Successfully!";
                            session.setAttribute("mess", mess);
                            request.getRequestDispatcher("profile.jsp").forward(request, response);
                        }
                    } else {
                        String error = "Please choose file JPEG, JPG, PNG or GIF";
                        request.setAttribute("error", error);
                        request.getRequestDispatcher("profile.jsp").forward(request, response);
                    }
                }
                if (imageFileName == "") {
                    Users uNew = ud.updateUser2(u.getUsername(), first_name, last_name, u.getPassword(), new_password, email, address, gender, dob, id);
                    session.setAttribute("user", uNew);
                    String mess = "Update Profile Successfully!";
                    session.setAttribute("mess", mess);
                    request.getRequestDispatcher("profile.jsp").forward(request, response);
                }
            }

        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
