/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dal.PlayListDAO;
import dal.SongsDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.PlayLists;
import model.PlayListsSongs;

/**
 *
 * @author Black
 */
@WebServlet(name = "PlayListSessServlet", urlPatterns = {"/playlistsess"})
public class PlayListSessServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet PlayListSessServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet PlayListSessServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        // processRequest(request, response);
        SongsDAO ad = new SongsDAO();

        HttpSession session = request.getSession();
        Map<Integer, PlayListsSongs> playl = (Map<Integer, PlayListsSongs>) session.getAttribute("playl");
        if (playl == null) {
            playl = new LinkedHashMap<>();
        }

        request.setAttribute("playl", playl);
        request.getRequestDispatcher("PlayListSong.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //  processRequest(request, response);
        HttpSession session = request.getSession();
        Map<Integer, PlayListsSongs> playl = (Map<Integer, PlayListsSongs>) session.getAttribute("playl");
        if (playl == null) {
            playl = new LinkedHashMap<>();
        }
        int userID = Integer.parseInt(request.getParameter("user"));
        String name = request.getParameter("name");
        
        for (Map.Entry<Integer, PlayListsSongs> entry : playl.entrySet()) {
            Integer song_ID = entry.getKey();
            PlayListsSongs play = entry.getValue();
            play.getSong_ID();
            
        }
        
        PlayLists playlistt = PlayLists.builder().user_ID(userID).name(name).build();
        
        int plid = new PlayListDAO().insertPlayList(playlistt);
        
        new PlayListDAO().addplaylist(plid, playl);
        
        session.removeAttribute("playl");
        response.sendRedirect("allplaylist");
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
