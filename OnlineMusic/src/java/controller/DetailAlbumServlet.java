/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dal.albumDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.Albums;
import model.Categories;
import model.CategoryDetails;
import model.Songs;
import model.Users;

/**
 *
 * @author anhqu
 */
public class DetailAlbumServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet DetailAlbumServlet</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet DetailAlbumServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
         HttpSession session = request.getSession();
        Users u = (Users) session.getAttribute("user");
        if(u.getRole_ID() != 1 || u == null ){
            PrintWriter out = response.getWriter();
            out.println("access denied");
        } else {      
        int albumid = Integer.parseInt(request.getParameter("id"));
        albumDAO dao = new albumDAO();      
        request.setAttribute("albumid", albumid);      
        List<Songs> listS = dao.getAllMusic(albumid);
        request.setAttribute("listS", listS);  
         List<CategoryDetails> listC = dao.getAllCategoryDetails();
        request.setAttribute("listC", listC);
        request.getRequestDispatcher("managedetailAlbum.jsp").forward(request, response);
        }
        
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        Users u = (Users) session.getAttribute("user");
        if(u.getRole_ID() != 1 || u == null ){
            PrintWriter out = response.getWriter();
            out.println("access denied");
        } else { 
            albumDAO dao = new albumDAO();
        int albumid = Integer.parseInt(request.getParameter("albumid"));
        String[] songid = request.getParameterValues("music");
            for (String s : songid) {
              
                dao.insertAlbum_Song(albumid, Integer.parseInt(s));
               
            }
      response.sendRedirect("updatealbum?id=" + albumid);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
