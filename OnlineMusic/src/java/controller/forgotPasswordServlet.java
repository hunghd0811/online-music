/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dal.EmailDAO;
import dal.UserDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.MessagingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.RandomString;
import model.Users;
import smtp.Email;

/**
 *
 * @author anhqu
 */
public class forgotPasswordServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet forgotPasswordServlet</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet forgotPasswordServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
         request.setAttribute("error", null);
        request.getRequestDispatcher("forgetpassword.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
               String email = request.getParameter("email");
         EmailDAO edao = new EmailDAO();
         UserDAO udao = new UserDAO();
         if (edao.getUserByEmail(email) != null) {
              RandomString rand = new RandomString();           
              edao.updateRandomString(email, rand.randomAlphaNumeric(8));
               Users u = new Users();               
                 u = edao.getUserByEmail(email);
                 int userID = u.getUser_ID();   
                 udao.updateStatus(userID, false);             
                String random = u.getRandom();
             try {               
                 Email.sendEmail(email, "[?] Request Reset Password #" + userID, "Your Password Here!! To Login, click link <a href='http://localhost:8080/SE1607_Group4_OnlineMusic/changepass?id=" + random + "'>here...</a> </br> <h4>Login and chill with the music</h4> " );          
             } catch (MessagingException e) {
                   Logger.getLogger(forgotPasswordServlet.class.getName()).log(Level.SEVERE, null, e);
                   request.setAttribute("error", "2");
                 request.getRequestDispatcher("forgetpassword.jsp").forward(request, response);
             }
                request.setAttribute("error", "3");
                 request.getRequestDispatcher("forgetpassword.jsp").forward(request, response);
        } else {
            request.setAttribute("error", "1");
            request.getRequestDispatcher("forgetpassword.jsp").forward(request, response);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
