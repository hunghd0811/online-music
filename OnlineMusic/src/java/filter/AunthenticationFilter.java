/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package filter;

import dal.UserDAO;
import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.Users;

/**
 *
 * @author Black
 */
@WebFilter(filterName = "AunthenticationFilter", urlPatterns = {"/addtoplaylists", "/delete-playlistsess", "/deleteplaylist",
    "/detailplaylist", "/playlistsess", "/profiledetails", "/allplaylist", "/admin/*", "/UpdateProfile", "/listacc", "/manageAlbum","/manager",
     "/delete", "/add", "/update", "/lofi", "/searchacc", "/block", "/unblock", "/deletealbum", "/updatestatus",
    "/addalbum", "/addsongtoalbum", "/managecomment", "/blockcomment", "/unblockcomment", "/addcomment", "/managehistory",
    "/manageLike", "/report", "/blockAll", "/uploadavatar", "/share", "/unshare",
    "/shareplaylist", "/deleteSCheckbox", "/addonesongtoplaylist", "/reply", "/changepass", "/deletecomment", "/edicomment", "/playlistaddone"})
public class AunthenticationFilter implements Filter {//"/manager",
    /**
     *
     * @param request The servlet request we are processing
     * @param response The servlet response we are creating
     * @param chain The filter chain we are processing
     *
     * @exception IOException if an input/output error occurs
     * @exception ServletException if a servlet error occurs
     */
    @Override
    public void doFilter(ServletRequest request, ServletResponse response,
            FilterChain chain)
            throws IOException, ServletException {
        HttpServletRequest reques = (HttpServletRequest) request;
        HttpServletResponse respon = (HttpServletResponse) response;
        HttpSession session = reques.getSession();
        Users user = (Users) session.getAttribute("user");
        if (user != null) {
            chain.doFilter(request, response);
        }else{
            Cookie[] cookies = reques.getCookies();
            String username = null;
            String password = null;
            for (Cookie cooky : cookies) {
                if (cooky.getName().equals("username")) {
                    username = cooky.getValue();
                }
                if (cooky.getName().equals("password")) {
                    password = cooky.getValue();
                }
                if (username != null && password != null) {
                    break;
                }
            }
            if (username != null && password != null) {
                Users userlogin = new UserDAO().check(username, password);
                if (user != null) {
                    session.setAttribute("user", user);
                    chain.doFilter(request, response);
                    return;
                }
            }
        respon.sendRedirect("/SE1607_Group4_OnlineMusic/sign");
        }
    }
    /**
     * Destroy method for this filter
     */
    @Override
    public void destroy() {        
    }

    /**
     * Init method for this filter
     * @param filterConfig
     */
    @Override
    public void init(FilterConfig filterConfig) {        
       
    }
     
}
