/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.Timestamp;

/**
 *
 * @author anhqu
 */
public class Users {
    public static final int ADMIN = 1;
    public static final int MANAGER = 2;
    public static final int CUSTOMER = 3;
    private int user_ID;
    private String username;
    private String password;
    private String first_name;
    private String last_name;
    private String avatar;
    private Timestamp t_create;
    private Timestamp t_lastOnline;
    private String email;
    private boolean status;
    private String address;
    private String gender;
    private String dob;
    private int role_ID;
    private String random;
    private boolean report;
    
    public Users() {
    }
    
    public Users(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public Users(int user_ID, String username, String password, String email,boolean status) {
        this.user_ID = user_ID;
        this.username = username;
        this.password = password;
        this.email = email;
        this.status = status;
    }

    
    public Users(String username, String password, String email) {
        this.username = username;
        this.password = password;
        this.email = email;
    }
    
    public Users(int user_ID, String username, String password, String first_name, String last_name, String avatar, Timestamp t_create, Timestamp t_lastOnline, String email, boolean status, String address, String gender, String dob, int role_ID) {
        this.user_ID = user_ID;
        this.username = username;
        this.password = password;
        this.first_name = first_name;
        this.last_name = last_name;
        this.avatar = avatar;
        this.t_create = t_create;
        this.t_lastOnline = t_lastOnline;
        this.email = email;
        this.status = status;
        this.address = address;
        this.gender = gender;
        this.dob = dob;
        this.role_ID = role_ID;
    }

    public Users(int user_ID, String username, String password, String first_name, String last_name, String avatar, Timestamp t_create, Timestamp t_lastOnline, String email, boolean status, String address, String gender, String dob, int role_ID, String random) {
        this.user_ID = user_ID;
        this.username = username;
        this.password = password;
        this.first_name = first_name;
        this.last_name = last_name;
        this.avatar = avatar;
        this.t_create = t_create;
        this.t_lastOnline = t_lastOnline;
        this.email = email;
        this.status = status;
        this.address = address;
        this.gender = gender;
        this.dob = dob;
        this.role_ID = role_ID;
        this.random = random;
    }

    public Users(int user_ID, String username, String password, String first_name, String last_name, String avatar, Timestamp t_create, Timestamp t_lastOnline, String email, boolean status, String address, String gender, String dob, int role_ID, String random, boolean report) {
        this.user_ID = user_ID;
        this.username = username;
        this.password = password;
        this.first_name = first_name;
        this.last_name = last_name;
        this.avatar = avatar;
        this.t_create = t_create;
        this.t_lastOnline = t_lastOnline;
        this.email = email;
        this.status = status;
        this.address = address;
        this.gender = gender;
        this.dob = dob;
        this.role_ID = role_ID;
        this.random = random;
        this.report = report;
    }

    
    public boolean isReport() {
        return report;
    }

    public void setReport(boolean report) {
        this.report = report;
    }
    
    

    public int getUser_ID() {
        return user_ID;
    }

    public void setUser_ID(int user_ID) {
        this.user_ID = user_ID;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public Timestamp getT_create() {
        return t_create;
    }

    public void setT_create(Timestamp t_create) {
        this.t_create = t_create;
    }

    public Timestamp getT_lastOnline() {
        return t_lastOnline;
    }

    public void setT_lastOnline(Timestamp t_lastOnline) {
        this.t_lastOnline = t_lastOnline;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getGender() {
        return gender;
    }
    

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public int getRole_ID() {
        return role_ID;
    }

    public void setRole_ID(int role_ID) {
        this.role_ID = role_ID;
    }

    public String getRandom() {
        return random;
    }

    public void setRandom(String random) {
        this.random = random;
    }
    
    
     public Timestamp getT_now() {
        return MyMethod.getT_now();
    }
      public String getTimeAgo(){
        return MyMethod.getTimeAgo(MyMethod.getT_now(), t_lastOnline);
    }

    public void setGender(String gender) {
        this.gender = gender;
    }
      
        @Override
    public String toString() {
        return "Users{" + "user_ID=" + user_ID + ", username=" + username + ", password=" + password + ", first_name=" + first_name + ", last_name=" + last_name + ", avatar=" + avatar + ", t_create=" + t_create + ", t_lastOnline=" + t_lastOnline + ", email=" + email + ", status=" + status + ", address=" + address + ", gender=" + gender + ", dob=" + dob + ", role_ID=" + role_ID + '}';
    }
}
