/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;


import java.sql.Timestamp;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 *
 * @author Black
 */
@Data
@Builder
@Getter
@Setter
@ToString
public class PlayLists {
    private int playlist_ID;
    private int user_ID;
    private String name;
    private Songs song;
    private Timestamp t_create;
    private boolean sharestatus;
    

    public PlayLists() {
    }

    public PlayLists(int playlist_ID, int user_ID, String name, Timestamp t_create) {
        this.playlist_ID = playlist_ID;
        this.user_ID = user_ID;
        this.name = name;
        this.t_create = t_create;
    }

    public PlayLists(int playlist_ID, int user_ID, String name, Songs song, Timestamp t_create) {
        this.playlist_ID = playlist_ID;
        this.user_ID = user_ID;
        this.name = name;
        this.song = song;
        this.t_create = t_create;
    }

    public PlayLists(int playlist_ID, int user_ID, String name, Songs song, Timestamp t_create, boolean sharestatus) {
        this.playlist_ID = playlist_ID;
        this.user_ID = user_ID;
        this.name = name;
        this.song = song;
        this.t_create = t_create;
        this.sharestatus = sharestatus;
    }
   
}
