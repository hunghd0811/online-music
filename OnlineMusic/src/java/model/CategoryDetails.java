/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Thanh Thao
 */
public class CategoryDetails {
    private int categoryDetail_ID;
    private String categoryDetail_name;
    private int categoryID;

    public CategoryDetails() {
    }

    public CategoryDetails(int categoryDetail_ID, String categoryDetail_name, int categoryID) {
        this.categoryDetail_ID = categoryDetail_ID;
        this.categoryDetail_name = categoryDetail_name;
        this.categoryID = categoryID;
    }

    public int getCategoryDetail_ID() {
        return categoryDetail_ID;
    }

    public void setCategoryDetail_ID(int categoryDetail_ID) {
        this.categoryDetail_ID = categoryDetail_ID;
    }

    public String getCategoryDetail_name() {
        return categoryDetail_name;
    }

    public void setCategoryDetail_name(String categoryDetail_name) {
        this.categoryDetail_name = categoryDetail_name;
    }

    public int getCategoryID() {
        return categoryID;
    }

    public void setCategoryID(int categoryID) {
        this.categoryID = categoryID;
    }
    
    
    
}
