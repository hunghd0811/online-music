/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.Timestamp;

/**
 *
 * @author Auriat
 */
public class ReplyComment {
    private int id;
    private int song_ID;
    private int comment_ID;
    private int user_ID;
    private String content;
    private Timestamp t_create;
    private Timestamp t_lastUpdate;
    private boolean status;
    private boolean report;

    public ReplyComment() {
    }

    public ReplyComment(int id, int song_ID, int comment_ID, int user_ID, String content, Timestamp t_create, Timestamp t_lastUpdate, boolean status, boolean report) {
        this.id = id;
        this.song_ID = song_ID;
        this.comment_ID = comment_ID;
        this.user_ID = user_ID;
        this.content = content;
        this.t_create = t_create;
        this.t_lastUpdate = t_lastUpdate;
        this.status = status;
        this.report = report;
    }

 

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public boolean isReport() {
        return report;
    }

    public void setReport(boolean report) {
        this.report = report;
    }

 
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSong_ID() {
        return song_ID;
    }

    public void setSong_ID(int song_ID) {
        this.song_ID = song_ID;
    }

    public int getComment_ID() {
        return comment_ID;
    }

    public void setComment_ID(int comment_ID) {
        this.comment_ID = comment_ID;
    }

    public int getUser_ID() {
        return user_ID;
    }

    public void setUser_ID(int user_ID) {
        this.user_ID = user_ID;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Timestamp getT_create() {
        return t_create;
    }

    public void setT_create(Timestamp t_create) {
        this.t_create = t_create;
    }

    public Timestamp getT_lastUpdate() {
        return t_lastUpdate;
    }

    public void setT_lastUpdate(Timestamp t_lastUpdate) {
        this.t_lastUpdate = t_lastUpdate;
    }
    
    
}
