/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.Timestamp;

/**
 *
 * @author Black
 */
public class Songs {

    private int song_ID;
    private String name;
    private String author;
    private String duration;
    private String image;
    private String path;
    private Timestamp t_create;
    private Timestamp t_lastUpdate;
    private int category_ID;
    private int album_ID;
    private int playlist_ID;
    private String lyric;
    private boolean like;
    private int views;

    public Songs() {
    }

    public Songs(int song_ID) {
        this.song_ID = song_ID;
    }

    
    public Songs(int song_ID, String name, String author, String duration, String image, String path, Timestamp t_create, Timestamp t_lastUpdate, int category_ID, String lyric) {
        this.song_ID = song_ID;
        this.name = name;
        this.author = author;
        this.duration = duration;
        this.image = image;
        this.path = path;
        this.t_create = t_create;
        this.t_lastUpdate = t_lastUpdate;
        this.category_ID = category_ID;
        this.lyric = lyric;

    }

    public Songs(int song_ID, String name, String author, String duration, String image, String path, Timestamp t_create, Timestamp t_lastUpdate, int category_ID, int album_ID, int playlist_ID, String lyric) {
        this.song_ID = song_ID;
        this.name = name;
        this.author = author;
        this.duration = duration;
        this.image = image;
        this.path = path;
        this.t_create = t_create;
        this.t_lastUpdate = t_lastUpdate;
        this.category_ID = category_ID;
        this.album_ID = album_ID;
        this.playlist_ID = playlist_ID;
        this.lyric = lyric;

    }

    public Songs(String name, String author, String duration, String image, String path, Timestamp t_create, Timestamp t_lastUpdate,String lyric) {
        this.name = name;
        this.author = author;
        this.duration = duration;
        this.image = image;
        this.path = path;
        this.t_create = t_create;
        this.t_lastUpdate = t_lastUpdate;
        this.lyric = lyric;
    }

    public Songs(String name, String author, String duration, String image, String path, Timestamp t_create, Timestamp t_lastUpdate, int category_ID, String lyric) {
        this.name = name;
        this.author = author;
        this.duration = duration;
        this.image = image;
        this.path = path;
        this.t_create = t_create;
        this.t_lastUpdate = t_lastUpdate;
        this.category_ID = category_ID;
        this.lyric = lyric;

    }

    public Songs(String name, String author, String duration, String image, String path, Timestamp t_create, Timestamp t_lastUpdate, int category_ID, int album_ID, int playlist_ID, String lyric) {
        this.name = name;
        this.author = author;
        this.duration = duration;
        this.image = image;
        this.path = path;
        this.t_create = t_create;
        this.t_lastUpdate = t_lastUpdate;
        this.category_ID = category_ID;
        this.album_ID = album_ID;
        this.playlist_ID = playlist_ID;
        this.lyric = lyric;
    }

    public Songs(int song_ID, String name, String author, String duration, String image, String path, Timestamp t_create, Timestamp t_lastUpdate, int category_ID, int album_ID, int playlist_ID, String lyric, boolean like) {
        this.song_ID = song_ID;
        this.name = name;
        this.author = author;
        this.duration = duration;
        this.image = image;
        this.path = path;
        this.t_create = t_create;
        this.t_lastUpdate = t_lastUpdate;
        this.category_ID = category_ID;
        this.album_ID = album_ID;
        this.playlist_ID = playlist_ID;
        this.lyric = lyric;
        this.like = like;
    }

    public int getViews() {
        return views;
    }

    public void setViews(int views) {
        this.views = views;
    }

    public boolean isLike() {
        return like;
    }

    public void setLike(boolean like) {
        this.like = like;
    }

    
    public String getLyric() {
        return lyric;
    }

    public void setLyric(String lyric) {
        this.lyric = lyric;
    }

    
    public int getAlbum_ID() {
        return album_ID;
    }

    public void setAlbum_ID(int album_ID) {
        this.album_ID = album_ID;
    }

    public int getPlaylist_ID() {
        return playlist_ID;
    }

    public void setPlaylist_ID(int playlist_ID) {
        this.playlist_ID = playlist_ID;
    }

    public int getSong_ID() {
        return song_ID;
    }

    public void setSong_ID(int song_ID) {
        this.song_ID = song_ID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Timestamp getT_create() {
        return t_create;
    }

    public void setT_create(Timestamp t_create) {
        this.t_create = t_create;
    }

    public Timestamp getT_lastUpdate() {
        return t_lastUpdate;
    }

    public void setT_lastUpdate(Timestamp t_lastUpdate) {
        this.t_lastUpdate = t_lastUpdate;
    }

    public int getCategory_ID() {
        return category_ID;
    }

    public void setCategory_ID(int category_ID) {
        this.category_ID = category_ID;
    }

    @Override
    public String toString() {
        return "Songs{" + "song_ID=" + song_ID + ", name=" + name + ", author=" + author + ", duration=" + duration + ", image=" + image + ", path=" + path + ", t_create=" + t_create + ", t_lastUpdate=" + t_lastUpdate + ", category_ID=" + category_ID + ", lyric=" + lyric +  '}';
    }

}
